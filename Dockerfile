FROM php:7.1.18-apache

RUN apt-get update
RUN docker-php-ext-install pdo pdo_mysql mysqli gettext
RUN a2enmod rewrite
RUN apt-get install -y libzip-dev && docker-php-ext-install zip
COPY scripts/start.sh /root/
COPY ./www /var/www/html/
RUN mkdir -p /var/www/cache /var/www/assets /var/www/templates_c
RUN chown -R www-data:www-data /var/www/*
RUN chmod 775 /var/www/*
CMD ["bash","/root/start.sh"]
