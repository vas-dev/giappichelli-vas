BEGIN;

ALTER TABLE lessons ADD COLUMN type integer not null default 1;
ALTER TABLE lessons ADD COLUMN homework integer not null default 1;
ALTER TABLE lessons ADD COLUMN materials integer not null default 1;
ALTER TABLE lessons ADD COLUMN questions integer not null default 1;
ALTER TABLE lessons ADD COLUMN related varchar(1024);
ALTER TABLE lessons ADD COLUMN playlist varchar(1024);

COMMIT;