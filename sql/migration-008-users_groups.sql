BEGIN;

ALTER TABLE users ADD COLUMN groups varchar(255);

CREATE TABLE groups (
  id serial,
  name varchar(255),
  courses varchar(255),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

COMMIT;