BEGIN;

ALTER TABLE courses ADD COLUMN teachers varchar(255);
UPDATE courses SET teachers = CONCAT(';', teacher_id, ';');
ALTER TABLE courses DROP COLUMN teacher_id;

COMMIT;