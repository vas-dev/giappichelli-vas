BEGIN;

CREATE TABLE users_be (
  id serial,
  company varchar(255),
  lastname varchar(255) not null,
  firstname varchar(255) not null,
  email varchar(255) not null,
  role integer not null default 1,
  roles varchar(255),
  password char(32),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE menus (
  id serial,
  lang char(2),
  slug varchar(255),
  content text,
  published integer not null default 1,
  published_datetime datetime,
  published_datetime2 datetime,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE pages (
  id serial,
  lang char(2),
  title varchar(1024),
  cover varchar(255),
  slug varchar(255),
  content text,
  published integer not null default 1,
  published_datetime datetime,
  published_datetime2 datetime,
  seo_title text,
  seo_image varchar(255),
  seo_description text,
  seo_keywords text,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE users (
  id serial,
  uniqid char(13),
  company varchar(255),
  firstname varchar(255),
  lastname varchar(255),
  email varchar(255),
  password char(64),
  address varchar(1024),
  city varchar(255),
  province char(2),
  zipcode char(5),
  country varchar(255),
  phone varchar(255),
  mobile varchar(255),
  roles varchar(255),
  courses varchar(255),
  groups varchar(255),
  premium integer,
  active integer,
  privacy integer,
  privacy2 integer,
  newsletter integer,
  days_1 integer,
  dates text,
  source varchar(255),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE users_files (
  id serial,
  user_id bigint(20) not null,
  homework_id bigint(20),
  name varchar(255),
  asset varchar(255),
  type varchar(255),
  size integer,
  reply_name varchar(255),
  reply_asset varchar(255),
  reply_type varchar(255),
  reply_size integer,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE redirects (
  id integer not null auto_increment,
  request_uri varchar(255),
  redirect_uri varchar(255),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_categories (
  id serial,
  category_id integer,
  slug varchar(255) not null,
  title varchar(255) not null,
  color varchar(255),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_tags (
  id serial,
  slug varchar(255) not null,
  tag varchar(255) not null,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_articles (
  id serial,
  slug varchar(255) not null,
  title varchar(255) not null,
  cover varchar(255),
  author varchar(255),
  description text,
  body text,
  date date,
  category_id bigint(20) unsigned not null,
  category2_id bigint(20) unsigned,
  cities text,
  published integer not null default 1,
  published_datetime datetime,
  requires_login integer,
  seo_title text,
  seo_description text,
  seo_keyword text,
  seo_keywords text,
  seo_trackback varchar(255),
  homepage integer,
  focus integer,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id),
  foreign key (category_id) references blog_categories (id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_articles_comments (
  id serial,
  user_id bigint(20) unsigned not null,
  article_id bigint(20) unsigned not null,
  status integer,
  parent_id integer,
  body text,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id),
  foreign key (article_id) references blog_articles (id) on delete cascade on update cascade,
  foreign key (user_id) references users (id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_articles_images (
  id serial,
  article_id bigint(20) unsigned not null,
  title varchar(255),
  image varchar(255),
  name varchar(255),
  sorting integer,
  size integer,
  md5 char(32),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id),
  foreign key (article_id) references blog_articles (id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_articles_files (
  id serial,
  article_id bigint(20) unsigned not null,
  title varchar(255),
  file varchar(255),
  name varchar(255),
  sorting integer,
  size integer,
  md5 char(32),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id),
  foreign key (article_id) references blog_articles (id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE blog_articles_tags (
  id serial,
  article_id bigint(20) unsigned not null,
  tag_id bigint(20) unsigned not null,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id),
  foreign key (article_id) references blog_articles (id) on delete cascade on update cascade,
  foreign key (tag_id) references blog_tags (id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE banners (
  id serial,
  slug varchar(255),
  title varchar(255),
  image varchar(255),
  navigator varchar(255),
  cta varchar(1024),
  link varchar(1024),
  category integer,
  sorting integer,
  published integer,
  date_begin date,
  date_end date,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE courses (
  id serial,
  lang char(2),
  parent_id bigint(20) unsigned,
  teachers varchar(255),
  name varchar(255),
  category varchar(255),
  city varchar(255),
  slug varchar(255),
  description text,
  preview text,
  duration text,
  calendar text,
  place text,
  prices text,
  image varchar(255),
  pdf varchar(255),
  form varchar(255),
  button_label varchar(255),
  button_link varchar(255),
  published integer not null default 1,
  published_datetime datetime,
  published_datetime2 datetime,
  sorting integer not null default 0,
  max_hours integer,
  features_form integer not null default 0,
  seo_title text,
  seo_image varchar(255),
  seo_description text,
  seo_keywords text,
  likes integer not null default 0,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE courses_users (
  id serial,
  course_id bigint(20) not null,
  user_id bigint(20) not null,
  max_hours integer null,
  created_at datetime not null,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE teachers (
  id serial,
  lang char(2),
  parent_id bigint(20) unsigned,
  name varchar(255),
  slug varchar(255),
  description text,
  image varchar(255),
  pdf varchar(255),
  published integer not null default 1,
  published_datetime datetime,
  published_datetime2 datetime,
  sorting integer not null default 0,
  seo_title text,
  seo_image varchar(255),
  seo_description text,
  seo_keywords text,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE lessons (
  id serial,
  courses varchar(255),
  type integer not null default 1,
  homework integer not null default 1,
  materials integer not null default 1,
  questions integer not null default 1,
  related varchar(1024),
  playlist varchar(1024),
  name varchar(255),
  description text,
  widget text,
  widget_audio text,
  lesson_date date,
  lesson_time varchar(255),
  support_button varchar(255),
  support_target varchar(255),
  files text,
  max_views integer not null default 0,
  published integer not null default 1,
  published_datetime datetime,
  published_datetime2 datetime,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE lessons_questions (
  id serial,
  lesson_id bigint(20) unsigned,
  user_id bigint(20) unsigned,
  subject varchar(255),
  question text,
  status integer not null default 0,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE lessons_users (
  id serial,
  lesson_id bigint(20) not null,
  user_id bigint(20) not null,
  created_at datetime not null,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE homeworks (
  id serial,
  course_id bigint(20) unsigned,
  name varchar(255),
  description text,
  published integer not null default 1,
  published_datetime date,
  published_datetime2 date,
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
);

CREATE TABLE pubblications (
  id serial,
  lang char(2),
  parent_id bigint(20) unsigned,
  name varchar(255),
  description text,
  image varchar(255),
  url varchar(255),
  teachers varchar(255),
  courses varchar(255),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

CREATE TABLE groups (
  id serial,
  name varchar(255),
  courses varchar(255),
  created_at datetime not null,
  updated_at datetime,
  deleted_at datetime,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

COMMIT;
