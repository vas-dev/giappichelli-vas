BEGIN;

ALTER TABLE lessons ADD COLUMN courses varchar(255);
UPDATE lessons SET courses = CONCAT(';', course_id, ';');
ALTER TABLE lessons DROP COLUMN course_id;

COMMIT;