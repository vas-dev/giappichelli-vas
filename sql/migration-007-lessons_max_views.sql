BEGIN;

ALTER TABLE lessons ADD COLUMN max_views integer not null default 0;

CREATE TABLE lessons_users (
  id serial,
  lesson_id bigint(20) not null,
  user_id bigint(20) not null,
  created_at datetime not null,
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_bin;

COMMIT;