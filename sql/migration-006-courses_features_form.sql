BEGIN;

ALTER TABLE courses ADD COLUMN features_form integer not null default 0;

COMMIT;