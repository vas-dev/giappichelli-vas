# Quick Start for Developers

```sh
  $ docker-compose up
```

The `www` directory is mounted (bind) into the container during development
and copied (COPY) into the image at `docker build`.

Have fun!