{extends file='layout-admin.tmpl'}
<html>
  <body>
    {block name="breadcrumbs"}
    <div class="breadcrumbs">
      <a href="{$base_url}/admin/pages">Pagine</a> <span class="divider">&raquo;</span>
      <strong>{$page.slug}</strong>
    </div>
    {/block}
    {block name="content"}
    <div class="sheet">
      <div class="sheet-header">
        <h1>
          Modifica pagina
          <small>{$page.slug}</small>
        </h1>
      </div>
      <div class="sheet-body">
        {if $errors}
        <div class="alert alert-danger">
          <strong>ATTENZIONE: si sono verificati degli errori, correggere prima di proseguire!</strong>
        </div>
        {/if}
        <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
          <fieldset>
            <legend>Informazioni di base</legend>
            <div class="form-group{if isset($errors.lang)} error{/if}">
              <label class="control-label col-md-3" for="lang">Lingua</label>
              <div class="controls col-md-9">
                <select name="lang" id="lang" class="form-control">
                  {html_options options=$LANGS selected=_f('lang', $page)}
                </select>
                {if isset($errors.lang)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.slug)} error{/if}">
              <label class="control-label col-md-3" for="slug">SLUG</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="slug" name="slug" value="{_f('slug', $page)}">
                {if isset($errors.slug)}<p class="help-block error">Campo obbligatorio!</p>{/if}
                <p class="help-block">
                  Inserire lo SLUG della pagina, es. <code>/</code> per la home page, <code>/contatti</code> per la pagina contatti.<br>
                  È anche possibile inserire SLUG di secondo livello, es. <code>/sezione/pagina</code>.
                </p>
              </div>
            </div>
            <div class="form-group{if isset($errors.title)} error{/if}">
              <label class="control-label col-md-3" for="title">Titolo</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="title" name="title" value="{_f('title', $page)}">
                {if isset($errors.title)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.cover)} error{/if}">
              <label class="control-label col-md-3" for="cover">Header</label>
              <div class="controls col-md-9">
                <input type="file" class="form-control" id="cover" name="cover" value="">
                {if isset($page.cover) and $page.cover}
                <p class="mt20">
                  <input type="hidden" name="cover" value="{$page.cover}">
                  <img src="{$base_url}/assets/{$page.cover}" alt="{$page.title}" class="img-responsive img-editable" id="img_cover">
                </p>
                {/if}
                {if isset($errors.cover)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3" for="title">Contenuto</label>
              <div class="controls col-md-9">
                <div class="table-responsive">
                  <table class="table table-striped table-hover table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 80px">Posizione</th>
                        <th style="width: 80px">ID</th>
                        <th>Nome</th>
                        <th>Tipo contenuto</th>
                        <th class="text-center" style="width: 140px;">Azioni</th>
                      </tr>
                    </thead>
                    <tbody>
                      {foreach $page->content() as $c}
                      <tr>
                        <td>
                          <a href="{$base_url}/admin/pages/{$page.id}/{$c.id}">#{$c.sorting}</a>
                        </td>
                        <td>
                          <a href="{$base_url}/admin/pages/{$page.id}/{$c.id}">{$c.id}</a>
                        </td>
                        <td>
                          <a href="{$base_url}/admin/pages/{$page.id}/{$c.id}">{$c.name|default:"&mdash;"}</a>
                        </td>
                        <td>
                          {_v($c.type, $PAGE_CONTENT_TYPES)}
                        </td>
                        <td class="text-center">
                          <a href="{$base_url}/admin/pages/{$page.id}/{$c.id}" class="btn btn-xs btn-default">Modifica</a>
                          <a href="{$base_url}/admin/pages/{$page.id}/{$c.id}?delete=1" class="btn btn-xs btn-danger">Elimina</a>
                        </td>
                      </tr>
                      {/foreach}
                    </tbody>
                  </table>
                  <p class="mt20">
                    <a href="{$base_url}/admin/pages/{$page.id}/add" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Nuovo elemento</a>
                  </p>
                </div>
              </div>
            </div>
          </fieldset>
          <fieldset class="mt30">
            <legend>Impostazioni di pubblicazione</legend>
            <div class="form-group{if isset($errors.published)} error{/if}">
              <label class="control-label col-md-3" for="published">Pubblicato</label>
              <div class="controls col-md-9">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="published" value="1"{if _f('published', $page)} checked="checked"{/if}>
                  </label>
                </div>
                {if isset($errors.published)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.published_datetime)} error{/if}">
              <label class="control-label col-md-3" for="published_datetime">Data/Ora di pubblicazione</label>
              <div class="controls col-md-9">
                {_datetime_combos("published_datetime", _d('published_datetime', $page), -1, 1)}
                {if isset($errors.published_datetime)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.published_datetime2)} error{/if}">
              <label class="control-label col-md-3" for="published_datetime2">Data/Ora di fine pubblicazione</label>
              <div class="controls col-md-9">
                {_datetime_combos("published_datetime2", _d('published_datetime2', $page), -1, 1)}
                {if isset($errors.published_datetime2)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
          </fieldset>
          <fieldset class="mt30">
            <legend>Impostazioni SEO</legend>
            <div class="form-group{if isset($errors.seo_title)} error{/if}">
              <label class="control-label col-md-3" for="seo_title">Titolo</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="seo_title" name="seo_title" value="{_f('seo_title', $page)}">
                {if isset($errors.seo_title)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.seo_image)} error{/if}">
              <label class="control-label col-md-3" for="seo_image">Immagine</label>
              <div class="controls col-md-9">
                <input type="file" class="form-control" id="seo_image" name="seo_image" value="">
                {if isset($page.seo_image) and $page.seo_image}
                <p class="mt20">
                  <input type="hidden" name="seo_image" value="{$page.seo_image}">
                  <img src="{$base_url}/assets/{$page.seo_image}" alt="{$page.title}" class="img-responsive img-editable" id="img_seo_image">
                </p>
                {/if}
                {if isset($errors.seo_image)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.seo_description)} error{/if}">
              <label class="control-label col-md-3" for="seo_description">Descrizione</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="seo_description" name="seo_description" value="{_f('seo_description', $page)}">
                {if isset($errors.seo_description)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.seo_keywords)} error{/if}">
              <label class="control-label col-md-3" for="seo_keywords">Keywords</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="seo_keywords" name="seo_keywords" value="{_f('seo_keywords', $page)}">
                {if isset($errors.seo_keywords)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
          </fieldset>
          <div class="form-actions">
            <button type="submit" class="btn btn-success">Salva le modifiche</button>
            <a href="{$base_url}/admin/pages" class="btn btn-default">Annulla</a>
          </div>
        </form>
      </div>
    </div>
    {/block}
  </body>
</html>
