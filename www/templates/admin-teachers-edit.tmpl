{extends file='layout-admin.tmpl'}
<html>
  <body>
    {block name="breadcrumbs"}
    <div class="breadcrumbs">
      <a href="{$base_url}/admin/teachers">Docenti</a> <span class="divider">&raquo;</span>
      <strong>{$teacher.name}</strong>
    </div>
    {/block}
    {block name="content"}
    <div class="sheet">
      <div class="sheet-header">
        <h1>
          Modifica docente
          <small>{$teacher.name}</small>
        </h1>
        <ul class="nav nav-pills mt10">
          {foreach $LANGS as $k => $v}
          <li role="presentation"{if $lang eq $k} class="active"{/if}><a href="?lang={$k}">{$v}</a></li>
          {/foreach}
        </ul>
      </div>
      <div class="sheet-body">
        {if $errors}
        <div class="alert alert-danger">
          <strong>ATTENZIONE: si sono verificati degli errori, correggere prima di proseguire!</strong>
        </div>
        {/if}
        <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
          <fieldset>
            <div class="form-group{if isset($errors.name)} error{/if}">
              <label class="control-label col-md-3" for="name">Nome</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="name" name="name" value="{_f('name', $teacher)}">
                {if isset($errors.name)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.slug)} error{/if}">
              <label class="control-label col-md-3" for="slug">SLUG</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="slug" name="slug" value="{_f('slug', $teacher)}">
                {if isset($errors.slug) and $errors.slug eq 1}<p class="help-block error">Campo obbligatorio!</p>{/if}
                {if isset($errors.slug) and $errors.slug eq 2}<p class="help-block error">Questo SLUG è già stato assegnato!</p>{/if}
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3" for="image">Immagine di copertina</label>
              <div class="controls col-md-9">
                <input type="file" id="image" name="image" class="form-control">
                {if _f('image', $teacher)}
                <p class="mt20">
                  <input type="hidden" name="image" value="{$teacher.image}">
                  <img src="{$cdn_url}/assets/{$teacher.image}" alt="{$article.title}" class="img-responsive img-editable" id="img_image">
                </p>
                {/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.intro)} error{/if}">
              <label class="control-label col-md-3" for="intro">Introduzione (max 90 caratteri)</label>
              <div class="controls col-md-9">
                <textarea class="form-control textarea-large" id="intro" name="intro" data-behaviour="ckeditor">{_f('intro', $teacher)}</textarea>
                {if isset($errors.intro)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.description)} error{/if}">
              <label class="control-label col-md-3" for="description">Descrizione</label>
              <div class="controls col-md-9">
                <textarea class="form-control textarea-large" id="description" name="description" data-behaviour="ckeditor">{_f('description', $teacher)}</textarea>
                {if isset($errors.description)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.pdf)} error{/if}">
              <label class="control-label col-md-3" for="pdf">Allegato</label>
              <div class="controls col-md-9">
                <input type="file" class="form-control" id="pdf" name="pdf" value="">
                {if _a($teacher, 'pdf')}
                <p class="mt20">
                  <input type="hidden" name="pdf" value="{$teacher.pdf}">
                  <a href="{$base_url}/assets/{$teacher.pdf}" target="_blank" class="btn btn-default">Download</a>
                </p>
                {/if}
                {if isset($errors.pdf)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
          </fieldset>
          <fieldset class="mt30">
            <legend>Impostazioni di pubblicazione</legend>
            <div class="form-group{if isset($errors.published)} error{/if}">
              <label class="control-label col-md-3" for="published">Pubblicato</label>
              <div class="controls col-md-9">
                <select name="published" class="form-control">
                  {html_options options=$PUBLISHED selected=_f('published', $teacher)}
                </select>
                {if isset($errors.published)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.published_datetime)} error{/if}">
              <label class="control-label col-md-3" for="published_datetime">Data/Ora di pubblicazione</label>
              <div class="controls col-md-9">
                {_datetime_combos("published_datetime", _d('published_datetime', $teacher), -1, 1)}
                {if isset($errors.published_datetime)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.published_datetime2)} error{/if}">
              <label class="control-label col-md-3" for="published_datetime2">Data/Ora di fine pubblicazione</label>
              <div class="controls col-md-9">
                {_datetime_combos("published_datetime2", _d('published_datetime2', $teacher), -1, 1)}
                {if isset($errors.published_datetime2)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.sorting)} error{/if}">
              <label class="control-label col-md-3" for="sorting">Ordinamento</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="sorting" name="sorting" value="{_f('sorting', $teacher)}">
                {if isset($errors.sorting)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
          </fieldset>
          <fieldset class="mt30">
            <legend>Impostazioni SEO</legend>
            <div class="form-group{if isset($errors.seo_title)} error{/if}">
              <label class="control-label col-md-3" for="seo_title">Titolo</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="seo_title" name="seo_title" value="{_f('seo_title', $teacher)}">
                {if isset($errors.seo_title)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.seo_image)} error{/if}">
              <label class="control-label col-md-3" for="seo_image">Immagine</label>
              <div class="controls col-md-9">
                <input type="file" class="form-control" id="seo_image" name="seo_image" value="">
                {if _a($teacher, 'seo_image')}
                <p class="mt20">
                  <input type="hidden" name="seo_image" value="{$teacher.seo_image}">
                  <img src="{$base_url}/assets/{$teacher.seo_image}" alt="{$teacher.name}" class="img-responsive img-editable" id="img_seo_image">
                </p>
                {/if}
                {if isset($errors.seo_image)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.seo_description)} error{/if}">
              <label class="control-label col-md-3" for="seo_description">Descrizione</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="seo_description" name="seo_description" value="{_f('seo_description', $teacher)}">
                {if isset($errors.seo_description)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
            <div class="form-group{if isset($errors.seo_keywords)} error{/if}">
              <label class="control-label col-md-3" for="seo_keywords">Keywords</label>
              <div class="controls col-md-9">
                <input type="text" class="form-control" id="seo_keywords" name="seo_keywords" value="{_f('seo_keywords', $teacher)}">
                {if isset($errors.seo_keywords)}<p class="help-block error">Campo obbligatorio!</p>{/if}
              </div>
            </div>
          </fieldset>
          <div class="form-actions">
            <button type="submit" class="btn btn-success">Salva le modifiche</button>
            <a href="{$base_url}/admin/teachers" class="btn btn-default">Annulla</a>
          </div>
        </form>
      </div>
    </div>
    {/block}
  </body>
</html>
