{extends "layout-base.tmpl"}

{block name=title}{_("Registrazione")} &mdash; Giappichelli{/block}

{block name="head"}
<meta name="description" content="{_("Registrazione")} &mdash; Giappichelli">
<meta name="author" content="">
{/block}

{block name="content"}
<div class="container content-section mt30 mb60">
  {if $confirm}
  <h2 class="text-center">
    <strong>{_("COMPLIMENTI!")}</strong>
  </h2>
  <hr>
  <div class="row mb20">
    <div class="col-sm-8 col-sm-offset-2">
      <h4 class="text-center">
        {_("LA TUA REGISTRAZIONE È STATA COMPLETATA.<br>TI ABBIAMO MANDATO UN’EMAIL DI VERIFICA.<br>CLICCA SUL LINK CHE TROVI E CONFERMA LA TUA REGISTRAZIONE.")}<br>
        {_("Se non ricevi l’email prova a controllare anche nella cartella spam.")}
      </h4>
    </div>
  </div>
  {else}
  <h2 class="text-center">
    <strong>{_("REGISTRATI")}</strong>
  </h2>
  <div class="row">
    <div class="col-sm-12">
      <form method="post" action="{$base_url}/registrazione">
        <div class="row">
          <div class="col-sm-6 mt10{if isset($errors.firstname)} has-error{/if}">
            <label class="form-control-static" for="firstname">{_("Nome")} <strong class="red">*</strong></label>
            <input type="text" name="firstname" class="form-control" value="{_post('firstname')}">
            {if isset($errors.firstname) and $errors.firstname eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
          <div class="col-sm-6 mt10{if isset($errors.lastname)} has-error{/if}">
            <label class="form-control-static" for="lastname">{_("Cognome")} <strong class="red">*</strong></label>
            <input type="text" name="lastname" class="form-control" value="{_post('lastname')}">
            {if isset($errors.lastname) and $errors.lastname eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 mt10{if isset($errors.email)} has-error{/if}">
            <label class="form-control-static" for="email">{_("Email")} <strong class="red">*</strong></label>
            <input type="text" name="email" class="form-control" value="{_post('email')}">
            {if isset($errors.email) and $errors.email eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
            {if isset($errors.email) and $errors.email eq 2}<p class="help-block has-error">{_("Email non valida: si prega di verificare nuovamente il dato inserito!")}</p>{/if}
            {if isset($errors.email) and $errors.email eq 3}<p class="help-block has-error">{_("Questo indirizzo email risulta già registrato a Giappichelli")}</p>{/if}
          </div>
          <div class="col-sm-6 mt10{if isset($errors.email2)} has-error{/if}">
            <label class="form-control-static" for="email2">{_("Conferma e-mail")} <strong class="red">*</strong></label>
            <input type="text" name="email2" class="form-control" value="{_post('email2')}">
            {if isset($errors.email2) and $errors.email2 eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
            {if isset($errors.email2) and $errors.email2 eq 2}<p class="help-block has-error">{_("Le email inserite non corrispondono!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 mt10{if isset($errors.password)} has-error{/if}">
            <label class="form-control-static" for="password">{_("Password")} <strong class="red">*</strong></label>
            <input type="password" name="password" class="form-control" value="">
            <p class="help-block"><em>{_("Minimo sei caratteri con almeno una lettera e una cifra; non deve contenere il tuo nome o indirizzo email.")}</em></p>
            {if isset($errors.password) and $errors.password eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
            {if isset($errors.password) and $errors.password eq 2}<p class="help-block has-error">{_("Password non valida: minimo sei caratteri con almeno una lettera e una cifra; non deve contenere il tuo nome o indirizzo email.")}</p>{/if}
          </div>
          <div class="col-sm-6 mt10{if isset($errors.password2)} has-error{/if}">
            <label class="form-control-static" for="password2">{_("Conferma password")} <strong class="red">*</strong></label>
            <input type="password" name="password2" class="form-control" value="">
            {if isset($errors.password2) and $errors.password2 eq 1}<p class="help-block has-error">{_("Le password inserite non corrispondono!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 mt10{if isset($errors.province)} has-error{/if}">
            <label class="form-control-static" for="province">{_("Provincia")}</label>
            <select class="form-control" id="province" name="province">
              <option value=""></option>
              {html_options options=$PROVINCES selected=_f('province', $user)}
            </select>
            {if isset($errors.province) and $errors.province eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
          <div class="col-sm-6 mt10{if isset($errors.city)} has-error{/if}">
            <label class="form-control-static" for="city">{_("Città")}</label>
            <input type="text" name="city" id="city" class="form-control" value="{_post('city')}">
            {if isset($errors.city) and $errors.city eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 mt10{if isset($errors.address)} has-error{/if}">
            <label class="form-control-static" for="address">{_("Indirizzo")}</label>
            <input type="text" name="address" class="form-control" value="{_post('address')}">
            {if isset($errors.address) and $errors.address eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
          <div class="col-sm-3 mt10{if isset($errors.zipcode)} has-error{/if}">
            <label class="form-control-static" for="zipcode">{_("CAP")}</label>
            <input type="text" name="zipcode" class="form-control" value="{_post('zipcode')}" data-behaviour="numeric">
            {if isset($errors.zipcode) and $errors.zipcode eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
          <div class="col-sm-3 mt10{if isset($errors.country)} has-error{/if}">
            <label class="form-control-static" for="country">{_("Nazione")}</label>
            <select class="form-control" id="country" name="country">
              {html_options options=$COUNTRIES selected=_f('country', $user)}
            </select>
            {if isset($errors.country) and $errors.country eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 mt10{if isset($errors.phone)} has-error{/if}">
            <label class="form-control-static" for="phone">{_("Telefono")}</label>
            <input type="text" name="phone" class="form-control" value="{_post('phone')}" data-behaviour="numeric">
            {if isset($errors.phone) and $errors.phone eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
          <div class="col-sm-6 mt10{if isset($errors.mobile)} has-error{/if}">
            <label class="form-control-static" for="mobile">{_("Cellulare")}</label>
            <input type="text" name="mobile" class="form-control" value="{_post('mobile')}" data-behaviour="numeric">
            {if isset($errors.mobile) and $errors.mobile eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 mt30{if isset($errors.roles)} has-error{/if}">
            <label class="form-control-static" for="roles">{_("Voglio iscrivermi come")} <strong class="red">*</strong></label>
            <div class="mtm10">
              {foreach $USER_ROLES as $k => $v}
              <div class="radio">
                <label>
                  <input type="radio" name="roles[]" value="{$k}"{if in_array($k, _f('roles', $user, []))} checked="checked"{/if}> {$v}
                </label>
              </div>
              {/foreach}
            </div>
            {if isset($errors.roles) and $errors.roles eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 mt30{if isset($errors.courses)} has-error{/if}">
            <label class="form-control-static" for="courses">{_("Iscritto al corso")} <strong class="red">*</strong></label>
            <div class="mtm10">
              {foreach $courses as $k => $v}
              <div class="radio">
                <label>
                  <input type="radio" name="courses[]" value="{$k}"{if in_array($k, _f('courses', $user, []))} checked="checked"{/if}> {$v}
                </label>
              </div>
              {/foreach}
            </div>
            {if isset($errors.courses) and $errors.roles eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <label class="form-control-static mt30" for="password">{_("CONDIZIONI GENERALI D'USO ED INFORMATIVA SULLA PRIVACY")} <strong class="red">*</strong></label>
        {*<textarea class="form-control" rows="5" disabled>{_("INFORMATIVA SULLA PRIVACY")}
...
        </textarea>*}
        <div class="checkbox{if isset($errors.privacy)} has-error{/if}">
          <div style="padding-left: 20px;">
            <input type="checkbox" name="privacy" value="1"{if _request('privacy')} checked{/if}>
            {_("Dichiaro di letto e accettato integralmente le")} <a href="{$base_url}/termini" rel="external">{_("Condizioni generali di uso")}</a>
            {_("e l'")}<a href="{$base_url}/privacy" rel="external">{_("informativa sulla privacy")}</a>.
            {if isset($errors.privacy) and $errors.privacy eq 1}<p class="help-block has-error mb10">{_("Campo obbligatorio!")}</p>{/if}
          </div>
        </div>
        <div class="checkbox mt10{if isset($errors.privacy2)} has-error{/if}">
          <label>
            <input type="checkbox" name="privacy2" value="1"{if _request('privacy2')} checked{/if}>
            {_("Acconsento al trattamento dei miei dati per ricevere comunicazioni di offerte promozionali mediante posta, telefono, posta elettronica, sms, mms, effettuare analisi statistiche, studi e ricerche di mercato da parte di Giappichelli.")}
          </label>
        </div>
        <div class="checkbox mt10{if isset($errors.newsletter)} has-error{/if}">
          <label>
            <input type="checkbox" name="newsletter" value="1"{if _request('newsletter')} checked{/if}>
            {_("Voglio ricevere la newsletter di Giappichelli.")}
          </label>
        </div>
        <p class="mt60 text-center">
          <button type="submit" class="btn btn-primary btn-lg">{_("REGISTRATI")}</button>
        </p>
      </form>
    </div>
    {/if}
  </div>
</div>
{/block}
