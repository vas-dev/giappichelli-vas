{extends "layout-base.tmpl"}

{block name=title}
  {$course.name} - Giappichelli
{/block}

{block name="head"}
  <meta property="og:url" content="{$base_url}{$request_uri}">
  <meta property="og:image" content="{if $course.image}{$cdn_url}/assets/{$course.image}{/if}">
{/block}

{block name="content"}
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    {$course.name}
                </div>
                {if $course.city}
                <div class="col-sm-6 text-right">
                    <i class="fa fa-map-marker text-secondary" aria-hidden="true"></i> {$course.city}
                </div>
                {/if}
            </div>
        </div>
    </div>
    {if $course.category}
    <div style='margin-top:15px' class="container text-light {if count($courses) >= 2}mt30{else}mt60{/if} mb60">
        {if count($courses) >= 2}
        <ul class="nav nav-pills nav-justified mb30">
            {foreach $courses as $c}
            <li role="presentation"{if $c.id eq $course.id} class="active"{/if}>
                <a href="{$base_url}/corsi/{$c.slug}">
                    {$c.category}
                </a>
            </li>
            {/foreach}
        </ul>
        {/if}
        <div class="row">
            <div class="col-sm-8">
                {if $parent_course.image}
                <div class="mb30">
                    <img src="{$cdn_url}/assets/{$parent_course.image}" class="img-responsive" alt="">
                </div>
                {/if}
                {if $course.description}
                <div class="h4 text-primary mb30{if $parent_course.image} mt60{/if}">
                    Descrizione
                </div>
                <div class="mb30">
                    {$course.description}
                </div>
                {/if}
                {if $course.calendar}
                <div class="h4 text-primary mb30 mt60">
                    Programma
                </div>
                <div class="mb30">
                    {$course.calendar}
                </div>
                {/if}
                {if $course.pdf}
                <div class="h4 text-primary mb30 mt60">
                    Materiale didattico
                </div>
                <div class="mb30">
                    <a href="{$cdn_url}/assets/{$course.pdf}" target="_blank">
                        <i class="fa fa-download"></i>
                        Scarica il materiale didattico
                    </a>
                </div>
                {/if}
                {if $pubblications}
                <div class="h4 text-primary mb30 mt60">
                    Altre letture consigliate
                </div>
                <div class="row mb60">
                    {foreach $pubblications as $pubblication}
                    <div class="col-sm-3 mb30">
                        {if $pubblication.image}
                        <a href="{$pubblication.url}" target="_blank">
                            <img src="{$cdn_url}/assets/{$pubblication.image}" alt="" class="img-responsive">
                        </a>
                        {/if}
                        <div class="mt20 text-primary">
                            {$pubblication.name}
                        </div>
                    </div>
                    {/foreach}
                </div>
                {/if}
                {if $course.features_form}
                <hr>
                <a name="form"></a>
                <div class="h4 text-primary mb30 mt60">
                    Invia una richiesta di informazioni
                </div>
                {if $confirm}
                <div class="alert alert-success">
                  <strong>CONFERMA</strong>: il messaggio è stato inviato correttamente.
                </div>
                {else}
                <form method="post" action="{$base_url}{$request_uri}#form">
                  <div class="row">
                    <div class="col-sm-6 mt10{if isset($errors.firstname)} has-error{/if}">
                      <label class="form-control-static" for="firstname">{_("Nome")} <strong class="red">*</strong></label>
                      <input type="text" name="firstname" class="form-control" value="{_post('firstname')}" required>
                      {if isset($errors.firstname) and $errors.firstname eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
                    </div>
                    <div class="col-sm-6 mt10{if isset($errors.lastname)} has-error{/if}">
                      <label class="form-control-static" for="lastname">{_("Cognome")} <strong class="red">*</strong></label>
                      <input type="text" name="lastname" class="form-control" value="{_post('lastname')}" required>
                      {if isset($errors.lastname) and $errors.lastname eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 mt10{if isset($errors.email)} has-error{/if}">
                      <label class="form-control-static" for="email">{_("Email")} <strong class="red">*</strong></label>
                      <input type="text" name="email" class="form-control" value="{_post('email')}" required>
                      {if isset($errors.email) and $errors.email eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
                      {if isset($errors.email) and $errors.email eq 2}<p class="help-block has-error">{_("Email non valida: si prega di verificare nuovamente il dato inserito!")}</p>{/if}
                      {if isset($errors.email) and $errors.email eq 3}<p class="help-block has-error">{_("Questo indirizzo email risulta già registrato a Giappichelli")}</p>{/if}
                    </div>
                    <div class="col-sm-6 mt10{if isset($errors.phone)} has-error{/if}">
                      <label class="form-control-static" for="phone">{_("Numero di telefono")} <strong class="red">*</strong></label>
                      <input type="text" name="phone" class="form-control" value="{_post('phone')}" required>
                      {if isset($errors.phone) and $errors.phone eq 1}<p class="help-block has-error">{_("Campo obbligatorio!")}</p>{/if}
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12 mt10{if isset($errors.message)} has-error{/if}">
                      <label class="form-control-static" for="phone">{_("Messaggio")} <strong class="red">*</strong></label>
                      <textarea name="message" class="form-control" rows="5" required>{_post('message')}</textarea>
                    </div>
                  </div>
                  <label class="form-control-static mt30" for="password">{_("CONDIZIONI GENERALI D'USO ED INFORMATIVA SULLA PRIVACY")} <strong class="red">*</strong></label>
                  <div class="checkbox{if isset($errors.privacy)} has-error{/if}">
                    <div style="padding-left: 20px;">
                      <input type="checkbox" name="privacy" value="1"{if _request('privacy')} checked{/if}>
                      {_("Dichiaro di letto e accettato integralmente le")} <a href="{$base_url}/termini" rel="external">{_("Condizioni generali di uso")}</a>
                      {_("e l'")}<a href="{$base_url}/privacy" rel="external">{_("informativa sulla privacy")}</a>.
                      {if isset($errors.privacy) and $errors.privacy eq 1}<p class="help-block has-error mb10">{_("Campo obbligatorio!")}</p>{/if}
                    </div>
                  </div>
                  <div class="checkbox mt10{if isset($errors.privacy2)} has-error{/if}">
                    <label>
                      <input type="checkbox" name="privacy2" value="1"{if _request('privacy2')} checked{/if}>
                      {_("Acconsento al trattamento dei miei dati per ricevere comunicazioni di offerte promozionali mediante posta, telefono, posta elettronica, sms, mms, effettuare analisi statistiche, studi e ricerche di mercato da parte di Giappichelli.")}
                    </label>
                  </div>
                  <div class="checkbox mt10{if isset($errors.newsletter)} has-error{/if}">
                    <label>
                      <input type="checkbox" name="newsletter" value="1"{if _request('newsletter')} checked{/if}>
                      {_("Voglio ricevere la newsletter di Giappichelli.")}
                    </label>
                  </div>
                  <p class="mt60 text-center">
                    <button type="submit" class="btn btn-primary btn-lg">{_("AVANTI")}</button>
                  </p>
                </form>
                {/if}
                {/if}
            </div>
            <div class="col-sm-4">
                {if $course.place}
                <div class="row mt30">
                    <div class="col-xs-1">
                        <i class="fa fa-map-marker text-secondary" aria-hidden="true"></i>
                    </div>
                    <div class="col-xs-11">
                        {$course.place}
                    </div>
                </div>
                {/if}
                {if $course.duration}
                <div class="mt30">
                    {$course.duration}
                </div>
                {/if}
                {if $course.category}
                <div class="mt30">
                    {if $course.form}
                    <a class="btn btn-default btn-block" href="{$base_url}/download/{$course.form}?fn=Modulo+iscrizione.pdf" target="_blank">
                        {$course.button_label|default:"MODULO DI ISCRIZIONE"}
                    </a>
                    {elseif $course.button_link}
                    <a class="btn btn-default btn-block" href="{$course.button_link}" target="_blank">
                        {$course.button_label|default:"ISCRIVITI"}
                    </a>
                    {else}
                    <a class="btn btn-default btn-block" href="{$base_url}/registrazione?corso={$course.id}">
                        {$course.button_label|default:"ISCRIVITI"}
                    </a>
                    {/if}
                </div>
                {/if}
                {if $teachers or $course.prices}
                <div class="well well-gray mt30 ">
                    {foreach $teachers as $i => $teacher}
                        {if $i}
                        <hr class="mt30 mb30">
                        {/if}
                        <div class="mt30">
                            <a href="{$base_url}/docenti/{$teacher.slug}">{$teacher.name}</a>
                        </div>
                    {/foreach}
                    {if $course.prices}
                    {if $teachers}
                    <hr class="mt30 mb30">
                    {/if}
                    <div class="mt30">
                        {$course.prices}
                    </div>
                    {/if}
                </div>
                {/if}
            </div>
        </div>
    </div>
    {else}
    <div class="container mt60 mb60">
        <div class="row">
            <div class="col-sm-{if $course.image}6{else}12{/if}">
                {$course.description}
            </div>
            {if $course.image}
            <div class="col-sm-6">
                <img src="{$cdn_url}/assets/{$course.image}" class="img-responsive" alt="">
            </div>
            {/if}
        </div>
    </div>
    <div class="content-gray">
        <div class="container">
            <div class="row mb60">
                {assign "linked_courses" $course->get_linked_courses()}
                {foreach $linked_courses as $course}
                <div class="{if count($linked_courses) eq 1}col-sm-4 col-sm-offset-4{elseif count($linked_courses) eq 2}col-sm-6{else}col-sm-4{/if} mt30">
                    <div class="well well-course">
                        {if $course.image}
                        <div class="text-center">
                            <a href="{$base_url}/corsi/{$course.slug}">
                                <img src="{$cdn_url}/assets/{$course.image}" alt="" class="img-responsive" style="width: 100%;">
                            </a>
                        </div>
                        {/if}
                        <div class="well-gray" style='min-height:16em;'>
                            <div class="row">
                                <div class="col-xs-6 text-left">
                                    <strong class="text-primary">{$course.category|mb_strtoupper}</strong>
                                </div>
                                <div class="col-xs-6 text-right text-primary">
                                    <i class="fa fa-map-marker text-secondary" aria-hidden="true"></i>
                                    <strong class="text-primary">{$course.city|mb_strtoupper}</strong>
                                </div>
                            </div>
                            <p class="mt20">
                                {$course.preview}
                            </p>
                        </div>
                        <div class="well-white text-center">
                            <a class="btn btn-default" href="{$base_url}/corsi/{$course.slug}">
                                SCOPRI DI PIÙ
                            </a>
                        </div>
                    </div>
                </div>
                {/foreach}
            </div>
        </div>
    </div>
    {/if}
{/block}