<?php
  require("includes/loader.inc.php");

  $request_uri = "/";
  $request_parts = array();

  require("includes/smarty.inc.php");

  define('MEBB_IGNORE_ERRORS', false);
  define('MEBB_TEMPLATE_EXTENSION', 'tmpl');

  function glob_recursive($pattern, $flags = 0){
    $files = glob($pattern, $flags);
        
    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir){
      $files = array_merge($files, glob_recursive($dir.'/'.basename($pattern), $flags));
    }
        
    return $files;
  }

  function compile($smarty, $files = null, &$info = array()){
    if(!$files) $files = $smarty->getTemplateDir();
    if(!is_array($files)) $files = array($files);
  
    $info['errors'] = array();
  
    $extension = (defined('MEBB_TEMPLATE_EXTENSION')?MEBB_TEMPLATE_EXTENSION:'tpl');
    $files_to_compile = array();
    $compiled = array();
  
    foreach($files as &$file){
      if($file = realpath($file)){
        if(is_dir($file)){
          $tmp = glob_recursive($file.DIRECTORY_SEPARATOR.'*.'.$extension);
          foreach($tmp as $file){
            $files_to_compile[] = $file;
          }
        }else if(is_file($file)){
          $files_to_compile[] = $file;
        }
      }
    }
    foreach($files_to_compile as &$file){
      try{
        $template = $smarty->createTemplate($file, $smarty);
        if($template->mustCompile() || true){
          $source = $template->compiler->compileTemplate($template);  
        }else{
          $source = file_get_contents($file);
        }
        $compiled[] = array(
          'source' => $source,
          'file_original' => $file,
          'file_compiled' => $template->compiled->filepath
        );
      }catch(\Exception $e){
        $info['errors'][] = array(
          'exception' => $e,
          'file' => $file,
          'message' => $e->getMessage()
        );
        if(defined('MEBB_IGNORE_ERRORS')){
          if(!MEBB_IGNORE_ERRORS){
            throw $e;
          }
        }
      }
    }
    return $compiled;
  }
  
  function save($smarty, $compiled_templates, $file = null, $empty_file = true){
    if(!$file) $file = tempnam(sys_get_temp_dir(), 'i18n_'); 
    if($empty_file && file_exists($file)) unlink($file);
  
    $handle = fopen($file, "w");
    foreach($compiled_templates as $source){
      fwrite($handle, $source['source']);
    }
    fclose($handle);
  
    return $file;
  }
  
  function save_individual($smarty, $compiled_templates, $directory = null){
    if(!$directory) {
      $directory = sys_get_temp_dir();
      $directory = rtrim(rtrim($directory, '/'),'\\').DIRECTORY_SEPARATOR.'i18n_'.rand(pow(10,6),pow(10,7)).DIRECTORY_SEPARATOR;
    }
    if(!file_exists($directory)) mkdir($directory, 0777, true);
    $template_directories = $smarty->getTemplateDir();
  
    foreach($template_directories as &$template_directory){
      $template_directory = realpath($template_directory); 
    }
  
    foreach($compiled_templates as $source){
      $file = $source['file_original']; 
      foreach($template_directories as $template_directory){
        $file = str_replace($template_directory, '', $file);
      }
      $file = ltrim($file, DIRECTORY_SEPARATOR);
      $file = str_replace(DIRECTORY_SEPARATOR, '-', $file); 
      file_put_contents($directory.$file, $source['source']);
    }
  
    return $directory;
  }

  print 'Compiling...' . PHP_EOL;

  $info = array();
  $sources = compile($smarty, null, $info);
  $directory = save_individual($smarty, $sources, "locales/source/");

  print 'The following templates have been compiled into '.$directory.':'.PHP_EOL;
  foreach ($sources as $source){
    print '  - '.$source['file_original'].PHP_EOL;
  }

  if (count($info['errors'])>0) {
    print PHP_EOL.PHP_EOL.'The following errors have occured:'.PHP_EOL;
    foreach ($info['errors'] as $error) {
      $exception = $error['exception'];
      $file = $error['file'];
      print $file.' has the following error: '.$error['message'].PHP_EOL;
    }
  }
