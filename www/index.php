<?php
  require("includes/loader.inc.php");

 /**************************************************************************
  * CONTROLLERS
  **************************************************************************/

  $request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
  $request_parts = explode("/", trim($request_uri, "/"));
  $request_uri = "/" . implode("/", $request_parts);

  require("includes/smarty.inc.php");

  $redirect = models\Redirect::get_by_request_uri($request_uri);
  if ($redirect) {
    if (substr($redirect['redirect_uri'], 0, 4) === "http") {
      header("Location: " . $redirect['redirect_uri']);
      exit;
    } else {
      header("Location: " . BASE_URL . $redirect['redirect_uri']);
      exit;
    }
  }

  $principal = models\User::identify();
  $smarty->assign("principal", $principal);
  $smarty->assign("pcookie", !isset($_COOKIE['pcookie']));

  # CMS (slug-based)
  if ($page = models\Page::get_by_slug($request_uri)) {
    require("controllers/cms.inc.php"); exit;
  }
  # /landing
  else if (count($request_parts) == 1 && $request_parts[0] == 'landing') {
    require("controllers/landing.inc.php"); exit;
  }
  # /registrazione
  else if (count($request_parts) == 1 && $request_parts[0] == 'registrazione') {
    require("controllers/registrazione.inc.php"); exit;
  }
  # /registrazione/:id/:hash
  else if (count($request_parts) == 3 && $request_parts[0] == 'registrazione' && ctype_digit($request_parts[1])) {
    $id = $request_parts[1];
    $hash = $request_parts[2];
    require("controllers/registrazione-conferma.inc.php"); exit;
  }
  # /password
  else if (count($request_parts) == 1 && $request_parts[0] == 'password') {
    require("controllers/password.inc.php"); exit;
  }
  # /password/:id/:hash
  else if (count($request_parts) == 3 && $request_parts[0] == 'password' && ctype_digit($request_parts[1])) {
    $id = $request_parts[1];
    $hash = $request_parts[2];
    require("controllers/password-conferma.inc.php"); exit;
  }
  # /login
  else if (count($request_parts) == 1 && $request_parts[0] == 'login') {
    require("controllers/login.inc.php"); exit;
  }
  # /logout
  else if (count($request_parts) == 1 && $request_parts[0] == 'logout') {
    require("controllers/logout.inc.php"); exit;
  }
  # /news
  else if (count($request_parts) == 1 && $request_parts[0] == 'news') {
    $category = null;
    $tag = null;
    $year = null;
    $month = null;
    require("controllers/news.inc.php"); exit;
  }
  # /news/{:year}
  else if (count($request_parts) == 2 && $request_parts[0] == 'news' && ctype_digit($request_parts[1])) {
    $category = null;
    $tag = null;
    $year = $request_parts[1]*1;
    $month = null;
    require("controllers/news.inc.php"); exit;
  }
  # /news/{:year}/{:month}
  else if (count($request_parts) == 3 && $request_parts[0] == 'news' && ctype_digit($request_parts[1]) && ctype_digit($request_parts[2])) {
    $category = null;
    $tag = null;
    $year = $request_parts[1]*1;
    $month = $request_parts[2]*1;
    require("controllers/news.inc.php"); exit;
  }
  # /news/categorie/{:category}
  else if (count($request_parts) == 3 && $request_parts[0] == 'news' && $request_parts[1] == 'categorie') {
    $category = $request_parts[2];
    $tag = null;
    $year = null;
    $month = null;
    require("controllers/news.inc.php"); exit;
  }
  # /news/tags/{:tag}
  else if (count($request_parts) == 3 && $request_parts[0] == 'news' && $request_parts[1] == 'tags') {
    $category = null;
    $tag = $request_parts[2];
    $year = null;
    $month = null;
    require("controllers/news.inc.php"); exit;
  }
  # /news/{:slug}
  else if (count($request_parts) == 2 && $request_parts[0] == 'news' && ctype_digit(_unslug($request_parts[1]))) {
    $article_id = _unslug($request_parts[1]);
    require("controllers/news-dettaglio.inc.php"); exit;
  }
  # /docenti/{:slug}
  else if (count($request_parts) == 2 && $request_parts[0] == 'docenti' && ($teacher = models\Teacher::get_by_slug($request_parts[1]))) {
    require("controllers/docenti-dettaglio.inc.php"); exit;
  }
  # /corsi/{:slug}
  else if (count($request_parts) == 2 && $request_parts[0] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[1]))) {
    require("controllers/corsi-dettaglio.inc.php"); exit;
  }
  # /profilo
  else if (count($request_parts) == 1 && $request_parts[0] == 'profilo') {
    require("controllers/profilo.inc.php"); exit;
  }
  # /profilo/audio
  else if (count($request_parts) == 3 && $request_parts[0] == 'profilo' && $request_parts[1] == 'audio' && ctype_digit($request_parts[2])) {
    $id = $request_parts[2];
    require("controllers/profilo-audio.inc.php"); exit;
  }
  # /profilo/modifica
  else if (count($request_parts) == 2 && $request_parts[0] == 'profilo' && $request_parts[1] == 'modifica') {
    require("controllers/profilo-modifica.inc.php"); exit;
  }
  # /download/{path}
  else if (count($request_parts) >= 2 && $request_parts[0] == 'download') {
    $path = implode("/", array_slice($request_parts, 1));
    require("controllers/download.inc.php"); exit;
  }
  # /profilo/corsi/:slug
  else if (count($request_parts) == 3 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2]))) {
    require("controllers/profilo-corsi.inc.php"); exit;
  }
  # /profilo/corsi/{:slug}/{:lesson}
  else if (count($request_parts) == 4 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) &&
           ctype_digit($request_parts[3]) && ($lesson = models\Lesson::get_by_course_and_id($course['id'], $request_parts[3]))) {
    require("controllers/profilo-corsi-lezione.inc.php"); exit;
  }
  # /profilo/corsi/:slug/{:lesson}/domande
  else if (count($request_parts) == 5 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) &&
           ctype_digit($request_parts[3]) && ($lesson = models\Lesson::get_by_course_and_id($course['id'], $request_parts[3])) && $request_parts[4] == 'domande') {
    require("controllers/profilo-corsi-domande.inc.php"); exit;
  }
  # /profilo/corsi/:slug/compiti
  else if (count($request_parts) == 4 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) && $request_parts[3] == 'compiti') {
    require("controllers/profilo-corsi-compiti.inc.php"); exit;
  }
  # /profilo/corsi/:slug/compiti
  else if (count($request_parts) == 5 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) && $request_parts[3] == 'compiti' && $request_parts[4] == 'aggiungi') {
    require("controllers/profilo-corsi-compiti-aggiungi.inc.php"); exit;
  }
  # /profilo/corsi/:slug/compiti/aggiungi
  else if (count($request_parts) == 5 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) && $request_parts[3] == 'compiti' && ($homework = models\Homework::get_by_id($request_parts[4]))) {
    require("controllers/profilo-corsi-compiti-modifica.inc.php"); exit;
  }
  # /profilo/corsi/:slug/compiti/elimina
  else if (count($request_parts) == 6 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) && $request_parts[3] == 'compiti' && ($homework = models\Homework::get_by_id($request_parts[4])) && $request_parts[5] == 'elimina') {
    require("controllers/profilo-corsi-compiti-elimina.inc.php"); exit;
  }
  # /profilo/corsi/:slug/compiti/download
  else if (count($request_parts) == 6 && $request_parts[0] == 'profilo' && $request_parts[1] == 'corsi' && ($course = models\Course::get_by_slug($request_parts[2])) && $request_parts[3] == 'compiti' && ($homework = models\Homework::get_by_id($request_parts[4])) && $request_parts[5] == 'download') {
    require("controllers/profilo-corsi-compiti-download.inc.php"); exit;
  }
  # /js/translations
  else if (count($request_parts) == 2 && $request_parts[0] == 'js' && $request_parts[1] == 'translations.js') {
    require("controllers/js-translations.inc.php"); exit;
  }
  # /sitemap.xml
  else if (count($request_parts) == 1 && $request_parts[0] == 'sitemap.xml') {
    require("controllers/sitemap.inc.php"); exit;
  }
  # /resize
  else if (count($request_parts) > 1 && $request_parts[0] == 'resize') {
    require("controllers/resize.inc.php"); exit;
  }
  # /admin/...
  else if (count($request_parts) >= 1 && $request_parts[0] == 'admin') {
    $request_parts = array_slice($request_parts, 1);

    $principal = models\UserBE::identify();
    $smarty->assign("principal", $principal);

    # /
    if (count($request_parts) == 0 ||
        count($request_parts) == 1 && $request_parts[0] == '' ||
        count($request_parts) == 1 && $request_parts[0] == 'login') {
      require("controllers/admin-login.inc.php"); exit;
    }
    # /logout
    else if (count($request_parts) == 1 && $request_parts[0] == 'logout' || !$principal) {
      require("controllers/admin-logout.inc.php"); exit;
    }
    # /upload
    else if (count($request_parts) == 1 && $request_parts[0] == 'upload' || !$principal) {
      require("controllers/admin-upload.inc.php"); exit;
    }
    # /banners
    else if (count($request_parts) == 1 && $request_parts[0] == 'banners') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-banners.inc.php"); exit;
    }
    # /banners/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'banners' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-banners-add.inc.php"); exit;
    }
    # /banners/{:banner_id}/edit
    else if (count($request_parts) == 2 && $request_parts[0] == 'banners' ||
             count($request_parts) == 3 && $request_parts[0] == 'banners' && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-banners-edit.inc.php"); exit;
    }
    # /banners/{:banner_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'banners' && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-banners-delete.inc.php"); exit;
    }
    # /teachers
    else if (count($request_parts) == 1 && $request_parts[0] == 'teachers') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-teachers.inc.php"); exit;
    }
    # /teachers/{:teacher_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'teachers' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'teachers' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $teacher_id = $request_parts[1];
      require("controllers/admin-teachers-edit.inc.php"); exit;
    }
    # /teachers/{:teacher_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'teachers' && ctype_digit($request_parts[1]) && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $teacher_id = $request_parts[1];
      require("controllers/admin-teachers-delete.inc.php"); exit;
    }
    # /teachers/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'teachers' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-teachers-add.inc.php"); exit;
    }
    # /teachers/upload
    else if (count($request_parts) == 2 && $request_parts[0] == 'teachers' && $request_parts[1] == 'upload') {
      if (!in_array($principal['role'], array(1, 2, 3, 4))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-teachers-upload.inc.php"); exit;
    }
    # /pubblications
    else if (count($request_parts) == 1 && $request_parts[0] == 'pubblications') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-pubblications.inc.php"); exit;
    }
    # /pubblications/{:pubblication_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'pubblications' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'pubblications' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $pubblication_id = $request_parts[1];
      require("controllers/admin-pubblications-edit.inc.php"); exit;
    }
    # /pubblications/{:pubblication_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'pubblications' && ctype_digit($request_parts[1]) && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $pubblication_id = $request_parts[1];
      require("controllers/admin-pubblications-delete.inc.php"); exit;
    }
    # /pubblications/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'pubblications' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-pubblications-add.inc.php"); exit;
    }
    # /pubblications/upload
    else if (count($request_parts) == 2 && $request_parts[0] == 'pubblications' && $request_parts[1] == 'upload') {
      if (!in_array($principal['role'], array(1, 2, 3, 4))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-pubblications-upload.inc.php"); exit;
    }
    # /courses
    else if (count($request_parts) == 1 && $request_parts[0] == 'courses') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-courses.inc.php"); exit;
    }
    # /courses/{:course_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'courses' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'courses' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $course_id = $request_parts[1];
      require("controllers/admin-courses-edit.inc.php"); exit;
    }
    # /courses/{:course_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'courses' && ctype_digit($request_parts[1]) && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $course_id = $request_parts[1];
      require("controllers/admin-courses-delete.inc.php"); exit;
    }
    # /courses/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'courses' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-courses-add.inc.php"); exit;
    }
    # /courses/upload
    else if (count($request_parts) == 2 && $request_parts[0] == 'courses' && $request_parts[1] == 'upload') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-courses-upload.inc.php"); exit;
    }
    # /lessons
    else if (count($request_parts) == 1 && $request_parts[0] == 'lessons') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-lessons.inc.php"); exit;
    }
    # /lessons/upload
    else if (count($request_parts) == 2 && $request_parts[0] == 'lessons' && $request_parts[1] == 'upload') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-lessons-upload.inc.php"); exit;
    }
    # /lessons/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'lessons' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-lessons-add.inc.php"); exit;
    }
    # /lessons/{:lesson_id}/edit
    else if (count($request_parts) == 2 && $request_parts[0] == 'lessons' ||
             count($request_parts) == 3 && $request_parts[0] == 'lessons' && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-lessons-edit.inc.php"); exit;
    }
    # /lessons/{:lesson_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'lessons' && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-lessons-delete.inc.php"); exit;
    }
    # /pages
    else if (count($request_parts) == 1 && $request_parts[0] == 'pages') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-pages.inc.php"); exit;
    }
    # /pages/{:page_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'pages' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'pages' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $page_id = $request_parts[1];
      require("controllers/admin-pages-edit.inc.php"); exit;
    }
    # /pages/{:page_id}/add
    else if (count($request_parts) == 3 && $request_parts[0] == 'pages' && ctype_digit($request_parts[1]) && $request_parts[2] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $page_id = $request_parts[1];
      $content_id = null;
      require("controllers/admin-pages-edit-content.inc.php"); exit;
    }
    # /pages/{:page_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'pages' && ctype_digit($request_parts[1]) && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $page_id = $request_parts[1];
      require("controllers/admin-pages-delete.inc.php"); exit;
    }
    # /pages/{:page_id}/{:content_id}
    else if (count($request_parts) == 3 && $request_parts[0] == 'pages' && ctype_digit($request_parts[1]) && $request_parts[2]) {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $page_id = $request_parts[1];
      $content_id = $request_parts[2];
      require("controllers/admin-pages-edit-content.inc.php"); exit;
    }
    # /pages/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'pages' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-pages-add.inc.php"); exit;
    }
    # /pages/upload
    else if (count($request_parts) == 2 && $request_parts[0] == 'pages' && $request_parts[1] == 'upload') {
      if (!in_array($principal['role'], array(1, 2, 3, 4))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-pages-upload.inc.php"); exit;
    }
    # /menus
    else if (count($request_parts) == 1 && $request_parts[0] == 'menus') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-menus.inc.php"); exit;
    }
    # /menus/{:menu_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'menus' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'menus' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $menu_id = $request_parts[1];
      require("controllers/admin-menus-edit.inc.php"); exit;
    }
    # /menus/{:menu_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'menus' && ctype_digit($request_parts[1]) && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $menu_id = $request_parts[1];
      require("controllers/admin-menus-delete.inc.php"); exit;
    }
    # /menus/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'menus' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-menus-add.inc.php"); exit;
    }
    # /users-be
    else if (count($request_parts) == 1 && $request_parts[0] == 'users-be') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-users-be.inc.php"); exit;
    }
    # /users-be/{:user_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'users-be' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'users-be' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $user_id = $request_parts[1];
      require("controllers/admin-users-be-edit.inc.php"); exit;
    }
    # /users-be/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'users-be' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-users-be-add.inc.php"); exit;
    }
    # /users
    else if (count($request_parts) == 1 && $request_parts[0] == 'users') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-users.inc.php"); exit;
    }
    # /users/{:user_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'users' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'users' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $user_id = $request_parts[1];
      require("controllers/admin-users-edit.inc.php"); exit;
    }
    # /users/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'users' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-users-add.inc.php"); exit;
    }
    # /redirects
    else if (count($request_parts) == 1 && $request_parts[0] == 'redirects') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-redirects.inc.php"); exit;
    }
    # /redirects/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'redirects' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-redirects-add.inc.php"); exit;
    }
    # /redirects/{:redirect_id}/edit
    else if (count($request_parts) == 2 && $request_parts[0] == 'redirects' ||
             count($request_parts) == 3 && $request_parts[0] == 'redirects' && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-redirects-edit.inc.php"); exit;
    }
    # /redirects/{:redirect_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'redirects' && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-redirects-delete.inc.php"); exit;
    }
    # /translations
    else if (count($request_parts) == 1 && $request_parts[0] == 'translations') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-translations.inc.php"); exit;
    }
    # /blog-categories
    else if (count($request_parts) == 1 && $request_parts[0] == 'blog-categories') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-blog-categories.inc.php"); exit;
    }
    # /blog-categories/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'blog-categories' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-blog-categories-add.inc.php"); exit;
    }
    # /blog-categories/{:category_id}/edit
    else if (count($request_parts) == 2 && $request_parts[0] == 'blog-categories' ||
             count($request_parts) == 3 && $request_parts[0] == 'blog-categories' && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-blog-categories-edit.inc.php"); exit;
    }
    # /blog-categories/{:category_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'blog-categories' && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-blog-categories-delete.inc.php"); exit;
    }
    # /blog-articles
    else if (count($request_parts) == 1 && $request_parts[0] == 'blog-articles') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-blog-articles.inc.php"); exit;
    }
    # /blog-articles/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'blog-articles' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-blog-articles-add.inc.php"); exit;
    }
    # /blog-articles/upload
    else if (count($request_parts) == 2 && $request_parts[0] == 'blog-articles' && $request_parts[1] == 'upload') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-blog-articles-upload.inc.php"); exit;
    }
    # /blog-articles/{:article_id}/edit
    else if (count($request_parts) == 2 && $request_parts[0] == 'blog-articles' ||
             count($request_parts) == 3 && $request_parts[0] == 'blog-articles' && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-blog-articles-edit.inc.php"); exit;
    }
    # /blog-articles/{:article_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'blog-articles' && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-blog-articles-delete.inc.php"); exit;
    }
    # /blog-comments
    else if (count($request_parts) == 1 && $request_parts[0] == 'blog-comments') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-blog-comments.inc.php"); exit;
    }
    # /blog-comments/{:comment_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'blog-comments' ||
             count($request_parts) == 3 && $request_parts[0] == 'blog-comments' && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $id = $request_parts[1];
      require("controllers/admin-blog-comments-edit.inc.php"); exit;
    }
    # /groups
    else if (count($request_parts) == 1 && $request_parts[0] == 'groups') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-groups.inc.php"); exit;
    }
    # /groups/{:group_id}
    else if (count($request_parts) == 2 && $request_parts[0] == 'groups' && ctype_digit($request_parts[1]) ||
             count($request_parts) == 3 && $request_parts[0] == 'groups' && ctype_digit($request_parts[1]) && $request_parts[2] == 'edit') {
      if (!in_array($principal['role'], array(1, 2))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $group_id = $request_parts[1];
      require("controllers/admin-groups-edit.inc.php"); exit;
    }
    # /groups/{:group_id}/delete
    else if (count($request_parts) == 3 && $request_parts[0] == 'groups' && ctype_digit($request_parts[1]) && $request_parts[2] == 'delete') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      $group_id = $request_parts[1];
      require("controllers/admin-groups-delete.inc.php"); exit;
    }
    # /groups/add
    else if (count($request_parts) == 2 && $request_parts[0] == 'groups' && $request_parts[1] == 'add') {
      if (!in_array($principal['role'], array(1))) {
        header("Location: " . BASE_URL . "/admin/login"); exit;
      }
      require("controllers/admin-groups-add.inc.php"); exit;
    }
  }

  # default (not found)
  not_found();
