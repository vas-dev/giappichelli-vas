<?php

  namespace models;

  class BlogTag extends \ArrayObject {

    static public function add($tag) {
      global $db;
      mysqli_query($db, "INSERT INTO blog_tags (".
                    "slug,".
                    "tag,".
                    "created_at) ".
                  "VALUES (".
                    _text(_slug($tag)) . ", " .
                    _text($tag) . ", " .
                    "NOW());") or
                  die("query error in BlogTag::add: " . mysqli_error($db));
      return BlogTag::get_by_tag($tag);
    }

    static public function get() {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_tags ".
                        "ORDER BY tag;") or
            die("query error in BlogTag::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new BlogTag($r));
      }
      return $results;
    }

    static public function get_by_slug($slug) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_tags ".
                        "WHERE deleted_at IS NULL AND slug = " . _text($slug) . ";") or
            die("query error in BlogTag::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new BlogTag($r);
      }
    }

    static public function get_by_tag($tag) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_tags ".
                        "WHERE deleted_at IS NULL AND tag = " . _text($tag) . ";") or
            die("query error in BlogTag::get_by_tag: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new BlogTag($r);
      }
    }

    static public function get_or_add($tag) {
      $obj = BlogTag::get_by_tag($tag);
      if (!$obj) {
        $obj = BlogTag::add($tag);
      }
      return $obj;
    }

    static public function get_by_article($article) {
      global $db;
      $rs = mysqli_query($db, "SELECT bt.* ".
                        "FROM blog_tags AS bt ".
                        "JOIN blog_articles_tags AS bat ON (bt.id = bat.tag_id) ".
                        "WHERE bt.deleted_at IS NULL AND bat.deleted_at IS NULL AND ".
                             " bat.article_id = " . _integer($article['id']) . " ".
                        "ORDER BY bt.tag;") or
            die("query error in BlogTag::get_by_article: " . mysqli_error($db));
      $result = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($result, new BlogTag($r));
      }
      return $result;
    }

    public function not_empty($category, $params=array()) {
      global $memcached;
      $k = "GIAPPICHELLI:BlogTag.not_empty:" . $this['id'] . ":" . json_encode($params);
      $result = $memcached ? $memcached->get($k) : null;
      if ($result) {
        return $result - 1;
      }
      $params['tag'] = $this;
      $params['published'] = 1;
      $result = BlogArticle::get_count($params) > 0;
      if ($memcached) {
        $memcached->set($k, (int)$result + 1, 300);
      }
      return $result;
    }

  }

?>
