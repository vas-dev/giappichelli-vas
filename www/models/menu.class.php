<?php

  namespace models;

  class Menu extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO menus (".
                        "lang,".
                        "slug,".
                        "content,".
                        "published,".
                        "published_datetime,".
                        "published_datetime2,".
                        "created_at".
                      ") VALUES (".
                        _text(_a($params, 'lang')) . ", ".
                        _text(_a($params, 'slug')) . ", ".
                        _text(_a($params, 'content')) . ", ".
                        _integer(_a($params, 'published')) . ", ".
                        _datetime(_a($params, 'published_datetime')) . ", ".
                        _datetime(_a($params, 'published_datetime2')) . ", ".
                        "NOW()".
                      ");") 
                      or die("query error in Menu::add: " . mysqli_error($db));
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM menus WHERE id = " . _integer($id) . ";") or
            die("query error in Menu::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Menu($r);
      }
      return null;
    }

    static public function get_by_slug($slug) {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * FROM menus ".
                              "WHERE slug = " . _text($slug) . " ".
                              "AND lang = " . _text($lang) . " ".
                              "AND published = 1 ".
                              "AND (published_datetime IS NULL or published_datetime <= NOW()) ".
                              "AND (published_datetime2 IS NULL or published_datetime2 > NOW()) ".
                              "ORDER BY id DESC LIMIT 1;") or
            die("query error in Menu::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Menu($r);
      }
      return null;
    }

    static public function get($params=array(), $offset=0, $limit=25) {
      global $db;
      $where = Menu::get_where($params);
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM menus ".
                        "WHERE deleted_at IS NULL $where ".
                        "ORDER BY slug, lang ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Menu::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Menu($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Menu::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM menus ".
                        "WHERE deleted_at IS NULL $where;") or
            die("query error in Menu::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      $where = "";
      if (isset($params['q']) && $params['q']) {
        $where .= "AND (lower(slug) like " . _text('%' . mb_strtolower($params['q']) . '%') . ") ";
      }
      return $where;
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE menus SET ".
                        "deleted_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Menu::delete: " . mysqli_error($db));
    }

    public function content() {
      $parser = new \Symfony\Component\Yaml\Parser();
      $content = $this['content'] ? $parser->parse($this['content']) : array();
      return array_map('models\Menu::content_menu', $content);
    }

    static public function content_menu($menu) {
      return array(
        'name' => _a($menu, 'name'),
        'rel' => _a($menu, 'rel'),
        'link' => _a($menu, 'link'),
        'class' => _a($menu, 'class'),
        'children' => array_map('models\Menu::content_menu', _a($menu, 'children', array())),
      );
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE menus SET ".
                            "lang = " . _text(_a($params, 'lang')) . ", ".
                            "slug = " . _text(_a($params, 'slug')) . ", ".
                            "content = " . _text(_a($params, 'content')) . ", ".
                            "published = " . _integer(_a($params, 'published')) . ", ".
                            "published_datetime = " . _datetime(_a($params, 'published_datetime')) . ", ".
                            "published_datetime2 = " . _datetime(_a($params, 'published_datetime2')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Menu::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
    }

  }

?>
