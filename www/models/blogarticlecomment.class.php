<?php

  namespace models;

  class BlogArticleComment extends \ArrayObject {

    static public function add($article_id, $comment, $user=null, $parent_id=null) {
      global $principal, $db;
      $user = $user ? $user : $principal;
      mysqli_query($db, "INSERT INTO blog_articles_comments (".
                    "article_id,".
                    "user_id,".
                    "parent_id,".
                    "status,".
                    "body,".
                    "created_at) ".
                  "VALUES (".
                    _integer($article_id) . ", ".
                    _integer($user['id']) . ", ".
                    _integer($parent_id) . ", ".
                    _integer(0) . ", ".
                    _text($comment) . ", ".
                    "NOW());") or
                  die("query error in BlogArticleComment::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get($params=array(), $offset=0, $limit=20) {
      global $db, $lang;
      $where = BlogArticleComment::get_where($params);
      $rs = mysqli_query($db, "SELECT c.*, CONCAT(u.firstname, ' ', u.lastname) as nickname, 0 as moderator ".
                        "FROM blog_articles_comments c ".
                        "JOIN users u ON (c.user_id = u.id) ".
                        "LEFT JOIN blog_articles_comments c2 ON (c.parent_id = c2.id) ".
                        "WHERE c.deleted_at IS NULL AND u.deleted_at IS NULL $where ".
                        "ORDER BY COALESCE(c2.created_at, c.created_at) DESC, c.parent_id IS NOT NULL, c.created_at DESC ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in BlogArticleComment::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new BlogArticleComment($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = BlogArticleComment::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM blog_articles_comments c ".
                        "JOIN users u ON (c.user_id = u.id) ".
                        "WHERE c.deleted_at IS NULL AND u.deleted_at IS NULL $where;") or
            die("query error in BlogArticleComment::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      global $lang, $principal;
      $where = "";
      if (isset($params['status']) && $params['status'] == 0) {
        $where .= "AND (COALESCE(c.status, 0) = 0) ";
      }
      if (isset($params['status'])) {
        if ($principal) {
          $where .= "AND (c.status = " . _integer($params['status']) . " OR u.id = " . _integer($principal['id']) . ") ";
        } else {
          $where .= "AND (c.status = " . _integer($params['status']) . ") ";
        }
      }
      if (isset($params['article_id'])) {
        $where .= "AND (c.article_id = " . _integer($params['article_id']) . ") ";
      }
      if (isset($params['q'])) {
        $where .= "AND (lower(c.body) like " . _text('%' . strtolower($params['q']) . '%') . " ".
                    "OR lower(u.firstname) like " . _text('%' . strtolower($params['q']) . '%') . " ".
                    "OR lower(u.lastname) like " . _text('%' . strtolower($params['q']) . '%') . ") ";
      }
      return $where;
    }

    static public function get_social() {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT c.*, CONCAT(u.firstname, ' ', u.lastname) as nickname ".
                        "FROM blog_articles_comments c ".
                        "JOIN users u ON (c.user_id = u.id) ".
                        "LEFT JOIN blog_articles_comments c2 ON (c.parent_id = c2.id) ".
                        "WHERE c.deleted_at IS NULL AND u.deleted_at IS NULL ".
                        "ORDER BY COALESCE(c2.created_at, c.created_at) DESC, c.parent_id IS NOT NULL, c.created_at DESC ".
                        "LIMIT 4;") or
            die("query error in BlogArticleComment::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new BlogArticleComment($r));
      }
      return $results;
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT c.*, CONCAT(u.firstname, ' ', u.lastname) as nickname ".
                        "FROM blog_articles_comments c ".
                        "JOIN users u ON (c.user_id = u.id) ".
                        "WHERE c.deleted_at IS NULL AND c.id = " . _integer($id) . ";") or
            die("query error in BlogCategory::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new BlogArticleComment($r);
      }
    }

    public function get_article() {
      return BlogArticle::get_by_id($this['article_id']);
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE blog_articles_comments SET ".
                    "deleted_at = NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in BlogArticleComment::delete: " . mysqli_error($db));
    }

    public function update_status($status) {
      global $db;
      mysqli_query($db, "UPDATE blog_articles_comments SET ".
                    "status = " . _integer($status) . ", ".
                    "updated_at = " . "NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in BlogCategoryComment::update: " . mysqli_error($db));
    }

  }

?>
