<?php

  namespace models;

  class PageContent extends \ArrayObject {

    function config() {
      $result = array();
      foreach (array_filter(preg_split('/[\s\n]+/', $this['config'])) as $cfg) {
        $parts = explode(":", $cfg);
        $obj = null;
        $result[] = array(
          "type" => $parts[0],
          "obj" => $obj,
        );
      }
      return $result;
    }

    function __construct($value) {
      parent::__construct($value);
    }

  }
