<?php

  namespace models;

  class HomeworkFile extends \ArrayObject {

    public function update_reply($params) {
      global $db;
      mysqli_query($db, "UPDATE users_files SET ".
                            "reply_name = " . _text($params["reply_name"]) . ", ".
                            "reply_asset = " . _text($params["reply_asset"]) . ", ".
                            "reply_type = " . _text($params["reply_type"]) . ", ".
                            "reply_size = " . _text($params["reply_size"]) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in HomeworkFIle::update: " . mysqli_error($db));
    }

  }
