<?php

  namespace models;

  class Page extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO pages (".
                        "lang,".
                        "slug,".
                        "title,".
                        "cover,".
                        "content,".
                        "published,".
                        "published_datetime,".
                        "seo_title,".
                        "seo_image,".
                        "seo_description,".
                        "seo_keywords,".
                        "created_at".
                      ") VALUES (".
                        _text(_a($params, 'lang')) . ", ".
                        _text(_a($params, 'slug')) . ", ".
                        _text(_a($params, 'title')) . ", ".
                        _text(_a(_a($params, 'cover'), 'asset')) . ", ".
                        _text_json(_a($params, 'content')) . ", ".
                        _integer(_a($params, 'published')) . ", ".
                        _datetime(_a($params, 'published_datetime')) . ", ".
                        _text(_a($params, 'seo_title')) . ", ".
                        _text(_a(_a($params, 'seo_image'), 'asset')) . ", ".
                        _text(_a($params, 'seo_description')) . ", ".
                        _text(_a($params, 'seo_keywords')) . ", ".
                        "NOW()".
                      ");") 
                      or die("query error in Page::add: " . mysqli_error($db));
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM pages WHERE id = " . _integer($id) . ";") or
            die("query error in Page::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Page($r);
      }
      return null;
    }

    static public function get_by_slug($slug) {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * FROM pages ".
                              "WHERE slug = " . _text($slug) . " ".
                              "AND lang = " . _text($lang) . " ".
                              "AND deleted_at IS NULL ".
                              "AND published = 1 ".
                              "AND (published_datetime IS NULL or published_datetime <= NOW()) ".
                              "AND (published_datetime2 IS NULL or published_datetime2 > NOW()) ".
                              "ORDER BY id DESC LIMIT 1;") or
            die("query error in Page::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Page($r);
      }
      return null;
    }

    static public function get($params=array(), $offset=0, $limit=25) {
      global $db;
      $where = Page::get_where($params);
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM pages ".
                        "WHERE deleted_at IS NULL $where ".
                        "ORDER BY slug, lang ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Page::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Page($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Page::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM pages ".
                        "WHERE deleted_at IS NULL $where;") or
            die("query error in Page::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      $where = "";
      if (isset($params['q']) && $params['q']) {
        $where .= "AND (lower(slug) like " . _text('%' . mb_strtolower($params['q']) . '%') . ") ";
      }
      return $where;
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE pages SET ".
                        "deleted_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Page::delete: " . mysqli_error($db));
    }

    public function content() {
      $result = array();
      foreach ($this['content'] as $c) {
        $result[] = new PageContent($c);
      }
      return $result;
    }

    static public function content_page($page) {
      return array(
        'name' => _a($page, 'name'),
        'rel' => _a($page, 'rel'),
        'link' => _a($page, 'link'),
        'class' => _a($page, 'class'),
        'children' => array_map('models\Page::content_page', _a($page, 'children', array())),
      );
    }

    public function add_content($params) {
      $this['content'][] = $params;
      $this->update_content($this);
    }

    public function set_content($params) {
      foreach ($this['content'] as $i => $c) {
        if ($c['id'] == $params['id']) {
          $this['content'][$i] = $params;
          break;
        }
      }
      $this->update_content($this);
    }

    public function unset_content($content_id) {
      foreach ($this['content'] as $i => $c) {
        if ($c['id'] == $content_id) {
          unset($this['content'][$i]);
          break;
        }
      }
      $this->update_content($this);
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE pages SET ".
                            "lang = " . _text(_a($params, 'lang')) . ", ".
                            "slug = " . _text(_a($params, 'slug')) . ", ".
                            "title = " . _text(_a($params, 'title')) . ", ".
                            "cover = " . _text(_a(_a($params, 'cover'), 'asset')) . ", ".
                            "published = " . _integer(_a($params, 'published')) . ", ".
                            "published_datetime = " . _datetime(_a($params, 'published_datetime')) . ", ".
                            "seo_title = " . _text(_a($params, 'seo_title')) . ", ".
                            "seo_image = " . _text(_a(_a($params, 'seo_image'), 'asset')) . ", ".
                            "seo_description = " . _text(_a($params, 'seo_description')) . ", ".
                            "seo_keywords = " . _text(_a($params, 'seo_keywords')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Page::update: " . mysqli_error($db));
    }

    public function update_content($params) {
      global $db;
      $content = _a($params, 'content', array());
      usort($content, function ($a, $b) {
        if ($a['sorting'] == $b['sorting']) {
          return 0;
        }
        return ($a['sorting'] < $b['sorting']) ? -1 : 1;
      });
      mysqli_query($db, "UPDATE pages SET ".
                            "content = " . _text_json($content) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Page::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
      $this['content'] = $this['content'] ? json_decode($this['content'], true) : array();
    }

  }

?>
