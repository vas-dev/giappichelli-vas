<?php

  namespace models;

  class Pubblication extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO pubblications (".
                        "lang,".
                        "parent_id,".
                        "name,".
                        "description,".
                        "image,".
                        "url,".
                        "teachers,".
                        "courses,".
                        "created_at".
                      ") VALUES (".
                        _text(_a($params, 'lang', 'it')) . ", ".
                        _integer(_a($params, 'parent_id', 0)) . ", ".
                        _text(_a($params, 'name')) . ", ".
                        _text(_a($params, 'description')) . ", ".
                        _text(_a(_a($params, 'image'), 'asset')) . ", ".
                        _text(_a($params, 'url')) . ", ".
                        _from_array(_a($params, 'teachers')) . ", ".
                        _from_array(_a($params, 'courses')) . ", ".
                        "NOW()".
                      ");")
                      or die("query error in Pubblication::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM pubblications WHERE id = " . _integer($id) . ";") or
            die("query error in Pubblication::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Pubblication($r);
      }
      return null;
    }

    static public function get_by_slug($slug) {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * FROM pubblications ".
                              "WHERE slug = " . _text($slug) . " AND lang = " . _text($lang) . " ".
                              "AND deleted_at IS NULL ".
                              "AND published = 1 ".
                              "AND (published_datetime IS NULL or published_datetime <= NOW()) ".
                              "AND (published_datetime2 IS NULL or published_datetime2 > NOW()) ".
                              "ORDER BY id DESC LIMIT 1;") or
            die("query error in Pubblication::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Pubblication($r);
      }
      return null;
    }

    static public function get_by_id_and_lang($id, $lang) {
      global $db, $lang;
      if ($lang == 'it') {
        return Pubblication::get_by_id($id);
      }
      $rs = mysqli_query($db, "SELECT * FROM pubblications WHERE parent_id = " . _integer($id) . " AND lang = " . _text($lang) . ";") or
            die("query error in Pubblication::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Pubblication($r);
      }
      $obj = Pubblication::get_by_id($id);
      if ($obj) {
        $obj_id = Pubblication::add(array(
          'parent_id' => $obj['id'],
          'lang' => $lang,
          'name' => $obj['name'],
          'description' => $obj['description'],
          'image' => array("asset" => $obj['image']),
          'url' => $obj['url'],
        ));
        return Pubblication::get_by_id($obj_id);
      }
      return null;
    }

    static public function get_by_slug_all($slug) {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * FROM pubblications ".
                              "WHERE slug = " . _text($slug) . " AND lang = " . _text($lang) . " ".
                              "AND deleted_at IS NULL ".
                              "ORDER BY id DESC LIMIT 1;") or
            die("query error in Pubblication::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Pubblication($r);
      }
      return null;
    }

    static public function get($params=array(), $offset=0, $limit=25) {
      global $db;
      $where = Pubblication::get_where($params);
      $columns = _a($params, 'columns');
      $order_by = _a($params, 'order_by', "lower(c.name), c.id");
      $rs = mysqli_query($db, "SELECT c.* $columns".
                        "FROM pubblications c ".
                        "WHERE c.deleted_at IS NULL $where ".
                        "ORDER BY $order_by ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Pubblication::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Pubblication($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Pubblication::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM pubblications c ".
                        "WHERE c.deleted_at IS NULL $where;") or
            die("query error in Pubblication::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where(&$params) {
      global $lang;
      $where = "AND c.lang = " . _text($lang) . " ";
      if (isset($params['q']) && $params['q']) {
        $where .= "AND (lower(c.name) like " . _text('%' . mb_strtolower($params['q']) . '%') . ") ";
      }
      if (isset($params['published']) && is_numeric($params['published'])) {
        $where .= "AND (c.published = " . _integer($params['published']) . ") ";
        $where .= "AND (c.published_datetime IS NULL or c.published_datetime <= NOW()) ";
        $where .= "AND (c.published_datetime2 IS NULL or c.published_datetime2 > NOW()) ";
      }
      if (isset($params['teacher_id']) && $params['teacher_id']) {
        $where .= "AND (teachers LIKE " . _text("%;" . $params['teacher_id'] . ";%") . ") ";
      }
      if (isset($params['course_id']) && $params['course_id']) {
        $where .= "AND (courses LIKE " . _text("%;" . $params['course_id'] . ";%") . ") ";
      }
      return $where;
    }

    static public function get_vocabulary($params=array()) {
      $r = array();
      foreach (Pubblication::get($params, 0, 9999) as $pubblication) {
        $r[$pubblication['id']] = $pubblication['name'];
      }
      return $r;
    }

    public function get_lessons($params=array()) {
      return Lesson::get(array("pubblication_id" => $this['id']), 0, 100);
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE pubblications SET ".
                        "deleted_at = NOW() ".
                      "WHERE id = " . _integer($this['id_raw']) . ";") or
                      die("query error in Pubblication::delete: " . mysqli_error($db));
    }

    public function update($params) {
      global $db;
      // i18n-specific
      mysqli_query($db, "UPDATE pubblications SET ".
                            "name = " . _text(_a($params, 'name')) . ", ".
                            "description = " . _text(_a($params, 'description')) . ", ".
                            "url = " . _text(_a($params, 'url')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id_raw']) . ";") or
                      die("query error in Pubblication::update: " . mysqli_error($db));
      // i18n-agnostic
      mysqli_query($db, "UPDATE pubblications SET ".
                            "image = " . _text(_a(_a($params, 'image'), 'asset')) . ", ".
                            "teachers = " . _from_array(_a($params, 'teachers')) . ", ".
                            "courses = " . _from_array(_a($params, 'courses')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE " . _integer($this['id']) . " IN (id, parent_id);") or
                      die("query error in Pubblication::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
      $this['id_raw'] = $this['id'];
      $this['id'] = $this['parent_id'] ? $this['parent_id'] : $this['id'];
      $this['teachers'] = $this['teachers'] ? _to_array($this['teachers']) : array();
      $this['courses'] = $this['courses'] ? _to_array($this['courses']) : array();
    }

  }

?>