<?php

  namespace models;

  class Teacher extends \ArrayObject {

    static public function add($params) {
      global $db;
      $slug = _a($params, 'slug');
      if (Teacher::get_by_slug_all($slug)) {
        for ($i = 2; $i <= 100; $i++) {
          if (!Teacher::get_by_slug_all($slug . "-" . $i)) {
            break;
          }
        }
        $params['slug'] = $slug . "-" . $i;
      }
      mysqli_query($db, "INSERT INTO teachers (".
                        "lang,".
                        "parent_id,".
                        "name,".
                        "slug,".
                        "intro,".
                        "description,".
                        "image,".
                        "pdf,".
                        "published,".
                        "published_datetime,".
                        "published_datetime2,".
                        "sorting,".
                        "seo_title,".
                        "seo_image,".
                        "seo_description,".
                        "seo_keywords,".
                        "created_at".
                      ") VALUES (".
                        _text(_a($params, 'lang', 'it')) . ", ".
                        _integer(_a($params, 'parent_id', 0)) . ", ".
                        _text(_a($params, 'name')) . ", ".
                        _text(_a($params, 'slug')) . ", ".
                        _text(_a($params, 'intro')) . ", ".
                        _text(_a($params, 'description')) . ", ".
                        _text(_a(_a($params, 'image'), 'asset')) . ", ".
                        _text(_a(_a($params, 'pdf'), 'asset')) . ", ".
                        _integer(_a($params, 'published')) . ", ".
                        _datetime(_a($params, 'published_datetime')) . ", ".
                        _datetime(_a($params, 'published_datetime2')) . ", ".
                        _integer(_a($params, 'sorting')) . ", ".
                        _text(_a($params, 'seo_title')) . ", ".
                        _text(_a(_a($params, 'seo_image'), 'asset')) . ", ".
                        _text(_a($params, 'seo_description')) . ", ".
                        _text(_a($params, 'seo_keywords')) . ", ".
                        "NOW()".
                      ");")
                      or die("query error in Teacher::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM teachers WHERE id = " . _integer($id) . ";") or
            die("query error in Teacher::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Teacher($r);
      }
      return null;
    }

    static public function get_by_ids($ids) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM teachers WHERE id IN " . _text_in($ids) . ";") or
            die("query error in Teacher::get_by_ids: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Teacher($r));
      }
      return $results;
    }

    static public function get_by_slug($slug) {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * FROM teachers ".
                              "WHERE slug = " . _text($slug) . " AND lang = " . _text($lang) . " ".
                              "AND deleted_at IS NULL ".
                              "AND published = 1 ".
                              "AND (published_datetime IS NULL or published_datetime <= NOW()) ".
                              "AND (published_datetime2 IS NULL or published_datetime2 > NOW()) ".
                              "ORDER BY id DESC LIMIT 1;") or
            die("query error in Teacher::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Teacher($r);
      }
      return null;
    }

    static public function get_by_id_and_lang($id, $lang) {
      global $db, $lang;
      if ($lang == 'it') {
        return Teacher::get_by_id($id);
      }
      $rs = mysqli_query($db, "SELECT * FROM teachers WHERE parent_id = " . _integer($id) . " AND lang = " . _text($lang) . ";") or
            die("query error in Teacher::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Teacher($r);
      }
      $obj = Teacher::get_by_id($id);
      if ($obj) {
        $obj_id = Teacher::add(array(
          'parent_id' => $obj['id'],
          'lang' => $lang,
          'name' => $obj['name'],
          'slug' => $obj['slug'],
          'intro' => $obj['intro'],
          'description' => $obj['description'],
          'pdf' => array("asset" => $obj['pdf']),
          'published' => $obj['published'],
          'published_datetime' => $obj['published_datetime'],
          'published_datetime2' => $obj['published_datetime2'],
          'sorting' => $obj['sorting'],
          'seo_title' => $obj['seo_title'],
          'seo_image' => array("asset" => $obj['seo_image']),
          'seo_description' => $obj['seo_description'],
          'seo_keywords' => $obj['seo_keywords'],
        ));
        return Teacher::get_by_id($obj_id);
      }
      return null;
    }

    static public function get_by_slug_all($slug) {
      global $db, $lang;
      $rs = mysqli_query($db, "SELECT * FROM teachers ".
                              "WHERE slug = " . _text($slug) . " AND lang = " . _text($lang) . " ".
                              "AND deleted_at IS NULL ".
                              "ORDER BY id DESC LIMIT 1;") or
            die("query error in Teacher::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Teacher($r);
      }
      return null;
    }

    static public function get($params=array(), $offset=0, $limit=25) {
      global $db;
      $where = Teacher::get_where($params);
      $columns = _a($params, 'columns');
      $order_by = _a($params, 'order_by', "c.sorting DESC, lower(c.name), c.id");
      $rs = mysqli_query($db, "SELECT c.* $columns".
                        "FROM teachers c ".
                        "WHERE c.deleted_at IS NULL $where ".
                        "ORDER BY $order_by ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Teacher::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Teacher($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Teacher::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM teachers c ".
                        "WHERE c.deleted_at IS NULL $where;") or
            die("query error in Teacher::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where(&$params) {
      global $lang;
      $where = "AND c.lang = " . _text($lang) . " ";
      if (isset($params['q']) && $params['q']) {
        $where .= "AND (lower(c.name) like " . _text('%' . mb_strtolower($params['q']) . '%') . ") ";
      }
      if (isset($params['published']) && is_numeric($params['published'])) {
        $where .= "AND (c.published = " . _integer($params['published']) . ") ";
        $where .= "AND (c.published_datetime IS NULL or c.published_datetime <= NOW()) ";
        $where .= "AND (c.published_datetime2 IS NULL or c.published_datetime2 > NOW()) ";
      }
      return $where;
    }

    static public function get_vocabulary($params=array()) {
      $r = array();
      foreach (Teacher::get($params, 0, 9999) as $teacher) {
        $r[$teacher['id']] = $teacher['name'];
      }
      return $r;
    }

    public function get_lessons($params=array()) {
      return Lesson::get(array("teacher_id" => $this['id']), 0, 100);
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE teachers SET ".
                        "deleted_at = NOW() ".
                      "WHERE id = " . _integer($this['id_raw']) . ";") or
                      die("query error in Teacher::delete: " . mysqli_error($db));
    }

    public function update($params) {
      global $db;
      // i18n-specific
      mysqli_query($db, "UPDATE teachers SET ".
                            "name = " . _text(_a($params, 'name')) . ", ".
                            "intro = " . _text(_a($params, 'intro')) . ", ".
                            "description = " . _text(_a($params, 'description')) . ", ".
                            "slug = " . _text(_a($params, 'slug')) . ", ".
                            "seo_title = " . _text(_a($params, 'seo_title')) . ", ".
                            "seo_description = " . _text(_a($params, 'seo_description')) . ", ".
                            "seo_keywords = " . _text(_a($params, 'seo_keywords')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id_raw']) . ";") or
                      die("query error in Teacher::update: " . mysqli_error($db));
      // i18n-agnostic
      mysqli_query($db, "UPDATE teachers SET ".
                            "image = " . _text(_a(_a($params, 'image'), 'asset')) . ", ".
                            "pdf = " . _text(_a(_a($params, 'pdf'), 'asset')) . ", ".
                            "published = " . _integer(_a($params, 'published')) . ", ".
                            "published_datetime = " . _datetime(_a($params, 'published_datetime')) . ", ".
                            "published_datetime2 = " . _datetime(_a($params, 'published_datetime2')) . ", ".
                            "sorting = " . _integer(_a($params, 'sorting')) . ", ".
                            "seo_image = " . _text(_a(_a($params, 'seo_image'), 'asset')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE " . _integer($this['id']) . " IN (id, parent_id);") or
                      die("query error in Teacher::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
      $this['id_raw'] = $this['id'];
      $this['id'] = $this['parent_id'] ? $this['parent_id'] : $this['id'];
    }

  }

?>