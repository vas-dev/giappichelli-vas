<?php

  namespace models;

  class BlogArticle extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO blog_articles (".
                    "slug,".
                    "title,".
                    "cover,".
                    "author,".
                    "description,".
                    "body,".
                    "date,".
                    "category_id,".
                    "category2_id,".
                    "published,".
                    "published_datetime,".
                    "requires_login,".
                    "seo_title,".
                    "seo_description,".
                    "seo_keyword,".
                    "seo_keywords,".
                    "seo_trackback,".
                    "homepage,".
                    "focus,".
                    "created_at) ".
                  "VALUES (".
                    _text(_slug($params['title'])) . ", " .
                    _text($params['title']) . ", " .
                    _text(_a($params['cover'], 'asset')) . ", " .
                    _text($params['author']) . ", " .
                    _text($params['description']) . ", " .
                    _text($params['body']) . ", " .
                    _date($params['date']) . ", " .
                    _integer($params['category_id']) . ", " .
                    _integer($params['category2_id']) . ", " .
                    _boolean($params['published']) . ", " .
                    _datetime($params['published_datetime']) . ", " .
                    _integer($params['requires_login']) . ", " .
                    _text($params['seo_title']) . ", " .
                    _text($params['seo_description']) . ", " .
                    _text($params['seo_keyword']) . ", " .
                    _text($params['seo_keywords']) . ", " .
                    _text($params['seo_trackback']) . ", " .
                    _boolean($params['homepage']) . ", " .
                    _boolean($params['focus']) . ", " .
                    "NOW());") or
                  die("query error in BlogArticle::add: " . mysqli_error($db));
      $article_id = mysqli_insert_id($db);
      $article = BlogArticle::get_by_id($article_id);
      $article->update_files($params['files']);
      $article->update_images($params['images']);
      $article->update_tags($params['tags']);
      $article->reindex();
      return $article_id;
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_articles ".
                        "WHERE deleted_at IS NULL AND id = " . _integer($id) . ";") or
            die("query error in BlogArticle::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        $obj = new BlogArticle($r);
        $obj->load();
        return $obj;
      }
    }

    static public function get_by_slug($slug) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_articles ".
                        "WHERE deleted_at IS NULL AND slug = " . _text($slug) . ";") or
            die("query error in BlogArticle::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        $obj = new BlogArticle($r);
        $obj->load();
        return $obj;
      }
    }

    static public function get($params, $offset, $limit) {
      global $db, $lang;
      $where = BlogArticle::get_where($params);
      $rs = mysqli_query($db, "SELECT a.* ".
                        "FROM blog_articles a ".
                        "JOIN blog_categories c ON (a.category_id = c.id) ".
                        "WHERE a.deleted_at IS NULL $where ".
                        "ORDER BY a.date DESC, a.title ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in BlogArticle::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new BlogArticle($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = BlogArticle::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM blog_articles a ".
                        "JOIN blog_categories c ON (a.category_id = c.id) ".
                        "WHERE a.deleted_at IS NULL $where;") or
            die("query error in BlogArticle::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params, $filters=true) {
      global $lang, $principal, $principal_id;
      $where = "";
      if ($principal_id && $principal['role'] != 1) {
        if ($principal['role'] == 2) {
          $where .= " AND c.category_id IN (1, 2) ";
        } else if ($principal['role'] == 3) {
          $where .= " AND c.category_id IN (3) ";
        } else if ($principal['role'] == 4) {
          $where .= " AND c.category_id IN (4, 5, 6) ";
        }
      }
      if (isset($params['published']) && $params['published']) {
        $where .= "AND (a.published = 1) ";
        $where .= "AND (a.published_datetime IS NULL OR a.published_datetime < NOW()) ";
        if (!$principal) {
          $where .= "AND (COALESCE(a.requires_login, false) = false) ";
        }
      }
      if (isset($params['c_category_id']) && $params['c_category_id']) {
        $where .= "AND (c.category_id = " . _integer($params['c_category_id']) . ") ";
      }
      if (isset($params['glossary_id']) && $params['glossary_id']) {
        $where .= "AND (glossary_id = " . _integer($params['glossary_id']) . ") ";
      }
      if (isset($params['date1']) && $params['date1']) {
        $where .= "AND (a.date >= " . _date($params['date1']) . ") ";
      }
      if (isset($params['date2']) && $params['date2']) {
        $where .= "AND (a.date <= " . _date($params['date2']) . ") ";
      }
      if ($filters) {
        if (isset($params['homepage']) && $params['homepage']) {
          $where .= "AND (a.homepage = 1) ";
        }
        if (isset($params['focus']) && $params['focus']) {
          $where .= "AND (a.focus = 1) ";
        }
        if (isset($params['not:homepage']) && $params['not:homepage']) {
          $where .= "AND (a.homepage = 0 or a.homepage IS NULL) ";
        }
        if (isset($params['q']) && $params['q']) {
          $where .= "AND (a.title like " . _text('%' . $params['q'] . '%') . ") ";
        }
        if (isset($params['year']) && $params['year']) {
          $where .= "AND (extract(year from a.date) = " . _integer($params['year']) . ") ";
        }
        if (isset($params['month']) && $params['month']) {
          $where .= "AND (extract(month from a.date) = " . _integer($params['month']) . ") ";
        }
        if (isset($params['category']) && $params['category']) {
          $where .= "AND (a.category_id = " . _integer($params['category']['id']) . " OR a.category2_id = " . _integer($params['category']['id']) . ") ";
        }
        if (isset($params['category_id'])) {
          $where .= "AND (a.category_id = " . _integer($params['category_id']) . ") ";
        }
        if (isset($params['tag']) && $params['tag']) {
          $where .= "AND EXISTS(SELECT id FROM blog_articles_tags bat WHERE bat.deleted_at IS NULL AND bat.article_id = a.id AND bat.tag_id = " . _integer($params['tag']['id']) . ") ";
        }
        if (isset($params['lt:date'])) {
          $where .= "AND (a.date < " . _date($params['lt:date']) . ") ";
        }
        if (isset($params['gt:date'])) {
          $where .= "AND (a.date > " . _date($params['gt:date']) . ") ";
        }
      }
      return $where;
    }

    static public function get_archive($category_id, $params=array()) {
      global $db;
      $params['c_category_id'] = $category_id;
      $params['published'] = 1;
      $where = BlogArticle::get_where($params, false);
      $rs = mysqli_query($db, "SELECT extract(year from a.date) AS year, ".
                        "       extract(month from a.date) AS month, ".
                        "       count(*) AS count ".
                        "FROM blog_articles a ".
                        "JOIN blog_categories c ON (a.category_id = c.id) ".
                        "WHERE a.deleted_at IS NULL " . $where . " ".
                        "GROUP BY 1, 2 ".
                        "ORDER BY 1 DESC, 2 DESC;") or
            die("query error in BlogArticle::get_archive: " . mysqli_error($db));
      $result = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        if (!isset($result[$r['year']])) {
          $result[$r['year']] = array();
        }
        if (!isset($result[$r['year']][$r['month']])) {
          $result[$r['year']][$r['month']] = $r['count'];
        }
      }
      return $result;
    }

    public function add_file($params) {
      $params['article_id'] = $this['id'];
      return BlogArticleFile::add($params);
    }

    public function add_image($params) {
      $params['article_id'] = $this['id'];
      return BlogArticleImage::add($params);
    }

    public function add_comment($comment, $user=null, $parent_id=null) {
      return BlogArticleComment::add($this['id'], $comment, $user, $parent_id);
    }

    public function add_tag($tag) {
      global $db;
      mysqli_query($db, "INSERT INTO blog_articles_tags (".
                    "article_id,".
                    "tag_id,".
                    "created_at) ".
                  "VALUES (".
                    _integer($this['id']) . ", ".
                    _integer($tag['id']) . ", ".
                    "NOW());") or
                  die("query error in BlogArticle::add_tag: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE blog_articles SET ".
                    "deleted_at = NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in BlogArticle::delete: " . mysqli_error($db));
    }

    public function get_comments($params, $offset, $limit) {
      $params['article_id'] = $this['id'];
      return BlogArticleComment::get($params, $offset, $limit);
    }

    public function get_comments_count($params) {
      $params['article_id'] = $this['id'];
      return BlogArticleComment::get_count($params);
    }

    public function get_files() {
      return BlogArticleFile::get_by_article($this);
    }

    public function get_images() {
      return BlogArticleImage::get_by_article($this);
    }

    public function get_tags() {
      return BlogTag::get_by_article($this);
    }

    public function get_related() {
      return array_merge(
        BlogArticle::get(array("published" => 1, "category_id" => $this['category_id'], "lt:date" => _dd($this['date'])), 0, 2),
        BlogArticle::get(array("published" => 1, "category_id" => $this['category_id'], "gt:date" => _dd($this['date'])), 0, 1)
      );
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE blog_articles SET ".
                    "title = " . _text($params["title"]) . ", ".
                    "slug = " . _text($params["slug"]) . ", ".
                    "cover = " . _text(_a($params["cover"], 'asset')) . ", ".
                    "author = " . _text($params["author"]) . ", ".
                    "description = " . _text($params["description"]) . ", ".
                    "body = " . _text($params["body"]) . ", ".
                    "date = " . _date($params["date"]) . ", ".
                    "category_id = " . _integer($params["category_id"]) . ", ".
                    "category2_id = " . _integer($params["category2_id"]) . ", ".
                    "published = " . _boolean($params["published"]) . ", ".
                    "published_datetime = " . _datetime($params['published_datetime']) . ", " .
                    "requires_login = " . _integer($params['requires_login']) . ", " .
                    "seo_title = " . _text($params['seo_title']) . ", " .
                    "seo_description = " . _text($params['seo_description']) . ", " .
                    "seo_keyword = " . _text($params['seo_keyword']) . ", " .
                    "seo_keywords = " . _text($params['seo_keywords']) . ", " .
                    "seo_trackback = " . _text($params['seo_trackback']) . ", " .
                    "homepage = " . _boolean($params["homepage"]) . ", ".
                    "focus = " . _boolean($params["focus"]) . ", ".
                    "updated_at = " . "NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in BlogArticle::update: " . mysqli_error($db));
      $this->update_files($params['files']);
      $this->update_images($params['images']);
      $this->update_tags($params['tags']);
      // reindex
      $this['title'] = $params['title'];
      $this['description'] = $params['description'];
      $this['body'] = $params['body'];
      $this->reindex();
    }

    public function reindex() {
    }

    function update_files($files) {
      global $db;
      if (!$files) {
        $files = array();
      }
      $ids = array();
      $sorting = count($this['files']);
      foreach ($files as $file) {
        if (!isset($file['id'])) {
          $file_id = $this->add_file(array(
            'title' => null,
            'sorting' => $sorting++,
            'filename' => $file['name'],
            'file' => $file['asset'],
            'md5' => $file['md5'],
            'size' => $file['size'],
          ));
          array_push($ids, $file_id);
        } else {
          array_push($ids, $file['id']*1);
        }
      }
      if ($ids) {
        mysqli_query($db, "UPDATE blog_articles_files ".
                    "SET deleted_at = NOW() ".
                    "WHERE article_id = " . _integer($this['id']) . " AND id NOT IN (" . implode(",", $ids) . ");") or
                    die("query error in BlogArticle::update_files: " . mysqli_error($db));
      } else {
        mysqli_query($db, "UPDATE blog_articles_files ".
                    "SET deleted_at = NOW() ".
                    "WHERE article_id = " . _integer($this['id']) . " AND deleted_at IS NULL;") or
                    die("query error in BlogArticle::update_files: " . mysqli_error($db));
      }
    }

    function update_images($images) {
      global $db;
      if (!$images) {
        $images = array();
      }
      $ids = array();
      $sorting = count($this['images']);
      foreach ($images as $image) {
        if (!isset($image['id'])) {
          $image_id = $this->add_image(array(
            'title' => null,
            'sorting' => $sorting++,
            'filename' => $image['name'],
            'image' => $image['asset'],
            'md5' => $image['md5'],
            'size' => $image['size'],
          ));
          array_push($ids, $image_id);
        } else {
          array_push($ids, $image['id']*1);
        }
      }
      if ($ids) {
        mysqli_query($db, "UPDATE blog_articles_images ".
                    "SET deleted_at = NOW() ".
                    "WHERE article_id = " . _integer($this['id']) . " AND id NOT IN (" . implode(",", $ids) . ");") or
                    die("query error in BlogArticle::update_images: " . mysqli_error($db));
      } else {
        mysqli_query($db, "UPDATE blog_articles_images ".
                    "SET deleted_at = NOW() ".
                    "WHERE article_id = " . _integer($this['id']) . " AND deleted_at IS NULL;") or
                    die("query error in BlogArticle::update_images: " . mysqli_error($db));
      }
    }

    function update_tags($tags) {
      global $db;
      if (!$tags) {
        return;
      }
      $ids = array();
      $ctags = explode(",", $this['tags']);
      foreach ($tags as $tag) {
        $tag = BlogTag::get_or_add($tag);
        if (!in_array($tag['tag'], $ctags)) {
          $tag_id = $this->add_tag($tag);
        }
        array_push($ids, $tag['id']*1);
      }
      if ($ids) {
        mysqli_query($db, "UPDATE blog_articles_tags ".
                    "SET deleted_at = NOW() ".
                    "WHERE article_id = " . _integer($this['id']) . " AND tag_id NOT IN (" . implode(",", $ids) . ");") or
                    die("query error in BlogArticle::update_tags: " . mysqli_error($db));
      }
    }

    function __construct($value) {
      parent::__construct($value);
      $this['category'] = BlogCategory::get_by_id($this['category_id']);
    }

    public function load() {
      $this['images'] = $this->get_images();
      $this['files'] = $this->get_files();
      $this['tags'] = array();
      $this['tags_objects'] = $this->get_tags();
      foreach ($this['tags_objects'] as $tag) {
        array_push($this['tags'], $tag['tag']);
      }
      $this['tags'] = implode(",", $this['tags']);
    }
  }

  class BlogArticleFile extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO blog_articles_files (".
                    "article_id,".
                    "sorting,".
                    "title,".
                    "file,".
                    "name,".
                    "size,".
                    "md5,".
                    "created_at) ".
                  "VALUES (".
                    _integer($params['article_id']) . ", ".
                    _integer($params['sorting']) . ", ".
                    _text($params['title']) . ", ".
                    _text($params['file']) . ", ".
                    _text($params['filename']) . ", ".
                    _integer($params['size']) . ", ".
                    _text($params['md5']) . ", " .
                    "NOW());") or
                  die("query error in BlogArticleFile::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_article($article) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_articles_files ".
                        "WHERE deleted_at IS NULL AND article_id = " . _integer($article['id']) . " ".
                        "ORDER BY sorting, title, name;") or
            die("query error in BlogArticleFile::get_by_article: " . mysqli_error($db));
      $result = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($result, new BlogArticleFile($r));
      }
      return $result;
    }

  }

  class BlogArticleImage extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO blog_articles_images (".
                    "article_id,".
                    "sorting,".
                    "title,".
                    "image,".
                    "name,".
                    "size,".
                    "md5,".
                    "created_at) ".
                  "VALUES (".
                    _integer($params['article_id']) . ", ".
                    _integer($params['sorting']) . ", ".
                    _text($params['title']) . ", ".
                    _text($params['image']) . ", ".
                    _text($params['filename']) . ", ".
                    _integer($params['size']) . ", ".
                    _text($params['md5']) . ", " .
                    "NOW());") or
                  die("query error in BlogArticleImage::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_article($article) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_articles_images ".
                        "WHERE deleted_at IS NULL AND article_id = " . _integer($article['id']) . " ".
                        "ORDER BY sorting, title, name;") or
            die("query error in BlogArticleImage::get_by_article: " . mysqli_error($db));
      $result = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($result, new BlogArticleImage($r));
      }
      return $result;
    }

  }

?>
