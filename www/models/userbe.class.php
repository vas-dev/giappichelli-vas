<?php

  namespace models;

  class UserBE extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO users_be (".
                        "company, ".
                        "lastname, ".
                        "firstname, ".
                        "email, ".
                        "password, ".
                        "role, ".
                        "roles, ".
                        "courses, ".
                        "created_at".
                      ") VALUES (".
                        _text(isset($params['company']) ? $params['company'] : null) . ", " .
                        _text(isset($params['lastname']) ? $params['lastname'] : null) . ", " .
                        _text(isset($params['firstname']) ? $params['firstname'] : null) . ", " .
                        _text(isset($params['email']) ? $params['email'] : null) . ", " .
                        _password(isset($params['password']) ? $params['password'] : null) . ", " .
                        _text(isset($params['role']) ? $params['role'] : null) . ", " .
                        _from_array(isset($params['roles']) ? $params['roles'] : null) . ", " .
                        _from_array(isset($params['courses']) ? $params['courses'] : null) . ", " .
                        "NOW()".
                      ");")
                      or die("query error in UserBE::add: " . mysqli_error($db));
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM users_be WHERE id = " . _integer($id) . ";") or
            die("query error in UserBE::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new UserBE($r);
      }
      return null;
    }

    static public function get_by_email($email) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM users_be WHERE email = " . _text($email) . ";") or
            die("query error in UserBE::get_by_email: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new UserBE($r);
      }
      return null;
    }

    static public function get($params=array(), $offset=0, $limit=25) {
      global $db;
      $where = UserBE::get_where($params);
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM users_be ".
                        "WHERE deleted_at IS NULL $where ".
                        "ORDER BY id ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in UserBE::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new UserBE($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = UserBE::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM users_be ".
                        "WHERE deleted_at IS NULL $where;") or
            die("query error in UserBE::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      $where = "";
      if (isset($params['q'])) {
        $where .= "AND (firstname like " . _text('%' . $params['q'] . '%') . " OR ".
                       "lastname like " . _text('%' . $params['q'] . '%') . " OR ".
                       "email like " . _text('%' . $params['q'] . '%') . ") ";
      }
      return $where;
    }

    static public function identify() {
      global $_COOKIE;
      if (isset($_COOKIE[COOKIE_NAME . "-be"])) {
        $parts = explode("-", $_COOKIE[COOKIE_NAME . "-be"]);
        if (count($parts) == 2 && ctype_digit($parts[0]) && md5($parts[0] . COOKIE_SECRET)) {
          return UserBE::get_by_id($parts[0]);
        }
      }
    }

    public function forget() {
      setcookie(COOKIE_NAME . "-be", 'content', 1, "/", COOKIE_DOMAIN);
    }

    public function remember() {
      setcookie(COOKIE_NAME . "-be", $this['id'] . "-" . md5($this['id'] . COOKIE_SECRET), time()+365*24*3600, "/", COOKIE_DOMAIN);
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE users_be SET ".
                        "deleted_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in UserBE::delete: " . mysqli_error($db));
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE users_be SET ".
                        "company = " . _text($params['company']) . ", ".
                        "lastname = " . _text($params['lastname']) . ", ".
                        "firstname = " . _text($params['firstname']) . ", ".
                        "email = " . _text($params['email']) . ", ".
                        "password = COALESCE(" . _password($params['password']) . ", password), ".
                        "role = " . _integer($params['role']) . ", ".
                        "roles = " . _from_array($params['roles']) .", ".
                        "courses = " . _from_array($params['courses']) .", ".
                        "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in UserBE::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
      $this['roles'] = _to_array($this['roles']);
      $this['courses'] = _to_array($this['courses']);
    }
  }

?>
