<?php

  namespace models;

  class Homework extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO homeworks (".
                        "course_id,".
                        "name,".
                        "description,".
                        "published,".
                        "published_datetime,".
                        "published_datetime2,".
                        "created_at".
                      ") VALUES (".
                        _integer(_a($params, 'course_id', 0)) . ", ".
                        _text(_a($params, 'name')) . ", ".
                        _text(_a($params, 'description')) . ", ".
                        _integer(_a($params, 'published')) . ", ".
                        _datetime(_a($params, 'published_datetime')) . ", ".
                        _datetime(_a($params, 'published_datetime2')) . ", ".
                        "NOW()".
                      ");") 
                      or die("query error in Homework::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * FROM homeworks WHERE id = " . _integer($id) . ";") or
            die("query error in Homework::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Homework($r);
      }
      return null;
    }

    static public function get($params=array(), $offset=0, $limit=25) {
      global $db;
      $where = Homework::get_where($params);
      $columns = _a($params, 'columns');
      $order_by = _a($params, 'order_by', "c.published_datetime DESC");
      $rs = mysqli_query($db, "SELECT c.* $columns".
                        "FROM homeworks c ".
                        "WHERE c.deleted_at IS NULL $where ".
                        "ORDER BY $order_by ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Homework::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Homework($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Homework::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM homeworks c ".
                        "WHERE c.deleted_at IS NULL $where;") or
            die("query error in Homework::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where(&$params) {
      global $lang, $principal;
      $where = "";
      if (isset($params['q']) && $params['q']) {
        $where .= "AND (lower(c.name) like " . _text('%' . mb_strtolower($params['q']) . '%') . ") ";
      }
      if (isset($params['course_id']) && $params['course_id']) {
        $where .= "AND (c.course_id = " . _integer(mb_strtolower($params['course_id'])) . ") ";
      }
      if (isset($params['published']) && is_numeric($params['published'])) {
        $where .= "AND (c.published = " . _integer($params['published']) . ") ";
        $where .= "AND (c.published_datetime IS NULL or c.published_datetime <= current_date) ";
        # if (!_in_array(2, $principal['roles'])) {
        #   $where .= "AND (c.published_datetime2 IS NULL or cast(c.published_datetime2 as date) >= current_date) ";
        # }
      }
      return $where;
    }

    static public function get_vocabulary($params=array()) {
      $r = array();
      foreach (Homework::get($params, 0, 9999) as $homework) {
        $r[$homework['id']] = $homework['name'];
      }
      return $r;
    }

    public function get_file_by_user($user) {
      global $db;
      $rs = mysqli_query($db, "SELECT f.*, u.firstname, u.lastname, u.email ".
                              "FROM users_files f ".
                              "JOIN users u ON (u.id = f.user_id) ".
                              "WHERE f.homework_id = " . _integer($this['id']) . " AND f.user_id = " . _integer($user['id']) . " ORDER BY f.created_at DESC;") or
                         die("query error in Homework::get_files_count: " . mysqli_error($db));
      if ($r = mysqli_fetch_assoc($rs)) {
        return new HomeworkFile($r);
      }
    }

    public function get_files() {
      global $db;
      $rs = mysqli_query($db, "SELECT f.*, u.firstname, u.lastname, u.email ".
                              "FROM users_files f ".
                              "JOIN users u ON (u.id = f.user_id) ".
                              "WHERE f.homework_id = " . _integer($this['id']) . " ORDER BY f.created_at DESC;") or
                         die("query error in Homework::get_files_count: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new HomeworkFile($r));
      }
      return $results;
    }

    public function get_files_count() {
      global $db;
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count FROM users_files WHERE homework_id = " . _integer($this['id']) . ";") or
                         die("query error in Homework::get_files_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }


    public function is_published() {
      $now = date('Y-m-d');
      return $this['published_datetime'] <= $now && $this['published_datetime2'] >= $now;
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE homeworks SET ".
                        "deleted_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Homework::delete: " . mysqli_error($db));
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE homeworks SET ".
                            "course_id = " . _integer(_a($params, 'course_id')) . ", ".
                            "name = " . _text(_a($params, 'name')) . ", ".
                            "description = " . _text(_a($params, 'description')) . ", ".
                            "published = " . _integer(_a($params, 'published')) . ", ".
                            "published_datetime = " . _datetime(_a($params, 'published_datetime')) . ", ".
                            "published_datetime2 = " . _datetime(_a($params, 'published_datetime2')) . ", ".
                            "updated_at = NOW() ".
                      "WHERE id = " . _integer($this['id']) . ";") or
                      die("query error in Homework::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
    }

  }

?>
