<?php

  namespace models;

  class Banner extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO banners (".
                    "slug,".
                    "title,".
                    "image,".
                    "navigator,".
                    "cta,".
                    "link,".
                    "category,".
                    "sorting,".
                    "published,".
                    "date_begin,".
                    "date_end,".
                    "created_at) ".
                  "VALUES (".
                    _text($params["slug"]) . ", ".
                    _text($params["title"]) . ", ".
                    _text(_a($params["image"], 'asset')) . ", ".
                    _text(_a($params["navigator"], 'asset')) . ", ".
                    _text($params["cta"]) . ", ".
                    _text($params["link"]) . ", ".
                    _integer($params["category"]) . ", ".
                    _integer($params["sorting"]) . ", ".
                    _integer($params["published"]) . ", ".
                    _date($params['date_begin']) . ", " .
                    _date($params['date_end']) . ", " .
                    "NOW());") or
                  die("query error in Banner::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get($params=array(), $offset=0, $limit=20) {
      global $db, $lang;
      $where = Banner::get_where($params);
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM banners ".
                        "WHERE deleted_at IS NULL $where ".
                        "ORDER BY category, slug, sorting, title ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Banner::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Banner($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Banner::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM banners ".
                        "WHERE deleted_at IS NULL $where;") or
            die("query error in Banner::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      global $lang, $principal, $principal_id;
      $where = "";
      if (isset($params['q'])) {
        $where .= "AND (title like " . _text('%' . $params['q'] . '%') . " OR ".
                       "link like " . _text('%' . $params['q'] . '%') . ") ";
      }
      if (isset($params['category']) && $params['category']) {
        $where .= "AND (category = " . _integer($params['category']) . ") ";
      }
      if (isset($params['slug']) && $params['slug']) {
        $where .= "AND (slug = " . _text($params['slug']) . ") ";
      }
      if (isset($params['published'])) {
        $where .= "AND (published = " . _integer($params['published']) . ") ";
      }
      if (isset($params['dates'])) {
        $where .= "AND ((date_begin IS NULL OR date_begin <= current_date) AND (date_end IS NULL OR date_end >= current_date)) ";
      }
      return $where;
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM banners ".
                        "WHERE deleted_at IS NULL AND id = " . _integer($id) . ";") or
            die("query error in Banner::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Banner($r);
      }
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE banners SET ".
                    "deleted_at = NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in Banner::delete: " . mysqli_error($db));
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE banners SET ".
                    "slug = " . _text($params["slug"]) . ", ".
                    "title = " . _text($params["title"]) . ", ".
                    "image = COALESCE(" . _text(_a($params["image"], 'asset')) . ", image), ".
                    "navigator = COALESCE(" . _text(_a($params["navigator"], 'asset')) . ", navigator), ".
                    "cta = " . _text($params["cta"]) . ", ".
                    "link = " . _text($params["link"]) . ", ".
                    "category = " . _integer($params["category"]) . ", ".
                    "sorting = " . _integer($params["sorting"]) . ", ".
                    "published = " . _integer($params["published"]) . ", ".
                    "date_begin = " . _date($params['date_begin']) . ", " .
                    "date_end = " . _date($params['date_end']) . ", " .
                    "updated_at = " . "NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in Banner::update: " . mysqli_error($db));
    }

    function __construct($value) {
      parent::__construct($value);
      if (strstr($this['link'], 'http://')) {
        $this['href'] = $this['link'];
        $this['rel'] = 'external';
      } else if ($this['link'] == '/punti-vendita/popup') {
        $this['href'] = $this['link'];
        $this['rel'] = null;
      } else {
        $this['href'] = BASE_URL . '/' . ltrim($this['link'], "/");
        $this['rel'] = null;
      }
    }

  }

?>
