<?php

  namespace models;

  class BlogCategory extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO blog_categories (".
                    "category_id,".
                    "slug,".
                    "title,".
                    "color,".
                    "created_at) ".
                  "VALUES (".
                    _integer($params['category_id']) . ", " .
                    _text(_slug($params['title'])) . ", " .
                    _text($params['title']) . ", " .
                    _text($params['color']) . ", " .
                    "NOW());") or
                  die("query error in BlogCategory::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_categories ".
                        "WHERE deleted_at IS NULL AND id = " . _integer($id) . ";") or
            die("query error in BlogCategory::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new BlogCategory($r);
      }
    }

    static public function get_by_slug($slug, $category_id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_categories ".
                        "WHERE deleted_at IS NULL AND slug = " . _text($slug) . " AND category_id = " . _integer($category_id) . ";") or
            die("query error in BlogCategory::get_by_slug: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new BlogCategory($r);
      }
    }

    static public function get_vocabulary() {
      global $BLOG_CATEGORIES;
      $options = array();
      foreach (BlogCategory::get(array(), 0, 1000) as $category) {
        $options[$category['id']] = _v($category['category_id'], $BLOG_CATEGORIES) . ' &raquo; ' . $category['title'];
      }
      return $options;
    }

    static public function get($params=array(), $offset=0, $limit=20) {
      global $db, $lang;
      $where = BlogCategory::get_where($params);
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM blog_categories ".
                        "WHERE deleted_at IS NULL $where ".
                        "ORDER BY category_id, title ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in BlogCategory::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new BlogCategory($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = BlogCategory::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM blog_categories ".
                        "WHERE deleted_at IS NULL $where;") or
            die("query error in BlogCategory::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      global $lang, $principal, $principal_id;
      $where = "";
      if ($principal_id && $principal['role'] != 1) {
        if ($principal['role'] == 2) {
          $where .= " AND category_id IN (1, 2) ";
        } else if ($principal['role'] == 3) {
          $where .= " AND category_id IN (3) ";
        } else if ($principal['role'] == 4) {
          $where .= " AND category_id IN (4, 5, 6) ";
        }
      }
      if (isset($params['category_id'])) {
        $where .= "AND (category_id = " . _integer($params['category_id']) . ") ";
      }
      if (isset($params['q'])) {
        $where .= "AND (title like " . _text('%' . $params['q'] . '%') . ") ";
      }
      return $where;
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE blog_categories SET ".
                    "deleted_at = NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in BlogCategory::delete: " . mysqli_error($db));
    }

    public function not_empty($params=array()) {
      global $memcached;
      $k = "GIAPPICHELLI:BlogCategory.not_empty:" . $this['id'] . ":" . json_encode($params);
      $result = $memcached ? $memcached->get($k) : null;
      if ($result) {
        return $result - 1;
      }
      $params['category'] = $this;
      $params['published'] = 1;
      $result = BlogArticle::get_count($params) > 0;
      if ($memcached) {
        $memcached->set($k, (int)$result + 1, 300);
      }
      return $result;
    }


    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE blog_categories SET ".
                    "category_id = " . _integer($params["category_id"]) . ", ".
                    "slug = " . _text($params["slug"]) . ", ".
                    "title = " . _text($params["title"]) . ", ".
                    "color = " . _text($params["color"]) . ", ".
                    "updated_at = " . "NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in BlogCategory::update: " . mysqli_error($db));
    }

  }

?>
