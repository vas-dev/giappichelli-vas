<?php

namespace models;

class Course extends \ArrayObject
{
    public static function add($params)
    {
        global $db;
        $slug = _a($params, 'slug');
        if (Course::get_by_slug_all($slug)) {
            for ($i = 2; $i <= 100; ++$i) {
                if (!Course::get_by_slug_all($slug.'-'.$i)) {
                    break;
                }
            }
            $params['slug'] = $slug.'-'.$i;
        }
        mysqli_query($db, 'INSERT INTO courses ('.
                        'lang,'.
                        'parent_id,'.
                        'teachers,'.
                        'name,'.
                        'category,'.
                        'city,'.
                        'slug,'.
                        'preview,'.
                        'description,'.
                        'duration,'.
                        'calendar,'.
                        'place,'.
                        'prices,'.
                        'image,'.
                        'pdf,'.
                        'form,'.
                        'button_label,'.
                        'button_link,'.
                        'max_hours,'.
                        'features_form,'.
                        'published,'.
                        'published_datetime,'.
                        'published_datetime2,'.
                        'sorting,'.
                        'seo_title,'.
                        'seo_image,'.
                        'seo_description,'.
                        'seo_keywords,'.
                        'created_at'.
                      ') VALUES ('.
                        _text(_a($params, 'lang', 'it')).', '.
                        _integer(_a($params, 'parent_id', 0)).', '.
                        _from_array(_a($params, 'teachers')).', '.
                        _text(_a($params, 'name')).', '.
                        _text(_a($params, 'category')).', '.
                        _text(_a($params, 'city')).', '.
                        _text(_a($params, 'slug')).', '.
                        _text(_a($params, 'preview')).', '.
                        _text(_a($params, 'description')).', '.
                        _text(_a($params, 'duration')).', '.
                        _text(_a($params, 'calendar')).', '.
                        _text(_a($params, 'place')).', '.
                        _text(_a($params, 'prices')).', '.
                        _text(_a(_a($params, 'image'), 'asset')).', '.
                        _text(_a(_a($params, 'pdf'), 'asset')).', '.
                        _text(_a(_a($params, 'form'), 'asset')).', '.
                        _text(_a($params, 'button_label')).', '.
                        _text(_a($params, 'button_link')).', '.
                        _integer(_a($params, 'max_hours')).', '.
                        _integer(_a($params, 'features_form')).', '.
                        _integer(_a($params, 'published')).', '.
                        _datetime(_a($params, 'published_datetime')).', '.
                        _datetime(_a($params, 'published_datetime2')).', '.
                        _integer(_a($params, 'sorting')).', '.
                        _text(_a($params, 'seo_title')).', '.
                        _text(_a(_a($params, 'seo_image'), 'asset')).', '.
                        _text(_a($params, 'seo_description')).', '.
                        _text(_a($params, 'seo_keywords')).', '.
                        'NOW()'.
                      ');')
                      or die('query error in Course::add: '.mysqli_error($db));

        return mysqli_insert_id($db);
    }

    public static function get_by_id($id)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * FROM courses WHERE id = '._integer($id).';') or
            die('query error in Course::get_by_id: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Course($r);
        }

        return null;
    }

    public static function get_by_slug($slug)
    {
        global $db, $lang;
        $rs = mysqli_query($db, 'SELECT * FROM courses '.
                              'WHERE slug = '._text($slug).' AND lang = '._text($lang).' '.
                              'AND deleted_at IS NULL '.
                              'AND published = 1 '.
                              'AND (published_datetime IS NULL or published_datetime <= NOW()) '.
                              'AND (published_datetime2 IS NULL or published_datetime2 > NOW()) '.
                              'ORDER BY id DESC LIMIT 1;') or
            die('query error in Course::get_by_slug: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Course($r);
        }

        return null;
    }

    public static function get_by_id_and_lang($id, $lang)
    {
        global $db, $lang;
        if ('it' == $lang) {
            return Course::get_by_id($id);
        }
        $rs = mysqli_query($db, 'SELECT * FROM courses WHERE parent_id = '._integer($id).' AND lang = '._text($lang).';') or
            die('query error in Course::get_by_id: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Course($r);
        }
        $obj = Course::get_by_id($id);
        if ($obj) {
            $obj_id = Course::add(array(
                'parent_id' => $obj['id'],
                'lang' => $lang,
                'name' => $obj['name'],
                'slug' => $obj['slug'],
                'preview' => $obj['preview'],
                'description' => $obj['description'],
                'duration' => $obj['duration'],
                'calendar' => $obj['calendar'],
                'place' => $obj['place'],
                'prices' => $obj['prices'],
                'pdf' => array('asset' => $obj['pdf']),
                'form' => array('asset' => $obj['form']),
                'published' => $obj['published'],
                'published_datetime' => $obj['published_datetime'],
                'published_datetime2' => $obj['published_datetime2'],
                'sorting' => $obj['sorting'],
                'seo_title' => $obj['seo_title'],
                'seo_image' => array('asset' => $obj['seo_image']),
                'seo_description' => $obj['seo_description'],
                'seo_keywords' => $obj['seo_keywords'],
            ));

            return Course::get_by_id($obj_id);
        }

        return null;
    }

    public static function get_by_slug_all($slug)
    {
        global $db, $lang;
        $rs = mysqli_query($db, 'SELECT * FROM courses '.
                              'WHERE slug = '._text($slug).' AND lang = '._text($lang).' '.
                              'AND deleted_at IS NULL '.
                              'ORDER BY id DESC LIMIT 1;') or
            die('query error in Course::get_by_slug: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Course($r);
        }

        return null;
    }

    public static function get($params = array(), $offset = 0, $limit = 25)
    {
        global $db;
        $where = Course::get_where($params);
        $columns = _a($params, 'columns');
        $order_by = _a($params, 'order_by', 'c.sorting DESC, lower(c.name), c.id');
        $rs = mysqli_query($db, "SELECT c.* $columns".
                        'FROM courses c '.
                        "WHERE c.deleted_at IS NULL $where ".
                        "ORDER BY $order_by ".
                        'LIMIT '._integer($limit).' OFFSET '._integer($offset).';') or
            die('query error in Course::get: '.mysqli_error($db));
        $results = array();
        while ($r = mysqli_fetch_assoc($rs)) {
            array_push($results, new Course($r));
        }

        return $results;
    }

    public static function get_count($params)
    {
        global $db;
        $where = Course::get_where($params);
        $rs = mysqli_query($db, 'SELECT COUNT(*) AS count '.
                        'FROM courses c '.
                        "WHERE c.deleted_at IS NULL $where;") or
            die('query error in Course::get_count: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);

        return $r['count'];
    }

    public static function get_where(&$params)
    {
        global $lang;
        $where = 'AND c.lang = '._text($lang).' ';
        if (isset($params['q']) && $params['q']) {
            $where .= 'AND (lower(c.name) like '._text('%'.mb_strtolower($params['q']).'%').') ';
        }
        if (isset($params['name']) && $params['name']) {
            $where .= 'AND (lower(c.name) = '._text(mb_strtolower($params['name'])).') ';
        }
        if (isset($params['ids']) && $params['ids']) {
            $where .= 'AND (c.id IN '._text_in($params['ids']).') ';
        }
        if (isset($params['linked']) && $params['linked']) {
            $where .= 'AND (c.category is not null) ';
        }
        if (isset($params['city']) && $params['city']) {
            $where .= 'AND (c.city = '._text($params['city']).') ';
        }
        if (isset($params['teacher_id']) && $params['teacher_id']) {
            $where .= 'AND (c.teachers LIKE '._text('%;'.$params['teacher_id'].';%').') ';
        }
        if (isset($params['user_like']) && $params['user_like']) {
            $where .= 'AND (EXISTS (SELECT id FROM users_courses WHERE course_id = c.id AND user_id = '._integer($params['user_like']).')) ';
        }
        if (isset($params['published']) && is_numeric($params['published'])) {
            $where .= 'AND (c.published = '._integer($params['published']).') ';
            $where .= 'AND (c.published_datetime IS NULL or c.published_datetime <= NOW()) ';
            $where .= 'AND (c.published_datetime2 IS NULL or c.published_datetime2 > NOW()) ';
        }

        return $where;
    }

    public static function get_vocabulary($params = array())
    {
        $r = array();
        foreach (Course::get($params, 0, 9999) as $course) {
            $r[$course['id']] = $course['name'].($course['category'] ? ' - '.$course['category'].' - '.$course['city'] : '');
        }

        return $r;
    }

    public function get_parent_course()
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * FROM courses WHERE name = '._text($this['name']).' AND category IS NULL ORDER BY id DESC LIMIT 1;') or
            die('query error in Course::get_parent_course: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Course($r);
        }

        return $this;
    }

    public function get_related($params = array(), $offset = 0, $limit = 25)
    {
        $params['name'] = $this['name'];
        $params['city'] = $this['city'];

        return Course::get($params, $offset, $limit);
    }

    public function get_linked_courses($params = array(), $offset = 0, $limit = 25)
    {
        $params['name'] = $this['name'];
        $params['linked'] = true;

        return Course::get($params, $offset, $limit);
    }

    public function get_lessons($params = array())
    {
        return Lesson::get(array_merge($params, array('course_id' => $this['id'])), 0, 100);
    }

    public function delete()
    {
        global $db;
        mysqli_query($db, 'UPDATE courses SET '.
                        'deleted_at = NOW() '.
                      'WHERE id = '._integer($this['id_raw']).';') or
                      die('query error in Course::delete: '.mysqli_error($db));
    }

    public function update($params)
    {
        global $db;
        // i18n-specific
        mysqli_query($db, 'UPDATE courses SET '.
                            'name = '._text(_a($params, 'name')).', '.
                            'category = '._text(_a($params, 'category')).', '.
                            'city = '._text(_a($params, 'city')).', '.
                            'slug = '._text(_a($params, 'slug')).', '.
                            'preview = '._text(_a($params, 'preview')).', '.
                            'description = '._text(_a($params, 'description')).', '.
                            'duration = '._text(_a($params, 'duration')).', '.
                            'calendar = '._text(_a($params, 'calendar')).', '.
                            'place = '._text(_a($params, 'place')).', '.
                            'prices = '._text(_a($params, 'prices')).', '.
                            'pdf = '._text(_a(_a($params, 'pdf'), 'asset')).', '.
                            'form = '._text(_a(_a($params, 'form'), 'asset')).', '.
                            'seo_title = '._text(_a($params, 'seo_title')).', '.
                            'seo_description = '._text(_a($params, 'seo_description')).', '.
                            'seo_keywords = '._text(_a($params, 'seo_keywords')).', '.
                            'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id_raw']).';') or
                      die('query error in Course::update: '.mysqli_error($db));
        // i18n-agnostic
        mysqli_query($db, 'UPDATE courses SET '.
                            'teachers =  '._from_array(_a($params, 'teachers')).', '.
                            'image = '._text(_a(_a($params, 'image'), 'asset')).', '.
                            'max_hours = '._integer(_a($params, 'max_hours')).', '.
                            'features_form = '._integer(_a($params, 'features_form')).', '.
                            'button_label = '._text(_a($params, 'button_label')).', '.
                            'button_link = '._text(_a($params, 'button_link')).', '.
                            'published = '._integer(_a($params, 'published')).', '.
                            'published_datetime = '._datetime(_a($params, 'published_datetime')).', '.
                            'published_datetime2 = '._datetime(_a($params, 'published_datetime2')).', '.
                            'sorting = '._integer(_a($params, 'sorting')).', '.
                            'seo_image = '._text(_a(_a($params, 'seo_image'), 'asset')).', '.
                            'updated_at = NOW() '.
                      'WHERE '._integer($this['id']).' IN (id, parent_id);') or
                      die('query error in Course::update: '.mysqli_error($db));
    }

    public function is_liked($user)
    {
        global $db;
        if (!$user) {
            return 0;
        }
        $rs = mysqli_query($db, 'SELECT * FROM users_courses '.
                              'WHERE course_id = '._integer($this['id']).' '.
                              'AND user_id = '._integer($user['id']).';') or
                              die('query error in Course::is_liked: '.mysqli_error($db));

        return (bool) mysqli_fetch_assoc($rs) ? 1 : 0;
    }

    public function like($user)
    {
        global $db;
        if (!$this->is_liked($user)) {
            mysqli_query($db, 'INSERT INTO users_courses (course_id, user_id, created_at) '.
                          'VALUES ('._integer($this['id']).', '._integer($user['id']).', NOW());') or
                          die('query error in Course::is_liked: '.mysqli_error($db));

            return 1;
        }

        return 0;
    }

    public function unlike($user)
    {
        global $db;
        if ($this->is_liked($user)) {
            mysqli_query($db, 'DELETE FROM users_courses '.
                          'WHERE course_id = '._integer($this['id']).' '.
                          'AND user_id = '._integer($user['id']).';') or
                          die('query error in Course::is_liked: '.mysqli_error($db));

            return 1;
        }

        return 0;
    }

    public function is_next_lesson_live()
    {
        $lessons = Lesson::get([
            'course_id' => $this['id'],
            'published' => 1,
            'type:not' => 99,
            'lesson_date:gte' => date('Y-m-d'),
            'order_by' => 'COALESCE(l.lesson_date, \'2099-12-31\') ASC',
        ], 0, 100);

        return count($lessons) >= 1 && 1 == $lessons[0]['type'] ? $lessons[0] : null;
    }

    public function is_visible_for_user($user)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT max_hours, created_at FROM courses_users '.
                              'WHERE course_id = '._integer($this['id']).' AND user_id = '._integer($user['id']).';') or
                              die('query error in Course::is_visible_for_user: '.mysqli_error($db));
        $row = mysqli_fetch_assoc($rs);
        if (!$row) {
            mysqli_query($db, 'INSERT INTO courses_users (user_id, course_id, created_at) VALUES ('._integer($user['id']).', '._integer($this['id']).', NOW());') or
                die('query error in Course::is_visible_for_user: '.mysqli_error($db));

            return true;
        } else {
            $d1 = strtotime($row['created_at']);
            $d2 = time();
            if (!$this['max_hours'] && !$row['max_hours']) {
                return true;
            }

            return floor(($d2 - $d1) / 3600.0) < ($row['max_hours'] ? $row['max_hours'] : $this['max_hours']);
        }
    }

    public function __construct($value)
    {
        parent::__construct($value);
        $this['id_raw'] = $this['id'];
        $this['id'] = $this['parent_id'] ? $this['parent_id'] : $this['id'];
        $this['teachers'] = $this['teachers'] ? _to_array($this['teachers']) : array();
    }
}
