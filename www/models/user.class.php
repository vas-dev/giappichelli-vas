<?php

namespace models;

class User extends \ArrayObject
{
    public static function add($params)
    {
        global $db;
        mysqli_query($db, 'INSERT INTO users ('.
                        'uniqid, '.
                        'company, '.
                        'lastname, '.
                        'firstname, '.
                        'email, '.
                        'password, '.
                        'address,'.
                        'city,'.
                        'province,'.
                        'zipcode,'.
                        'country,'.
                        'mobile,'.
                        'phone,'.
                        'courses,'.
                        'groups,'.
                        'roles,'.
                        'premium,'.
                        'active,'.
                        'privacy,'.
                        'privacy2,'.
                        'newsletter,'.
                        'source,'.
                        'created_at'.
                      ') VALUES ('.
                        _text(_a($params, 'uniqid', uniqid())).', '.
                        _text(_a($params, 'company')).', '.
                        _text(_a($params, 'lastname')).', '.
                        _text(_a($params, 'firstname')).', '.
                        _text(_a($params, 'email')).', '.
                        _password_sha256(_a($params, 'password')).', '.
                        _text(_a($params, 'address')).', '.
                        _text(_a($params, 'city')).', '.
                        _text(_a($params, 'province')).', '.
                        _text(_a($params, 'zipcode')).', '.
                        _text(_a($params, 'country')).', '.
                        _text(_a($params, 'mobile')).', '.
                        _text(_a($params, 'phone')).', '.
                        _from_array(_a($params, 'courses')).', '.
                        _from_array(_a($params, 'groups')).', '.
                        _from_array(_a($params, 'roles')).', '.
                        _integer(_a($params, 'premium')).', '.
                        _integer(_a($params, 'active')).', '.
                        _integer(_a($params, 'privacy')).', '.
                        _integer(_a($params, 'privacy2')).', '.
                        _integer(_a($params, 'newsletter')).', '.
                        _text(_a($params, 'source')).', '.
                        'NOW()'.
                      ');')
                      or die('query error in User::add: '.mysqli_error($db));

        return mysqli_insert_id($db);
    }

    public static function get_by_id($id)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * FROM users WHERE id = '._integer($id).';') or
            die('query error in User::get_by_id: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new User($r);
        }

        return null;
    }

    public static function get_by_email($email)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * FROM users WHERE lower(email) = '._text(mb_strtolower($email)).';') or
            die('query error in User::get_by_email: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new User($r);
        }

        return null;
    }

    public static function get($params = array(), $offset = 0, $limit = 25)
    {
        global $db;
        $where = User::get_where($params);
        if ('id' == _a($params, 'sort')) {
            $sort = 'id DESC';
        } else {
            $sort = 'lower(lastname), lower(firstname)';
        }
        $rs = mysqli_query($db, 'SELECT * '.
                        'FROM users '.
                        "WHERE 1 = 1 $where ".
                        "ORDER BY $sort ".
                        'LIMIT '._integer($limit).' OFFSET '._integer($offset).';') or
            die('query error in User::get: '.mysqli_error($db));
        $results = array();
        while ($r = mysqli_fetch_assoc($rs)) {
            array_push($results, new User($r));
        }

        return $results;
    }

    public static function get_count($params)
    {
        global $db;
        $where = User::get_where($params);
        $rs = mysqli_query($db, 'SELECT COUNT(*) AS count '.
                        'FROM users '.
                        "WHERE 1 = 1 $where;") or
            die('query error in User::get_count: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);

        return $r['count'];
    }

    public static function get_where($params)
    {
        $where = '';
        if (isset($params['q']) && $params['q']) {
            $where .= 'AND (lower(firstname) like '._text('%'.mb_strtolower($params['q']).'%').' OR '.
                       'lower(lastname) like '._text('%'.mb_strtolower($params['q']).'%').' OR '.
                       'lower(email) like '._text('%'.mb_strtolower($params['q']).'%').') ';
        }
        if (isset($params['course']) && $params['course']) {
            $where .= 'AND (courses LIKE '._text('%;'.$params['course'].';%').') ';
        }
        if (isset($params['group']) && $params['group']) {
            $where .= 'AND (groups LIKE '._text('%;'.$params['group'].';%').') ';
        }
        if (isset($params['role']) && $params['role']) {
            $where .= 'AND (roles LIKE '._text('%;'.$params['role'].';%').') ';
        }
        if (isset($params['courses']) && $params['courses']) {
            $where .= 'AND ('.implode(' OR ', array_map(function ($r) {
                return 'courses LIKE '._text('%;'.$r.';%').' ';
            }, $params['courses'])).') ';
        }
        if (isset($params['groups']) && $params['groups']) {
            $where .= 'AND ('.implode(' OR ', array_map(function ($r) {
                return 'groups LIKE '._text('%;'.$r.';%').' ';
            }, $params['groups'])).') ';
        }
        if (isset($params['roles']) && $params['roles']) {
            $where .= 'AND ('.implode(' OR ', array_map(function ($r) {
                return 'roles LIKE '._text('%;'.$r.';%').' ';
            }, $params['roles'])).') ';
        }

        return $where;
    }

    public static function get_vocabulary($params = array())
    {
        $result = array();
        foreach (User::get($params, 0, 9999) as $user) {
            $result[$user['id']] = $user['lastname'].' '.$user['firstname'];
        }

        return $result;
    }

    public static function identify()
    {
        global $_COOKIE;
        if (isset($_COOKIE[COOKIE_NAME])) {
            $parts = explode('-', $_COOKIE[COOKIE_NAME]);
            if (2 == count($parts) && ctype_digit($parts[0]) && md5($parts[0].COOKIE_SECRET)) {
                $user = User::get_by_id($parts[0]);

                return $user && $user['active'] && !$user['deleted_at'] ? $user : null;
            }
        }
    }

    public function add_file($params)
    {
        global $db;
        mysqli_query($db, 'INSERT INTO users_files ('.
                    'user_id,'.
                    'homework_id,'.
                    'name,'.
                    'asset,'.
                    'type,'.
                    'size,'.
                    'created_at) '.
                  'VALUES ('.
                    _integer($this['id']).', '.
                    _integer($params['homework_id']).', '.
                    _text($params['name']).', '.
                    _text($params['asset']).', '.
                    _text($params['type']).', '.
                    _text($params['size']).', '.
                    'NOW());') or
                  die('query error in User::add_file: '.mysqli_error($db));

        return mysqli_insert_id($db);
    }

    public function get_files($params = array())
    {
        global $db, $lang;
        $where = '';
        $rs = mysqli_query($db, 'SELECT f.*, h.name as homework, u.firstname, u.lastname, u.email '.
                        'FROM users_files f '.
                        'JOIN users u ON f.user_id = u.id '.
                        'LEFT JOIN homeworks h ON f.homework_id = h.id '.
                        'WHERE '.(in_array(2, $this['roles']) ? '1 = 1' : 'f.user_id = '._integer($this['id'])).$where.' '.
                        'ORDER BY f.created_at DESC') or
            die('query error in User::get_files: '.mysqli_error($db));
        $results = array();
        while ($r = mysqli_fetch_assoc($rs)) {
            array_push($results, $r);
        }

        return $results;
    }

    public function forget()
    {
        setcookie(COOKIE_NAME, 'content', 1, '/', COOKIE_DOMAIN);
    }

    public function remember()
    {
        setcookie(COOKIE_NAME, $this['id'].'-'.md5($this['id'].COOKIE_SECRET), time() + 4 * 3600, '/', COOKIE_DOMAIN);
    }

    public function delete()
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'deleted_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::delete: '.mysqli_error($db));
    }

    public function remove()
    {
        global $db;
        mysqli_query($db, 'DELETE FROM users '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::remove: '.mysqli_error($db));
    }

    public function undelete()
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'deleted_at = NULL '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::undelete: '.mysqli_error($db));
    }

    public function email_password()
    {
        $smarty = new \Smarty();
        $smarty->setTemplateDir('templates/');
        $smarty->setCompileDir('templates_c/');
        $smarty->setConfigDir('config/');
        $smarty->setCacheDir('cache/');

        $smarty->assign('user', $this);
        $smarty->assign('base_url', BASE_URL);

        $mail = new \PHPMailer();
        $mail->CharSet = 'utf-8';
        $mail->Encoding = '8bit';
        $mail->SetFrom(EMAIL_ADDRESS, EMAIL_NAME);
        $mail->AddAddress($this['email'], $this['firstname'].' '.$this['lastname']);
        $mail->Subject = 'Recupero password';
        $mail->Body = $smarty->fetch('email-password.tmpl');
        $mail->Send();
    }

    public function email_registrazione()
    {
        $smarty = new \Smarty();
        $smarty->setTemplateDir('templates/');
        $smarty->setCompileDir('templates_c/');
        $smarty->setConfigDir('config/');
        $smarty->setCacheDir('cache/');

        $smarty->assign('user', $this);
        $smarty->assign('base_url', BASE_URL);

        $mail = new \PHPMailer();
        $mail->CharSet = 'utf-8';
        $mail->Encoding = '8bit';
        $mail->SetFrom(EMAIL_ADDRESS, EMAIL_NAME);
        $mail->AddAddress($this['email'], $this['firstname'].' '.$this['lastname']);
        $mail->Subject = 'Conferma di registrazione';
        $mail->Body = $smarty->fetch('email-registrazione.tmpl');
        $mail->Send();
    }

    public function email_landing()
    {
        $smarty = new \Smarty();
        $smarty->setTemplateDir('templates/');
        $smarty->setCompileDir('templates_c/');
        $smarty->setConfigDir('config/');
        $smarty->setCacheDir('cache/');

        $smarty->assign('user', $this);
        $smarty->assign('base_url', BASE_URL);

        $mail = new \PHPMailer();
        $mail->CharSet = 'utf-8';
        $mail->Encoding = '8bit';
        $mail->SetFrom(EMAIL_ADDRESS, EMAIL_NAME);
        $mail->AddAddress($this['email'], $this['firstname'].' '.$this['lastname']);
        $mail->Subject = 'Conferma di registrazione';
        $mail->Body = $smarty->fetch('email-landing.tmpl');
        $mail->Send();
    }

    public static function password_policy($user, $password)
    {
        return strlen($password) >= 6 &&
             preg_match('/[a-zA-Z]/', $password) &&
             preg_match('/[0-9]/', $password) &&
             (!$user['lastname'] || false == strstr(strtolower($password), strtolower($user['lastname']))) &&
             (!$user['firstname'] || false == strstr(strtolower($password), strtolower($user['firstname']))) &&
             (!$user['email'] || false == strstr(strtolower($password), strtolower($user['email'])));
    }

    public function update($params)
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'company = '._text(_a($params, 'company')).', '.
                        'lastname = '._text(_a($params, 'lastname')).', '.
                        'firstname = '._text(_a($params, 'firstname')).', '.
                        'email = '._text(_a($params, 'email')).', '.
                        'password = '._password_sha256(_a($params, 'password')).', '.
                        'address = '._text(_a($params, 'address')).', '.
                        'city = '._text(_a($params, 'city')).', '.
                        'province = '._text(_a($params, 'province')).', '.
                        'zipcode = '._text(_a($params, 'zipcode')).', '.
                        'country = '._text(_a($params, 'country')).', '.
                        'mobile = '._text(_a($params, 'mobile')).', '.
                        'phone = '._text(_a($params, 'phone')).', '.
                        'roles = '._from_array(_a($params, 'roles')).', '.
                        'privacy = '._integer(_a($params, 'privacy')).', '.
                        'privacy2 = '._integer(_a($params, 'privacy2')).', '.
                        'newsletter = '._integer(_a($params, 'newsletter')).', '.
                        'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::update: '.mysqli_error($db));
    }

    public function update_calendar($params)
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'days_1 = '._integer(_a($params, 'days_1')).', '.
                        'dates = '._text_json(_a($params, 'dates')).', '.
                        'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::update_calendar: '.mysqli_error($db));
    }

    public function update_admin($params)
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'company = '._text(_a($params, 'company')).', '.
                        'lastname = '._text(_a($params, 'lastname')).', '.
                        'firstname = '._text(_a($params, 'firstname')).', '.
                        'email = '._text(_a($params, 'email')).', '.
                        'password = '._password_sha256(_a($params, 'password')).', '.
                        'address = '._text(_a($params, 'address')).', '.
                        'city = '._text(_a($params, 'city')).', '.
                        'province = '._text(_a($params, 'province')).', '.
                        'zipcode = '._text(_a($params, 'zipcode')).', '.
                        'country = '._text(_a($params, 'country')).', '.
                        'mobile = '._text(_a($params, 'mobile')).', '.
                        'phone = '._text(_a($params, 'phone')).', '.
                        'privacy = '._integer(_a($params, 'privacy')).', '.
                        'privacy2 = '._integer(_a($params, 'privacy2')).', '.
                        'newsletter = '._integer(_a($params, 'newsletter')).', '.
                        'courses = '._from_array(_a($params, 'courses')).', '.
                        'groups = '._from_array(_a($params, 'groups')).', '.
                        'roles = '._from_array(_a($params, 'roles')).', '.
                        'active = '._integer(_a($params, 'active')).', '.
                        'premium = '._integer(_a($params, 'premium')).', '.
                        'dates = '._text_json(_a($params, 'dates')).', '.
                        'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::update: '.mysqli_error($db));
    }

    public function update_active($active)
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'active = '._integer($active).', '.
                        'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::update_active: '.mysqli_error($db));
    }

    public function update_password($password)
    {
        global $db;
        mysqli_query($db, 'UPDATE users SET '.
                        'password = '._password_sha256($password).', '.
                        'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in User::update_password: '.mysqli_error($db));
    }

    public function get_max_hours()
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * FROM courses_users WHERE user_id = '._integer($this['id']).' ORDER BY course_id;') or
            die('query error in User::get_max_hours: '.mysqli_error($db));
        $max_hours = [];
        while ($row = mysqli_fetch_assoc($rs)) {
            $max_hours[$row['course_id']] = $row['max_hours'];
        }

        return $max_hours;
    }

    public function set_max_hours($max_hours)
    {
        global $db;
        foreach ($max_hours as $course_id => $value) {
            $rs = mysqli_query($db, 'SELECT * FROM courses_users '.
                'WHERE course_id = '._integer($course_id).' AND user_id = '._integer($this['id']).';') or
                die('query error in User::set_max_hours: '.mysqli_error($db));
            $row = mysqli_fetch_assoc($rs);
            if (!$row) {
                if ($value) {
                    mysqli_query($db, 'INSERT INTO courses_users (user_id, course_id, max_hours, created_at) '.
                        'VALUES ('._integer($this['id']).', '._integer($course_id).', '._integer_null($value).', NOW());') or
                        die('query error in User::set_max_hours: '.mysqli_error($db));
                }
            } else {
                mysqli_query($db, 'UPDATE courses_users SET max_hours = ' . _integer_null($value) . ' WHERE id = ' . _integer($row['id']) . ';') or
                    die('query error in User::set_max_hours: '.mysqli_error($db));
            }
        }

        return $max_hours;
    }

    public function courses() {
        $courses = $this['courses'];
        foreach ($this['groups'] as $group) {
            $group = Group::get_by_id($group);
            if ($group && $group['courses']) {
                $courses = array_merge($courses, $group['courses']);
            }
        }
        return array_unique($courses);
    }

    public function __construct($value)
    {
        parent::__construct($value);
        $this['roles'] = $this['roles'] ? _to_array($this['roles']) : array();
        $this['groups'] = $this['groups'] ? _to_array($this['groups']) : array();
        $this['courses'] = $this['courses'] ? _to_array($this['courses']) : array();
    }
}
