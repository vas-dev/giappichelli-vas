<?php

  namespace models;

  class Redirect extends \ArrayObject {

    static public function add($params) {
      global $db;
      mysqli_query($db, "INSERT INTO redirects (".
                    "request_uri,".
                    "redirect_uri,".
                    "created_at) ".
                  "VALUES (".
                    _text($params['request_uri']) . ", " .
                    _text($params['redirect_uri']) . ", " .
                    "NOW());") or
                  die("query error in Redirect::add: " . mysqli_error($db));
      return mysqli_insert_id($db);
    }

    static public function get_by_id($id) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM redirects ".
                        "WHERE deleted_at IS NULL AND id = " . _integer($id) . ";") or
            die("query error in Redirect::get_by_id: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Redirect($r);
      }
    }

    static public function get_by_request_uri($request_uri) {
      global $db;
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM redirects ".
                        "WHERE deleted_at IS NULL AND request_uri = " . _text($request_uri) . ";") or
            die("query error in Redirect::get_by_request_uri: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      if ($r) {
        return new Redirect($r);
      }
    }

    static public function get($params=array(), $offset=0, $limit=20) {
      global $lang, $db;
      $where = Redirect::get_where($params);
      $rs = mysqli_query($db, "SELECT * ".
                        "FROM redirects ".
                        "WHERE deleted_at IS NULL $where ".
                        "ORDER BY request_uri ".
                        "LIMIT " . _integer($limit) . " OFFSET " . _integer($offset) . ";") or
            die("query error in Redirect::get: " . mysqli_error($db));
      $results = array();
      while ($r = mysqli_fetch_assoc($rs)) {
        array_push($results, new Redirect($r));
      }
      return $results;
    }

    static public function get_count($params) {
      global $db;
      $where = Redirect::get_where($params);
      $rs = mysqli_query($db, "SELECT COUNT(*) AS count ".
                        "FROM redirects ".
                        "WHERE deleted_at IS NULL $where;") or
            die("query error in Redirect::get_count: " . mysqli_error($db));
      $r = mysqli_fetch_assoc($rs);
      return $r['count'];
    }

    static public function get_where($params) {
      global $lang;
      $where = "";
      if (isset($params['q'])) {
        $where .= "AND (request_uri like " . _text('%' . $params['q'] . '%') . ") ";
      }
      return $where;
    }

    public function delete() {
      global $db;
      mysqli_query($db, "UPDATE redirects SET ".
                    "deleted_at = NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in Redirect::delete: " . mysqli_error($db));
    }

    public function update($params) {
      global $db;
      mysqli_query($db, "UPDATE redirects SET ".
                    "request_uri = " . _text($params["request_uri"]) . ", ".
                    "redirect_uri = " . _text($params["redirect_uri"]) . ", ".
                    "updated_at = " . "NOW() ".
                  "WHERE id = " . _integer($this['id']) . ";") or
                  die("query error in Redirect::update: " . mysqli_error($db));
    }

  }

?>
