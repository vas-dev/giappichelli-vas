<?php

namespace models;

class Lesson extends \ArrayObject
{
    public static function add($params)
    {
        global $db;
        mysqli_query($db, 'INSERT INTO lessons ('.
                    'courses,'.
                    'name,'.
                    'description,'.
                    'type,'.
                    'widget,'.
                    'widget_audio,'.
                    'lesson_date,'.
                    'lesson_time,'.
                    'files,'.
                    'homework,'.
                    'materials,'.
                    'questions,'.
                    'related,'.
                    'playlist,'.
                    'support_button,'.
                    'support_target,'.
                    'max_views,'.
                    'published,'.
                    'published_datetime,'.
                    'published_datetime2,'.
                    'created_at) '.
                  'VALUES ('.
                    _from_array($params['courses']).', '.
                    _text($params['name']).', '.
                    _text($params['description']).', '.
                    _integer($params['type']).', '.
                    _text($params['widget']).', '.
                    _text($params['widget_audio']).', '.
                    _date($params['lesson_date']).', '.
                    _text($params['lesson_time']).', '.
                    _text_json($params['files']).', '.
                    _integer($params['homework']).', '.
                    _integer($params['materials']).', '.
                    _integer($params['questions']).', '.
                    _from_array($params['related']).', '.
                    _from_array($params['playlist']).', '.
                    _text($params['support_button']).', '.
                    _text($params['support_target']).', '.
                    _integer($params['published']).', '.
                    _integer($params['max_views']).', '.
                    _datetime($params['published_datetime']).', '.
                    _datetime($params['published_datetime2']).', '.
                    'NOW());') or
                  die('query error in Lesson::add: '.mysqli_error($db));

        return mysqli_insert_id($db);
    }

    public function add_question($params)
    {
        global $db;
        mysqli_query($db, 'INSERT INTO lessons_questions ('.
                    'lesson_id,'.
                    'user_id,'.
                    'subject,'.
                    'question,'.
                    'created_at) '.
                  'VALUES ('.
                    _integer($this['id']).', '.
                    _integer($params['user_id']).', '.
                    _text($params['subject']).', '.
                    _text($params['question']).', '.
                    'NOW());') or
                  die('query error in LessonQuestion::add: '.mysqli_error($db));

        return mysqli_insert_id($db);
    }

    public function get_questions($params = array())
    {
        global $db, $lang;
        $where = '';
        if (isset($params['status']) && '' !== $params['status'] && null !== $params['status']) {
            $where .= ' AND status = '._integer($params['status']).' ';
        }
        $rs = mysqli_query($db, 'SELECT q.*, u.firstname, u.lastname '.
                        'FROM lessons_questions q '.
                        'JOIN users u ON (q.user_id = u.id) '.
                        'WHERE q.lesson_id = '._integer($this['id']).$where.' '.
                        'ORDER BY created_at DESC') or
            die('query error in LessonQuestion::get: '.mysqli_error($db));
        $results = array();
        while ($r = mysqli_fetch_assoc($rs)) {
            array_push($results, $r);
        }

        return $results;
    }

    public function set_question_status($id, $status)
    {
        global $db, $lang;
        $rs = mysqli_query($db, 'UPDATE lessons_questions SET status = '._integer($status).' '.
                              'WHERE id = '._integer($id).';') or
            die('query error in LessonQuestion::get: '.mysqli_error($db));
    }

    public static function get($params = array(), $offset = 0, $limit = 20)
    {
        global $db, $lang;
        $where = Lesson::get_where($params);
        $order_by = _a($params, 'order_by', 'l.lesson_date DESC, l.name');
        $rs = mysqli_query($db, 'SELECT DISTINCT l.* '.
                        'FROM lessons l '.
                        "JOIN courses c ON (l.courses LIKE CONCAT('%;', c.id, ';%')) ".
                        "WHERE l.deleted_at IS NULL $where ".
                        'ORDER BY '.$order_by.' '.
                        'LIMIT '._integer($limit).' OFFSET '._integer($offset).';') or
            die('query error in Lesson::get: '.mysqli_error($db));
        $results = array();
        while ($r = mysqli_fetch_assoc($rs)) {
            array_push($results, new Lesson($r));
        }

        return $results;
    }

    public static function get_count($params)
    {
        global $db;
        $where = Lesson::get_where($params);
        $rs = mysqli_query($db, 'SELECT COUNT(DISTINCT l.id) AS count '.
                        'FROM lessons l '.
                        "JOIN courses c ON (l.courses LIKE CONCAT(';', c.id, '%')) ".
                        "WHERE l.deleted_at IS NULL $where;") or
            die('query error in Lesson::get_count: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);

        return $r['count'];
    }

    public static function get_where($params)
    {
        global $lang, $principal, $principal_id;
        $where = '';
        if (isset($params['q']) && $params['q']) {
            $where .= 'AND (l.name like '._text('%'.$params['q'].'%').') ';
        }
        if (isset($params['course_id']) && $params['course_id']) {
            $where .= 'AND (courses LIKE '._text('%;'.$params['course_id'].';%').') ';
        }
        if (isset($params['id:not']) && $params['id:not']) {
            $where .= 'AND (l.id <> '._integer($params['id:not']).') ';
        }
        if (isset($params['type:not']) && $params['type:not']) {
            $where .= 'AND (l.type <> '._integer($params['type:not']).') ';
        }
        if (isset($params['ids']) && $params['ids']) {
            $ors = [];
            foreach ($params['ids'] as $i) {
                $ors[] = 'l.id = '._integer($i);
            }
            $where .= 'AND ('.implode(' OR ', $ors).') ';
        }
        if (isset($params['courses']) && $params['courses']) {
            $ors = [];
            foreach ($params['courses'] as $c) {
                $ors[] = 'courses LIKE '._text('%;'.$c.';%').'';
            }
            $where .= 'AND ('.implode(' OR ', $ors).') ';
        }
        if (isset($params['year']) && $params['year']) {
            $where .= 'AND (extract(year from lesson_date) = '._integer($params['year']).') ';
        }
        if (isset($params['month']) && $params['month']) {
            $where .= 'AND (extract(month from lesson_date) = '._integer($params['month']).') ';
        }
        if (isset($params['lesson_date:lte']) && $params['lesson_date:lte']) {
            $where .= 'AND (lesson_date <= '._text($params['lesson_date:lte']).') ';
        }
        if (isset($params['lesson_date:lt']) && $params['lesson_date:lt']) {
            $where .= 'AND (lesson_date < '._text($params['lesson_date:lt']).') ';
        }
        if (isset($params['lesson_date:gt']) && $params['lesson_date:gt']) {
            $where .= 'AND (lesson_date > '._text($params['lesson_date:gt']).') ';
        }
        if (isset($params['lesson_date:gte']) && $params['lesson_date:gte']) {
            $where .= 'AND (lesson_date >= '._text($params['lesson_date:gte']).') ';
        }
        if (isset($params['published']) && $params['published']) {
            $where .= 'AND (l.published = '._integer($params['published']).') ';
            $where .= 'AND (l.published_datetime IS NULL or l.published_datetime <= NOW()) ';
            $where .= 'AND (l.published_datetime2 IS NULL or l.published_datetime2 > NOW()) ';
        }

        return $where;
    }

    public static function get_vocabulary($params = array())
    {
        $r = array();
        foreach (Lesson::get($params, 0, 9999) as $lesson) {
            $r[$lesson['id']] = $lesson['name'];
        }

        return $r;
    }

    public static function get_by_id($id)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * '.
                        'FROM lessons '.
                        'WHERE deleted_at IS NULL AND id = '._integer($id).';') or
            die('query error in Lesson::get_by_id: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Lesson($r);
        }
    }

    public static function get_by_course_and_id($course_id, $id)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * '.
                        'FROM lessons '.
                        'WHERE deleted_at IS NULL AND published = 1 AND courses LIKE '._text('%;'.$course_id.';%').' AND id = '._integer($id).';') or
            die('query error in Lesson::get_by_id: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Lesson($r);
        }
    }

    public function delete()
    {
        global $db;
        mysqli_query($db, 'UPDATE lessons SET '.
                    'deleted_at = NOW() '.
                  'WHERE id = '._integer($this['id']).';') or
                  die('query error in Lesson::delete: '.mysqli_error($db));
    }

    public function update($params)
    {
        global $db;
        mysqli_query($db, 'UPDATE lessons SET '.
                    'courses = '._from_array($params['courses']).', '.
                    'name = '._text($params['name']).', '.
                    'description = '._text($params['description']).', '.
                    'type = '._integer($params['type']).', '.
                    'widget = '._text($params['widget']).', '.
                    'widget_audio = '._text($params['widget_audio']).', '.
                    'lesson_date = '._date($params['lesson_date']).', '.
                    'lesson_time = '._text($params['lesson_time']).', '.
                    'files = '._text_json($params['files']).', '.
                    'homework = '._integer($params['homework']).', '.
                    'materials = '._integer($params['materials']).', '.
                    'questions = '._integer($params['questions']).', '.
                    'related = '._from_array($params['related']).', '.
                    'playlist = '._from_array($params['playlist']).', '.
                    'support_button = '._text($params['support_button']).', '.
                    'support_target = '._text($params['support_target']).', '.
                    'max_views = '._integer(_a($params, 'max_views')).', '.
                    'published = '._integer($params['published']).', '.
                    'published_datetime = '._datetime($params['published_datetime']).', '.
                    'published_datetime2 = '._datetime($params['published_datetime2']).', '.
                    'updated_at = '.'NOW() '.
                  'WHERE id = '._integer($this['id']).';') or
                  die('query error in Lesson::update: '.mysqli_error($db));
    }

    public function files()
    {
        $folders = array();
        $current_folder = null;
        foreach ($this['files'] as $file) {
            $folder = null;
            if (preg_match("/(.*) *\/ *(.*)/", _a($file, 'title'), $matches)) {
                $folder = $matches[1];
                $title = $matches[2];
            } else {
                $folder = 'Materiali';
                $title = _a($file, 'title');
            }
            if (!isset($folders[$folder])) {
                $folders[$folder] = array();
            }
            $file['title'] = $title;
            $folders[$folder][] = $file;
        }

        return $folders;
    }

    public function get_views_for_user($user)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT COUNT(*) AS count FROM lessons_users '.
                              'WHERE lesson_id = '._integer($this['id']).' AND user_id = '._integer($user['id']).';') or
                              die('query error in Lesson::is_visible_for_user: '.mysqli_error($db));
        $row = mysqli_fetch_assoc($rs);
        if (!$row) {
            return 0;
        } else {
            return $row['count'];
        }
    }

    public function add_view_for_user($user) {
        global $db;
        mysqli_query($db, 'INSERT INTO lessons_users (user_id, lesson_id, created_at) VALUES ('._integer($user['id']).', '._integer($this['id']).', NOW());') or
            die('query error in Lesson::is_visible_for_user: '.mysqli_error($db));
    }

    public function playlist($course_id)
    {
        $playlist = $this['playlist'];
        if (!$playlist) {
            return [];
        }

        return Lesson::get([
            'course_id' => $course_id,
            'published' => 1,
            'ids' => $playlist,
            'order_by' => 'l.lesson_date ASC',
        ], 0, 100);
    }

    public function related($course_id)
    {
        $related = $this['related'];
        if (!$related) {
            return [];
        }

        return Lesson::get([
            'course_id' => $course_id,
            'published' => 1,
            'ids' => $related,
            'order_by' => 'l.lesson_date ASC',
        ], 0, 100);
    }

    public function parent()
    {
        global $db;
        $where = '';
        if (isset($params['courses']) && $params['courses']) {
            $ors = [];
            foreach ($params['courses'] as $c) {
                $ors[] = 'courses LIKE '._text('%;'.$c.';%').'';
            }
            $where .= 'AND ('.implode(' OR ', $ors).') ';
        }
        $rs = mysqli_query($db, 'SELECT * '.
                        'FROM lessons '.
                        'WHERE deleted_at IS NULL ' . $where . ' AND playlist LIKE '._text('%;' . $this['id'] . ';%').';') or
            die('query error in Lesson::parent: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Lesson($r);
        }
    }

    public function name()
    {
        return preg_replace('/ *-? *playlist/i', '', $this['name']);
    }

    public function __construct($value)
    {
        parent::__construct($value);
        $this['files'] = $this['files'] ? json_decode($this['files'], true) : array();
        $this['courses'] = $this['courses'] ? _to_array($this['courses']) : array();
        $this['related'] = $this['related'] ? _to_array($this['related']) : array();
        $this['playlist'] = $this['playlist'] ? _to_array($this['playlist']) : array();
    }
}
