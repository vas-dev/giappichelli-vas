<?php

namespace models;

class Group extends \ArrayObject
{
    public static function add($params)
    {
        global $db;
        mysqli_query($db, 'INSERT INTO groups ('.
                        'name,'.
                        'courses,'.
                        'created_at'.
                      ') VALUES ('.
                        _text(_a($params, 'name')).', '.
                        _from_array(_a($params, 'courses')).', '.
                        'NOW()'.
                      ');')
                      or die('query error in Group::add: '.mysqli_error($db));

        return mysqli_insert_id($db);
    }

    public static function get_by_id($id)
    {
        global $db;
        $rs = mysqli_query($db, 'SELECT * FROM groups WHERE id = '._integer($id).';') or
            die('query error in Group::get_by_id: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);
        if ($r) {
            return new Group($r);
        }

        return null;
    }

    public static function get($params = array(), $offset = 0, $limit = 25)
    {
        global $db;
        $where = Group::get_where($params);
        $columns = _a($params, 'columns');
        $order_by = _a($params, 'order_by', 'lower(c.name), c.id');
        $rs = mysqli_query($db, "SELECT c.* $columns".
                        'FROM groups c '.
                        "WHERE c.deleted_at IS NULL $where ".
                        "ORDER BY $order_by ".
                        'LIMIT '._integer($limit).' OFFSET '._integer($offset).';') or
            die('query error in Group::get: '.mysqli_error($db));
        $results = array();
        while ($r = mysqli_fetch_assoc($rs)) {
            array_push($results, new Group($r));
        }

        return $results;
    }

    public static function get_count($params)
    {
        global $db;
        $where = Group::get_where($params);
        $rs = mysqli_query($db, 'SELECT COUNT(*) AS count '.
                        'FROM groups c '.
                        "WHERE c.deleted_at IS NULL $where;") or
            die('query error in Group::get_count: '.mysqli_error($db));
        $r = mysqli_fetch_assoc($rs);

        return $r['count'];
    }

    public static function get_where(&$params)
    {
        global $lang;
        $where = '';
        if (isset($params['q']) && $params['q']) {
            $where .= 'AND (lower(c.name) like '._text('%'.mb_strtolower($params['q']).'%').') ';
        }
        if (isset($params['name']) && $params['name']) {
            $where .= 'AND (lower(c.name) = '._text(mb_strtolower($params['name'])).') ';
        }

        return $where;
    }

    public static function get_vocabulary($params = array())
    {
        $r = array();
        foreach (Group::get($params, 0, 9999) as $group) {
            $r[$group['id']] = $group['name'];
        }

        return $r;
    }

    public function delete()
    {
        global $db;
        mysqli_query($db, 'UPDATE groups SET '.
                        'deleted_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in Group::delete: '.mysqli_error($db));
    }

    public function update($params)
    {
        global $db;
        mysqli_query($db, 'UPDATE groups SET '.
                            'name = '._text(_a($params, 'name')).', '.
                            'courses = '._from_array(_a($params, 'courses')).', '.
                            'updated_at = NOW() '.
                      'WHERE id = '._integer($this['id']).';') or
                      die('query error in Group::update: '.mysqli_error($db));
    }

    public function __construct($value)
    {
        parent::__construct($value);
        $this['courses'] = $this['courses'] ? _to_array($this['courses']) : array();
    }
}
