<?php
  function _smarty() {
    global $_COOKIE;
    $smarty = new Smarty();
    $smarty->setTemplateDir('templates/');
    $smarty->setCompileDir('templates_c/');
    $smarty->setConfigDir('config/');
    $smarty->setCacheDir('cache/');
    $smarty->assign("debug", DEBUG);
    $smarty->assign("protocol", PROTOCOL);
    $smarty->assign("base_url_raw", BASE_URL_RAW);
    $smarty->assign("cdn_url", CDN_URL);
    $smarty->assign("key_assets", KEY_ASSETS);
    $smarty->assign("recaptcha_key", RECAPTCHA_KEY);
    $smarty->assign("menu", null);
    $smarty->assign("submenu", null);
    $smarty->assign("principal", null);
    $smarty->assign("COOKIE", $_COOKIE);
    return $smarty;
  }

  $smarty = _smarty();

  $LANGS = array(
    'it' => 'Italiano',
    // 'en' => 'English',
    // 'de' => 'Deutsch',
  );

  $smarty->assign("LANGS", $LANGS);

  require("helpers/misc.inc.php");

  // i18n
  if ($request_parts && isset($LANGS[$request_parts[0]])) {
    $lang = $request_parts[0];
    $request_parts = array_slice($request_parts, 1);
    $request_uri = "/" . implode("/", $request_parts);
  } else if (($lang = _request('lang')) && isset($LANGS[$lang])) {
    //
  } else if (($lang = _a($_COOKIE, 'lang')) && isset($LANGS[$lang])) {
    //
  } else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $lang = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
    switch ($lang){
      case 'it':
        $lang = 'it';
        break;
      default:
        $lang = 'it';
        break;
    }
  } else {
    $lang = _a($_COOKIE, 'lang', 'it');
  }

  if ($lang != _a($_COOKIE, 'lang')) {
    setcookie('lang', $lang, time()+365*24*3600, "/", COOKIE_DOMAIN);
  }

  define("BASE_URL", BASE_URL_RAW . ($lang != 'it' && _a($request_parts, 0) != 'admin' ? "/" . $lang : ""));

  $smarty->assign("lang", $lang);
  $smarty->assign("request_uri", $request_uri);
  $smarty->assign("base_url", BASE_URL);

  if ($lang == "it") {
    $language = "it_IT.UTF8";
  } else if ($lang == "de") {
    $language = "de_DE.UTF8";
  } else {
    $language = "en_GB.UTF8";
  }

  putenv("LC_ALL=$language");
  setlocale(LC_ALL, $language);
  $domain = "messages";
  if (file_exists(DIR_ASSETS . "/../../src/locales/" . $language . "/LC_MESSAGES/messages.rev")) {
    $rev = file_get_contents(DIR_ASSETS . "/../../src/locales/" . $language . "/LC_MESSAGES/messages.rev");
    $domain = "messages_r" . $rev;
  }

  bindtextdomain($domain, DIR_ASSETS . "/../../src/locales");
  textdomain($domain);

  require("helpers/vocabularies.inc.php");
?>
