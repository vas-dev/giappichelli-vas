<?php

  function _a($a, $k, $d=null) {
    return isset($a[$k]) ? $a[$k] : $d;
  }

  function _u($v) {
    return urlencode(strtolower($v));
  }

  function _from_array($v) {
    if ($v) {
      $v = array_unique($v);
      sort($v);
      return _text(";" . implode(";", $v) . ";");
    }
    return "null";
  }

  function _from_sorted_array($v) {
    if ($v) {
      $v = array_unique($v);
      return _text(";" . implode(";", $v) . ";");
    }
    return "null";
  }

  function _to_array($v) {
    $v = trim($v, ";");
    $result = array();
    if ($v != "") {
      foreach (explode(";", $v) as $key) {
        array_push($result, $key);
      }
    }
    $result = array_unique($result);
    sort($result);
    return $result;
  }

  function _to_sorted_array($v) {
    $v = trim($v, ";");
    $result = array();
    if ($v != "") {
      foreach (explode(";", $v) as $key) {
        array_push($result, $key);
      }
    }
    $result = array_unique($result);
    return $result;
  }

  function _unquote($v) {
    return (get_magic_quotes_gpc() && is_string($v) ? trim(stripslashes($v)) : (is_string($v) ? trim($v) : $v));
  }

  function _get($k, $d=null) {
    return array_key_exists($k, $_GET) ? _unquote($_GET[$k]) : $d;
  }

  function _post($k, $d=null) {
    return array_key_exists($k, $_POST) ? _unquote($_POST[$k]) : $d;
  }

  function _request($k, $d=null) {
    return array_key_exists($k, $_REQUEST) ? _unquote($_REQUEST[$k]) : $d;
  }

  function _post_date($k) {
    $day = array_key_exists($k . "_day", $_POST) && $_POST[$k . "_day"] ? $_POST[$k . "_day"]*1 : null;
    $month = array_key_exists($k . "_month", $_POST) && $_POST[$k . "_month"] ? $_POST[$k . "_month"]*1 : null;
    $year = array_key_exists($k . "_year", $_POST) && $_POST[$k . "_year"] ? $_POST[$k . "_year"]*1 : null;
    if ($day && $month && $year) {
      return sprintf("%02d", $day) . "/" . sprintf("%02d", $month) . "/" . sprintf("%04d", $year);
    }
    return null;
  }

  function _post_datetime($k) {
    $value = _post_date($k);
    if (!$value) {
      return null;
    }
    $hour = array_key_exists($k . "_hour", $_POST) && $_POST[$k . "_hour"] ? $_POST[$k . "_hour"]*1 : null;
    $minute = array_key_exists($k . "_minute", $_POST) && $_POST[$k . "_minute"] ? $_POST[$k . "_minute"]*1 : null;
    return $value . " " . sprintf("%02d", $hour) . ":" . sprintf("%02d", $minute) . ":00";
  }

  function _post_fileupload($k) {
    $v = _post($k);
    $v_title = _post($k . "_title");
    $v_description = _post($k . "_description");
    if (is_array($v)) {
      $data = array();
      foreach ($v as $i => $f) {
        $v = json_decode(_unquote($f), true);
        if (is_array($v_title) && isset($v_title[$i])) {
          $v['title'] = $v_title[$i];
        }
        if (is_array($v_description) && isset($v_description[$i])) {
          $v['description'] = $v_description[$i];
        }
        array_push($data, $v);
      }
      return $data;
    }
    return null;
  }

  function _post_tags($k) {
    $v = _post($k);
    return ($v ? explode(",", $v) : null);
  }

  function _image($k, $category, $w=null, $h=null) {
    $image = _file($k, $category);
    if ($image) {
      if ($w && $h) {
        $image['asset'] = _thumbnail($image['path'], $w, $h, $category);
      } else if ($w) {
        $image['asset'] = _resize($image['path'], $w, $category);
      }
    } else {
      $image = _post($k);
      if ($image) {
        $image = _image_local($image, $category, array());
        $image = array(
          "asset" => $image
        );
      }
    }
    return $image;
  }

  function _image_local($image, $category, $params=array()) {
    if (!$image || !preg_match('/^http/i', $image)) {
      return $image;
    }
    $filename = tempnam(sys_get_temp_dir(), 'download-');
    $curl = curl_init($image);
    curl_setopt_array($curl, array(
      CURLOPT_URL => $image,
      CURLOPT_BINARYTRANSFER => 1,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_FILE => fopen($filename, 'wb+'),
      CURLOPT_TIMEOUT  => 60,
      CURLOPT_USERAGENT => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)'
    ));
    $response = curl_exec($curl);
    if ($response === false) {
      unlink($file);
      return $image;
    }
    $md5 = md5_file($filename);
    $asset = _asset($category, $md5, preg_replace('/.*\./', '', $image));
    $asset_path = _asset_path($asset);
    rename($filename, $asset_path);
    return $asset;
  }

  function _file($k, $category) {
    if (!array_key_exists($k, $_FILES)) {
      return array();
    }
    $result = array();
    $files = $_FILES[$k];
    if ($files['error'] == 0) {
      $ext = null;
      $type = $files['type'];
      $name = $files['name'];
      if ($type == "image/jpeg" || preg_match('/.*\.jpg$/i', $name)) {
        $ext = "jpg";
      } else if ($type == "image/png" || preg_match('/.*\.png$/i', $name)) {
        $ext = "png";
      } else if ($type == "image/gif" || preg_match('/.*\.gif$/i', $name)) {
        $ext = "gif";
      } else if ($type == "application/pdf" || preg_match('/.*\.pdf$/i', $name)) {
        $ext = "pdf";
      } else if ($type == "audio/mp3" || preg_match('/.*\.mp3$/i', $name)) {
        $ext = "mp3";
      } else if ($type == "video/mp4" || preg_match('/.*\.mp4$/i', $name)) {
        $ext = "mp4";
      } else if ($type == "application/doc" || preg_match('/.*\.doc$/i', $name)) {
        $ext = "doc";
      } else if ($type == "application/docx" || preg_match('/.*\.docx$/i', $name)) {
        $ext = "docx";
      } else if ($type == "application/rtf" || preg_match('/.*\.rtf$/i', $name)) {
        $ext = "rtf";
      } else if ($type == "application/osd" || preg_match('/.*\.osd$/i', $name)) {
        $ext = "osd";
      } else if ($type == "text/plain" || preg_match('/.*\.txt$/i', $name)) {
        $ext = "txt";
      } else if ($type == "application/zip" || preg_match('/.*\.zip$/i', $name)) {
        $ext = "zip";
      }
      if ($ext) {
        $md5 = md5_file($files['tmp_name']);
        $asset = _asset($category, $md5, $ext);
        $asset_path = _asset_path($asset);
        move_uploaded_file($files['tmp_name'], $asset_path);
        return array(
          "asset" => $asset,
          "path" => $asset_path,
          "ext" => $ext,
          "md5" => $md5,
          "name" => $files['name'],
          "size" => $files['size'],
        );
      }
    }
    $file = _post($k);
    if ($file) {
      return array("asset" => $file);
    }
    return null;
  }

  function _file_media($k, $category) {
    if (!array_key_exists($k, $_FILES)) {
      return array();
    }
    $result = array();
    $files = $_FILES[$k];
    if ($files['error'] == 0) {
      $md5 = md5_file($files['tmp_name']);
      $asset = $category . "/" . _slug(_unac(preg_replace('/\.[a-zA-Z0-9]+$/', '', $files['name'])), 10) . (strpos($files['name'], ".") !== FALSE ?  "." . _unac(preg_replace('/^.*\./', '', $files['name'])) : "");
      $asset_path = _asset_path($asset);
      move_uploaded_file($files['tmp_name'], $asset_path);
      return array(
        "asset" => $asset,
        "path" => $asset_path,
        "md5" => $md5,
        "name" => $files['name'],
        "size" => $files['size']
      );
    }
    return null;
  }

  function _files($k, $category) {
    if (!array_key_exists($k, $_FILES)) {
      return array();
    }
    $result = array();
    $files = $_FILES[$k];
    foreach($files['error'] as $key => $error) {
      if ($files['error'][$key] == 0) {
        $ext = null;
        $type = $files['type'][$key];
        $name = $files['name'][$key];
        if ($type == "application/zip" || preg_match('/.*\.zip$/i', $name)) {
          $zip = zip_open($files['tmp_name'][$key]);
          if (is_resource($zip)) {
            while ($entry = zip_read($zip)) {
              $name = zip_entry_name($entry);
              $content = zip_entry_read($entry, zip_entry_filesize($entry));
              $path = tempnam(DIR_ASSETS, "zip-");
              $f = fopen($path, 'wb'); fwrite($f, $content); fclose($f);
              $type = mime_content_type($path);
              if ($type == "image/jpeg") {
                $ext = "jpg";
              } else if ($type == "image/png") {
                $ext = "png";
              } else if ($type == "image/gif") {
                $ext = "gif";
              } else {
                unlink($path);
                continue;
              }
              $md5 = md5_file($path);
              $asset = _asset($category, $md5, $ext);
              $asset_path = _asset_path($asset);
              rename($path, $asset_path);
              array_push($result, array(
                "asset" => $asset,
                "path" => $asset_path,
                "ext" => $ext,
                "md5" => $md5,
                "name" => $name
              ));
            }
          }
          continue;
        } else if ($type == "image/jpeg" || preg_match('/.*\.jpg$/i', $name)) {
          $ext = "jpg";
        } else if ($type == "image/png" || preg_match('/.*\.png$/i', $name)) {
          $ext = "png";
        } else if ($type == "image/gif" || preg_match('/.*\.gif$/i', $name)) {
          $ext = "gif";
        } else if ($type == "application/pdf" || preg_match('/.*\.pdf$/i', $name)) {
          $ext = "pdf";
        } else if ($type == "audio/mp3" || preg_match('/.*\.mp3$/i', $name)) {
          $ext = "mp3";
        } else if ($type == "video/mp4" || preg_match('/.*\.mp4$/i', $name)) {
          $ext = "mp4";
        }
        if ($ext) {
          $md5 = md5_file($files['tmp_name'][$key]);
          $asset = _asset($category, $md5, $ext);
          $asset_path = _asset_path($asset);
          move_uploaded_file($files['tmp_name'][$key], $asset_path);
          array_push($result, array(
            "asset" => $asset,
            "path" => $asset_path,
            "ext" => $ext,
            "md5" => $md5,
            "name" => $files['name'][$key]
          ));
        }
      }
    }
    return $result;
  }

  function _crop($path, $w, $h, $crop, $category=null) {
    $type = mime_content_type($path);
    if ($type == "image/jpeg") {
      $img = imagecreatefromjpeg($path);
    } else if ($type == "image/png") {
      $img = imagecreatefrompng($path);
    } else if ($type == "image/gif") {
      $img = imagecreatefromgif($path);
    }
    $thumb = imagecreatetruecolor($w, $h);
    $white = imagecolorallocate($thumb, 255, 255, 255);
    imagefill($thumb, 0, 0, $white);
    imagecopyresampled($thumb, $img, 0, 0, $crop[0], $crop[1], $w, $h, $crop[2], $crop[3]);
    $path = tempnam(DIR_ASSETS, "thumbnail-");
    imagejpeg($thumb, $path, 95);
    $md5 = md5_file($path);
    $asset = _asset(($category ? $category : "thumbnails"), $md5, "jpg");
    $asset_path = _asset_path($asset);
    rename($path, $asset_path);
    return $asset;
  }

  function _resize_c($path, $w, $h, $category) {
    $type = mime_content_type($path);
    if ($type == "image/jpeg") {
      $img = imagecreatefromjpeg($path);
    } else if ($type == "image/png") {
      $img = imagecreatefrompng($path);
    } else if ($type == "image/gif") {
      $img = imagecreatefromgif($path);
    }
    $width = imagesx($img);
    $height = imagesy($img);
    $ratio = min((float)$h/(float)$height, (float)$w/(float)$width);
    $ratio = $ratio > 1 ? 1 : $ratio;
    $thumb = imagecreatetruecolor($w, $h);
    $white = imagecolorallocate($thumb, 255, 255, 255);
    imagefill($thumb, 0, 0, $white);
    imagecopyresampled($thumb, $img, ($w-($width*$ratio))/2, ($h-($height*$ratio))/2, 0, 0, $width*$ratio, $height*$ratio, $width, $height);
    $path = tempnam(DIR_ASSETS, "thumbnail-");
    imagejpeg($thumb, $path, 95);
    $md5 = md5_file($path);
    $asset = _asset($category, $md5, "jpg");
    $asset_path = _asset_path($asset);
    rename($path, $asset_path);
    return $asset;
  }

  function _resize($path, $w, $category, $output=null) {
    $type = mime_content_type($path);
    if ($type == "image/jpeg") {
      $img = imagecreatefromjpeg($path);
    } else if ($type == "image/png") {
      $img = imagecreatefrompng($path);
    } else if ($type == "image/gif") {
      $img = imagecreatefromgif($path);
    }
    $width = imagesx($img);
    $height = imagesy($img);
    $h = $w/$width*1.0*$height;
    if ($width < $w) {
      $thumb = imagecreatetruecolor($w, $height);
      $white = imagecolorallocate($thumb, 255, 255, 255);
      imagefill($thumb, 0, 0, $white);
      imagecopyresampled($thumb, $img, ($w - $width) / 2, 0, 0, 0, $width, $height, $width, $height);
    } else {
      $thumb = imagecreatetruecolor($w, $h);
      $white = imagecolorallocate($thumb, 255, 255, 255);
      imagefill($thumb, 0, 0, $white);
      imagecopyresampled($thumb, $img, 0, 0, 0, 0, $w, $h, $width, $height);
    }
    $path = $output ? $output : tempnam(DIR_ASSETS, "thumbnail-");
    imagejpeg($thumb, $path, 95);
    if ($output) {
      return $output;
    }
    $md5 = md5_file($path);
    $asset = _asset($category, $md5, "jpg");
    $asset_path = _asset_path($asset);
    rename($path, $asset_path);
    return $asset;
  }

  function _resize_h($path, $w, $h, $category, $output=null) {
    $type = mime_content_type($path);
    if ($type == "image/jpeg") {
      $img = imagecreatefromjpeg($path);
    } else if ($type == "image/png") {
      $img = imagecreatefrompng($path);
    } else if ($type == "image/gif") {
      $img = imagecreatefromgif($path);
    }
    $width = imagesx($img);
    $height = imagesy($img);
    $ratio = (float)$h/(float)$height;
    $ratio_w = $width*$ratio;
    if ($ratio_w > $w) {
      $r = (float)$w/(float)$h;
      $c1 = array("x" => ($width / 2) - ($height * $r / 2), "y" => 0);
      $c2 = array("x" => ($width / 2) + ($height * $r / 2), "y" => $height);
      $thumb = imagecreatetruecolor($w, $h);
      imagecopyresampled($thumb, $img, 0, 0, $c1['x'], $c1['y'], $w, $h, $c2['x'] - $c1['x'], $c2['y'] - $c1['y']);
    } else {
      $thumb = imagecreatetruecolor($ratio_w, $h);
      imagecopyresampled($thumb, $img, 0, 0, 0, 0, $ratio_w, $h, $width, $height);
    }
    $path = $output ? $output : tempnam(DIR_ASSETS, "thumbnail-");
    imagejpeg($thumb, $path, 95);
    if ($output) {
      return $output;
    }
    $md5 = md5_file($path);
    $asset = _asset($category, $md5, "jpg");
    $asset_path = _asset_path($asset);
    rename($path, $asset_path);
    return $asset;
  }

  function _thumbnail($path, $w, $h, $category=null, $ext="jpg", $output=null) {
    $type = mime_content_type($path);
    if ($type == "image/jpeg") {
      $img = imagecreatefromjpeg($path);
    } else if ($type == "image/png") {
      $img = imagecreatefrompng($path);
    } else if ($type == "image/gif") {
      $img = imagecreatefromgif($path);
    }
    $r = (float)$w/(float)$h;
    $width = imagesx($img);
    $height = imagesy($img);
    $ratio = (float)$width/(float)$height;
    if ($ratio > $r) {
      $c1 = array("x" => ($width / 2) - ($height * $r / 2), "y" => 0);
      $c2 = array("x" => ($width / 2) + ($height * $r / 2), "y" => $height);
    } else {
      $c1 = array("x" => 0, "y" => 0);
      $c2 = array("x" => $width, "y" => $width / $r);
    }
    $thumb = imagecreatetruecolor($w, $h);
    imagecopyresampled($thumb, $img, 0, 0, $c1['x'], $c1['y'], $w, $h, $c2['x'] - $c1['x'], $c2['y'] - $c1['y']);
    $path = $output ? $output : tempnam(DIR_ASSETS, "thumbnail-");
    if ($ext == 'jpg') {
      imagejpeg($thumb, $path, 95);
    } else {
      imagepng($thumb, $path);
    }
    if ($output) {
      return $output;
    }
    $md5 = md5_file($path);
    $asset = _asset(($category ? $category : "thumbnails"), $md5, $ext);
    $asset_path = _asset_path($asset);
    rename($path, $asset_path);
    return $asset;
  }

  $_now_override = null;
  function _now() {
    global $_now_override;
    return ($_now_override ? $_now_override : 'now()');
  }

  function _boolean($v) {
    return ($v*1 ? "true" : "false");
  }

  function _date($v) {
    if (preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $v)) {
      return _text($v);
    }
    $v = date_parse_from_format("d/m/Y", $v);
    return ($v && $v['year'] && $v['year'] != "0000" ? "'" . $v['year'] . "-" . sprintf("%02d", $v['month']) . "-" . sprintf("%02d", $v['day']) . "'" : 'null');
  }

  function _date_json($v) {
    $v = date_parse_from_format("d/m/Y", $v);
    return ($v && $v['year'] && $v['year'] != "0000" ? $v['year'] . "-" . sprintf("%02d", $v['month']) . "-" . sprintf("%02d", $v['day']) : null);
  }

  function _datetime($v) {
    if (preg_match('/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $v)) {
      return _text($v);
    }
    $v = date_parse_from_format("d/m/Y H:M:i", $v);
    return ($v && $v['year'] && $v['year'] != "0000" ? "'" . $v['year'] . "-" . sprintf("%02d", $v['month']) . "-" . sprintf("%02d", $v['day']) . " ".
                                                             sprintf("%02d", $v['hour']) . ":" . sprintf("%02d", $v['minute']) . ":" . sprintf("%02d", $v['second']) . "'" : 'null');
  }

  function _datetime_json($v) {
    $v = date_parse_from_format("d/m/Y H:M:i", $v);
    return ($v && $v['year'] && $v['year'] != "0000" ? $v['year'] . "-" . sprintf("%02d", $v['month']) . "-" . sprintf("%02d", $v['day']) . " ".
                                                       sprintf("%02d", $v['hour']) . ":" . sprintf("%02d", $v['minute']) . ":" . sprintf("%02d", $v['second']) : null);
  }

  function _numeric($v) {
    if (is_float($v)) {
      return number_format($v, 8, ".", "");
    }
    return ($v !== null ? str_replace(",", ".", floatval(str_replace(",", ".", str_replace(".", "", $v)))) : 'null');
  }

  function _numeric_json($v) {
    return ($v !== null ? floatval(str_replace(",", ".", str_replace(".", "", $v))) : null);
  }

  function _integer($v) {
    return $v ? str_replace(".", "", $v)*1 : 0;
  }

  function _integer_null($v) {
    return ($v && $v*1 ? $v*1 : 'null');
  }

  function _float($v) {
    return number_format((float)str_replace(",", ".", str_replace(".", "", $v)), 12, ".", "");
  }

  function _password($v) {
    return ($v ? _text(md5($v)) : "password");
  }

  function _password_sha256($v) {
    return ($v ? _text(hash('sha256', $v)) : "password");
  }

  function _text($v) {
    global $db; 
    return ($v ? "'" . mysqli_real_escape_string($db, $v) . "'" : "null");
  }

  function _text_json($v) {
    if (is_array($v)) {
      $v = json_encode($v);
    }
    return _text($v);
  }

  function _text_in($v) {
    if (!$v || !is_array($v)) {
      return "()";
    }
    return "(" . implode(", ", array_map("_integer", $v)) . ")";
  }

  function _asset($category, $md5, $ext) {
    $dir = $category .  DIRECTORY_SEPARATOR . _asset_dir($md5);
    return $dir . DIRECTORY_SEPARATOR . $md5 . "." . $ext;
  }

  function _asset_dir($v) {
    return substr($v, 0, 2) . DIRECTORY_SEPARATOR .
           substr($v, 2, 2) . DIRECTORY_SEPARATOR .
           substr($v, 4, 4) . DIRECTORY_SEPARATOR .
           substr($v, 8, 8);
  }

  function _asset_path($asset) {
    $path = DIR_ASSETS . DIRECTORY_SEPARATOR . $asset;
    $dir = dirname($path);
    if (!file_exists($dir)) { mkdir($dir, 0770, true); }
    return $path;
  }

  function _unac($v) {
    return iconv('UTF-8', 'ASCII//TRANSLIT', $v);
  }

  function _slug($v) {
    $v = trim(preg_replace('/[^a-z0-9]+/', '-', strtolower(_unac($v))), '-');
    if (substr_count($v, "-") > 5) {
      $v = implode("-", array_slice(explode("-", $v), 0, 5));
    }
    return $v;
  }

  function _unslug($v) {
    return trim(preg_replace('/-.*/', '', $v));
  }

  function not_found() {
    global $smarty;
    header('HTTP/1.0 404 Not Found', true, 404);
    $smarty->display("not-found.tmpl");
    exit;
  }

  function not_found_json() {
    header('HTTP/1.0 404 Not Found', true, 404);
    header("Content-Type: application/json; charset=utf-8");
    echo json_encode(array("error" => "not found"));
    exit;
  }

  function _email($email) {
    return preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$^", strtolower($email));
  }

  function _codicefiscale($cf) {
    if($cf=='')
    return false;

    if(strlen($cf)!= 16)
    return false;

    $cf=strtoupper($cf);
    if(!preg_match("/[A-Z0-9]+$/", $cf))
    return false;
    $s = 0;
    for($i=1; $i<=13; $i+=2){
    $c=$cf[$i];
    if('0'<=$c and $c<='9')
         $s+=ord($c)-ord('0');
    else
         $s+=ord($c)-ord('A');
    }

    for($i=0; $i<=14; $i+=2){
    $c=$cf[$i];
    switch($c){
         case '0':  $s += 1;  break;
         case '1':  $s += 0;  break;
         case '2':  $s += 5;  break;
         case '3':  $s += 7;  break;
         case '4':  $s += 9;  break;
         case '5':  $s += 13;  break;
         case '6':  $s += 15;  break;
         case '7':  $s += 17;  break;
         case '8':  $s += 19;  break;
         case '9':  $s += 21;  break;
         case 'A':  $s += 1;  break;
         case 'B':  $s += 0;  break;
         case 'C':  $s += 5;  break;
         case 'D':  $s += 7;  break;
         case 'E':  $s += 9;  break;
         case 'F':  $s += 13;  break;
         case 'G':  $s += 15;  break;
         case 'H':  $s += 17;  break;
         case 'I':  $s += 19;  break;
         case 'J':  $s += 21;  break;
         case 'K':  $s += 2;  break;
         case 'L':  $s += 4;  break;
         case 'M':  $s += 18;  break;
         case 'N':  $s += 20;  break;
         case 'O':  $s += 11;  break;
         case 'P':  $s += 3;  break;
         case 'Q':  $s += 6;  break;
         case 'R':  $s += 8;  break;
         case 'S':  $s += 12;  break;
         case 'T':  $s += 14;  break;
         case 'U':  $s += 16;  break;
         case 'V':  $s += 10;  break;
         case 'W':  $s += 22;  break;
         case 'X':  $s += 25;  break;
         case 'Y':  $s += 24;  break;
         case 'Z':  $s += 23;  break;
      }
    }

    if( chr($s%26+ord('A'))!=$cf[15] )
    return false;
    return true;
  }

  function _json($v) {
    $v = json_decode($v, true);
    if (!$v) {
      $e = json_last_error();
      switch ($e) {
        case JSON_ERROR_DEPTH:
          $e = "JSON: the maximum stack depth has been exceeded"; break;
        case JSON_ERROR_STATE_MISMATCH:
          $e = "JSON: invalid or malformed JSON"; break;
        case JSON_ERROR_CTRL_CHAR:
          $e = "JSON: control character error, possibly incorrectly encoded"; break;
        case JSON_ERROR_SYNTAX:
          $e = "JSON: syntax error"; break;
        case JSON_ERROR_UTF8:
          $e = "JSON: malformed UTF-8 characters, possibly incorrectly encoded"; break;
        default:
          $e = "JSON: generic error"; break;
      }
      header('HTTP/1.0 400 Bad Request', true, 400);
      header("Content-Type: application/json; charset=utf-8");
      echo json_encode(array("error" => $e));
      exit;
    }
    return $v;
  }

?>
