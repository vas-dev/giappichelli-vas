<?php
  require("includes/config.inc.php");
  require("includes/database.inc.php");
  require("includes/memcached.inc.php");
  require("includes/misc.inc.php");

  spl_autoload_extensions(".class.php");
  spl_autoload_register();

  require("vendor/autoload.php");
?>
