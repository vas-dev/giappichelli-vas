<?php
  define('DEBUG', true);

    define('DB_HOST', getenv('DB_HOST') ?: 'localhost');
    define('DB_DATABASE', getenv('DB_DATABASE') ?: 'giappichelli_formazione');
    define('DB_USER', getenv('DB_USER') ?: 'giappichelli_formazione');
    define('DB_PASSWORD', getenv('DB_PASSWORD') ?: 'giappichelli_formazione');

  define('PROTOCOL', isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http');
  define('BASE_URL_RAW', PROTOCOL . '://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost'));
  define('CDN_URL', PROTOCOL . '://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost'));
  define('DIR_ASSETS', (isset($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : '.') . '/assets');
  define('KEY_ASSETS', 'v14');

  define('EMAIL_ADDRESS', 'commerciale@giappichelli.it');
  define('EMAIL_NAME', 'Giappichelli Formazione');
  define('EMAIL_NOTIFICATIONS', 'formazione@giappichelli.it');

  define('COOKIE_NAME', 'auth');
  define('COOKIE_DOMAIN', (isset($_SERVER['HTTP_HOST']) ? preg_replace('/:[0-9]+$/', '', $_SERVER['HTTP_HOST']) : 'localhost'));
  define('COOKIE_SECRET', '5e7f80888f3d491c4963881364048c24');

  define('RECAPTCHA_KEY', '6Lc9qgoUAAAAAC4ntTQaeQwJRelvLefsceIMEECR');
  define('RECAPTCHA_SECRET', '6Lc9qgoUAAAAAOlzFZZW-DK8_LysTyTuCTjPivu-');

  /**************************************************************************
  * apply configuration                                                     *
  **************************************************************************/

  if (DEBUG == true) {
    error_reporting(E_ALL);
    ini_set('display_errors','On');
  } else {
    error_reporting(E_ALL);
    ini_set('display_errors','Off');
    ini_set('log_errors', 'On');
  }

  /**************************************************************************
  * set locale
  **************************************************************************/

  setlocale(LC_ALL, 'it_IT.UTF8');
  date_default_timezone_set('Europe/Rome');
