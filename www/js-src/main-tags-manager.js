$(function () {
  $('[data-behaviour~=tagmanager]').each(function () {
    var $this = $(this)
    $this.tagsManager({
        replace: false
      , output: $this.siblings('.tags-hidden')
      , tagsContainer: $this.siblings('.tags-container')
    })
  })
})
