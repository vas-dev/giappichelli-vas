$(function () {
  $('.img-editable').each(function () {
    var $p = $('<div class="img-editor"><a href="" class="btn btn-primary btn-xs mt5">Modifica</a> <a href="" class="btn btn-danger btn-xs mt5">Elimina</a></div>')
      , $this = $(this)
    $p.find('.btn-primary').on('click', function (e) {
      e.preventDefault()
      featherEditor.launch({
        image: $this.attr('id'),
        url: $this.attr('src')
      });
    })
    $p.find('.btn-danger').on('click', function (e) {
      e.preventDefault()
      if (confirm('Sei sicuro di voler eliminare l\'immagine?')) {
        $this.closest('.form-group').find('input[type=hidden]').val('')
        $this.closest('.form-group').find('div.img-editor').remove()
        $this.remove()
      }
    })
    if ($(this).hasClass('img-editable-noremove')) {
      $p.find('.btn-danger').remove()
    }
    $p.insertAfter($this);
  });
  if ($('.img-editable').length > 0 && window.Aviary) {
    var featherEditor = new Aviary.Feather({
      apiKey: '258839e49b3045f0ac405a8de331ed31',
      apiVersion: 3,
      fileFormat: 'jpg',
      jpgQuality: 85,
      maxSize: 1440,
      theme: 'light',
      onSave: function(imageID, newURL) {
        var $img = $('#' + imageID);
        $img.attr('src', newURL);
        $img.prev('input[type=hidden]').val(newURL);
      }
    });
  }
});
