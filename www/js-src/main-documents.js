$(function () {
  $('.list-documents .list-documents--header > a').click(function (e) {
    e.preventDefault();
    var $target = $(e.target).closest('.list-documents--header');
    if ($target.find('.fa-chevron-down').length) {
      $target.nextUntil('.list-documents--header', '.list-documents--item').slideDown();
      $target.find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up')
    } else {
      $target.nextUntil('.list-documents--header', '.list-documents--item').slideUp();
      $target.find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down')
    }
  });
  $('.list-documents .list-documents--item').click(function (e) {
    if ($(e.target).closest('form').length) {
      return;
    }
    if ($(e.target).closest('a').attr('href') != '#' && $(e.target).closest('a').attr('href') != '') {
      return;
    }
    e.preventDefault();
    var $target = $(e.target).closest('.list-documents--item');
    if ($target.find('.fa-chevron-down').length) {
      $target.nextUntil('.list-documents--item', '.list-documents--item-file').slideDown();
      $target.find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up')
    } else {
      $target.nextUntil('.list-documents--item', '.list-documents--item-file').slideUp();
      $target.find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down')
    }
  });
});
