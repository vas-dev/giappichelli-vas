$(function () {
  $('a[rel=gallery]').on('click', function (e) {
    e.preventDefault();
  }).fancybox();
  $('body').removeClass('preload')
  $('#side-menu').metisMenu()
  $('.localscroll').localScroll({duration: 500, hash: true, offset: -15})
  $('input[type=text]').attr('autocomplete', 'off').attr('spellcheck', 'off')
  $('.navbar-minimalize').click(function(e) {
    e.preventDefault()
    var $body = $('body')
    $body.toggleClass('mini-navbar')
    if (!$body.hasClass('mini-navbar') || $body.hasClass('body-small')) {
      $('#side-menu').hide();
      setTimeout(
        function () {
          $('#side-menu').fadeIn(500);
        }, 100);
    } else if ($('body').hasClass('fixed-sidebar')){
      $('#side-menu').hide();
      setTimeout(
        function () {
          $('#side-menu').fadeIn(500);
        }, 300);
    } else {
      $('#side-menu').removeAttr('style');
    }
    $.cookie('navbar', $body.hasClass('mini-navbar') ? 'mini' : 'full', {expires: 7, path: '/'});
  })
  var $tabs = $('.sheet-tabs')
  $tabs.affix({
    offset: {
      top: function () {
        var offsetTop = $tabs.offset().top
        return (this.top = offsetTop)
      }
    }
  }).on('affix.bs.affix', function () {
    $('.sheet-body').css('padding-top', '52px')
  }).on('affix-top.bs.affix', function () {
    $('.sheet-body').css('padding-top', '20px')
  })
  $(".localscroll").localScroll({duration:500, hash:true, offset:-52});
  $('body').on('custom:datepicker', '[data-behaviour~=datepicker]', function () {
    $(this).datepicker({
        'dayNamesMin': ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"]
      , 'monthNames': ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]
      , 'dateFormat': "dd/mm/yy"
    })
  });
  $("[data-behaviour~=datepicker]").trigger('custom:datepicker');
  $("[data-behaviour~=numeric]").numeric({allowMinus: false});
})

$(window).bind("load", function() {
  if($("body").hasClass('fixed-sidebar')) {
    $('.sidebar-collapse').slimScroll({
      height: '100%',
      railOpacity: 0.9,
    });
  }
})
