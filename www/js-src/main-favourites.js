$(function () {
  $('body').on('click', '.btn-favourite', function (e) {
    e.preventDefault()
    var $this = $(e.target).closest('.btn-favourite')
      , action = $this.data('value') ? 'unlike' : 'like'
      , course = $this.data('id')
      , likes = $this.data('likes')
    $.fancybox.showLoading();
    $.ajax({
      url: Giappichelli.base_url + '/ajax/favourites',
      method: 'POST',
      data: {
        course: course,
        action: action,
        referer: self.location.href.replace(Giappichelli.base_url, ''),
      },
      success: function (data, textStatus, jqXHR) {
        if (data.status) {
          if (action == 'like') {
            $this.find('i').removeClass('fa-heart-o').addClass('fa-heart')
            $this.data('value', 1)
            $this.data('likes', likes + 1)
          } else if (action == 'unlike') {
            $this.find('i').removeClass('fa-heart').addClass('fa-heart-o')
            $this.data('value', 0)
            $this.data('likes', likes - 1)
          }
        } else if (data.error) {
          bootbox.alert(data.error);
        }
        $.fancybox.hideLoading();
      },
      error: function () {
        $.fancybox.hideLoading();
      }
    });
  });
  $('.btn-favourite').each(function () {
    var $this = $(this)
      , likes = $this.data('likes')
      , tmpl = _.template(likes == 1 ? window.Giappichelli.translations['favourites-likes-singular'] : window.Giappichelli.translations['favourites-likes']);
    $this.tooltip({placement: 'top', title: tmpl({count: likes})});
  });
});
