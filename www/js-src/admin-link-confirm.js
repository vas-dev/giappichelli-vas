$(function () {
  $('a[data-behaviour~=link_confirm]').click(function (e) {
    if (confirm('Sei sicuro di voler proseguire?')) {
      return true
    }
    return false
  })
})
