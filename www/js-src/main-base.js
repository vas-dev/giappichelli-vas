$(function () {
  $('a[rel=gallery]').on('click', function (e) {
    e.preventDefault();
  }).fancybox();
  $('a[rel=popup]').on('click', function (e) {
    e.preventDefault();
    var $this = $(e.target)
    $.fancybox.open({
        src: $this.attr('href')
      , type: 'iframe'
      , padding: 0
    });
  });
  $('a[rel=external]').attr('target', '_blank');
  $('[data-behaviour~=privacy-cookie-close]').click(function (e) {
    e.preventDefault()
    $(e.target).closest('.privacy-cookie').slideUp()
    $.ajax({
        url: '/'
      , type: 'post'
      , data: {'cookie': 1}
    })
  })
  $('a[href^=\\#]').click(function(e) {
    var anchor = $(e.target).closest('a').attr('href').substr(1)
    var $target = $('a[name=' + anchor + ']');
    if ($target.length == 0) {
      return;
    }
    e.preventDefault()
    var scrollTop = $target.offset().top
    $('html, body').stop().animate({
      scrollTop: scrollTop
    }, 1000, 'easeInOutExpo');
  })
  $('body').on('custom:datepicker', '[data-behaviour~=datepicker]', function () {
    $(this).datepicker({
        'dayNamesMin': ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa"]
      , 'monthNames': ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"]
      , 'dateFormat': "dd/mm/yy"
    })
  });
  $("[data-behaviour~=datepicker]").trigger('custom:datepicker');
  $("[data-behaviour~=numeric]").numeric({allowMinus: false});
  if (self.location.hash) {
    var $target = $(self.location.hash + '-scroll')
    if ($target.length) {
      var scrollTop = $target.offset().top
      setTimeout(function () {
        $('html, body').stop().animate({
          scrollTop: scrollTop + 150
        }, 1000, 'easeInOutExpo');
      }, 500);
    }
  }
  $('body').on('click', '#calendar-container a', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(e.target).closest('a').attr('href')
      , type: 'post'
      , data: {'calendar': 1}
      , success: function (data, textStatus, jqXHR) {
          $('#calendar-container').replaceWith(data);
        }
    });
  });
  $('.btn').addClass('hvr-curl-bottom-right');
})
