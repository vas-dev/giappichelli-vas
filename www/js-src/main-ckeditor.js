$(function () {
  var config = {
      filebrowserUploadUrl: '/admin/upload',
      filebrowserImageUploadUrl: '/admin/upload',
      autoParagraph: false,
      toolbarGroups: [
        {"name":"basicstyles","groups":["basicstyles"]},
        {"name":"links","groups":["links"]},
        {"name":"paragraph","groups":["list","blocks"]},
        {"name":"document","groups":["mode"]},
        {"name":"insert","groups":["insert"]},
        {"name":"styles","groups":["styles"]},
        {"name":"about","groups":["about"]}
      ]
  };
  config.allowedContent = true;
  $("[data-behaviour~=ckeditor]").ckeditor(config);
  $("[data-behaviour~=ckeditor]").filter('[maxlength]').each(function () {
    var that = this
    CKEDITOR.instances.experience.on('key',function(event){
        var deleteKey = 46;
        var backspaceKey = 8;
        var keyCode = event.data.keyCode;
        if (keyCode === deleteKey || keyCode === backspaceKey)
            return true;
        else
        {
            var textLimit = parseInt($(that).attr('maxlength'));
            var str = CKEDITOR.instances.experience.getData();
            if (str.length >= textLimit)
                return false;
        }
    });
  });
});
