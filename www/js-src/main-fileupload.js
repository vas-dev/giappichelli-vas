$(function () {
  $('[data-behaviour~=fileupload]').each(function () {
    var $this = $(this)
      , $table = $this.siblings('table')
      , template = _.template($table.siblings('script[type="text/html"]').html())
      , url = $this.data('url')
    $this.fileupload({
        url: url
      , formData: {}
      , type: 'POST'
      , dataType: 'json'
      , add: function (e, data) {
          $.each(data.files, function () {
            var tmpl = template({file: this, status: 0})
            $table.find('tbody').append(tmpl).closest('.table').show()
            this.context = $table.find('tr:last')
            this.context.data('data', data)
          })
          data.submit()
        }
      , done: function (e, data) {
          $.each(data.files, function (i) {
            var $context = this.context
            $context.replaceWith(template({file: this, status: 1, result: JSON.stringify(data.result[i])}))
          })
        }
      , fail: function (e, data) {
          $.each(data.files, function () {
            var $context = this.context
            $context.replaceWith(template({file: this, status: data.errorThrown == 'abort' ? 2 : 3}))
          })
        }
      , progress: function (e, data) {
          $.each(data.files, function () {
            var $context = this.context
              , progress = parseInt(data.loaded / data.total * 100, 10);
            $context.find('.progress')
                    .attr('aria-valuenow', progress)
                    .find('.bar').css('width', progress + '%');
          })
        }
    })
    $this.closest('.controls')
         .on('click', '[data-behaviour~=fileupload-cancel]', function (e) {
            e.preventDefault()
            var data = $(this).closest('tr').data('data')
            if (data && data.jqXHR) {
              data.jqXHR.abort()
            }
          })
         .on('click', '[data-behaviour~=fileupload-remove]', function (e) {
            e.preventDefault()
            $(this).closest('tr').remove()
            if ($table.find('tr').length == 0) {
              $table.hide()
            }
          })
    $table.find('tbody').sortable({
        cursor: 'move'
      , handle: '.handle'
    }).disableSelection();
  })
})
