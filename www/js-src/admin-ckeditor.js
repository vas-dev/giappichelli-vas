$(function () {
  var config = {
      filebrowserUploadUrl: '/admin/upload',
      filebrowserImageUploadUrl: '/admin/upload',
      autoParagraph: false,
      toolbarGroups: [
        {"name":"basicstyles","groups":["basicstyles"]},
        {"name":"links","groups":["links"]},
        {"name":"paragraph","groups":["list","blocks"]},
        {"name":"document","groups":["mode"]},
        {"name":"insert","groups":["insert"]},
        {"name":"styles","groups":["styles"]},
        {"name":"about","groups":["about"]}
      ]
  };
  config.allowedContent = true;
  $("[data-behaviour~=ckeditor]").ckeditor(config);
});
