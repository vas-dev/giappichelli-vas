<?php
  $smarty->registerPlugin('function', '_u', '_u');
  $smarty->registerPlugin('function', '_get', '_get');
  $smarty->registerPlugin('function', '_post', '_post');
  $smarty->registerPlugin('function', '_request', '_request');

  function _d($k, $value=null) {
    $v = _post_date($k);
    $v = ($v !== null ? $v : ($value ? _f($k, $value) : null));
    return ($v && strpos($v, "-") ? substr($v, 8, 2) . "/" . substr($v, 5, 2) . "/" . substr($v, 0, 4) : $v);
  }

  function _dt($k, $value=null) {
    $v = _post_datetime($k);
    $v = ($v !== null ? $v : ($value ? _f($k, $value) : null));
    return ($v && strpos($v, "-") ? substr($v, 8, 2) . "/" . substr($v, 5, 2) . "/" . substr($v, 0, 4) . " " . substr($v, 11, 2) . ":" . substr($v, 14, 2) . ":" . "00" : $v);
  }

  function _dd($v) {
    return ($v && strpos($v, "-") ? substr($v, 8, 2) . "/" . substr($v, 5, 2) . "/" . substr($v, 0, 4) : $v);
  }

  function _ddf($v) {
    global $MONTHS;
    return ($v && strpos($v, "-") ? substr($v, 8, 2)*1 . " " . strtolower(_v(substr($v, 5, 2)*1, $MONTHS)) . " " . substr($v, 0, 4) : $v);
  }

  function _ddt($v) {
    return ($v && strpos($v, "-") ? _dd($v) . " alle " . substr($v, 11, 5) : $v);
  }

  function _ddtf($v) {
    return ($v && strpos($v, "-") ? _ddf($v) . " alle " . substr($v, 11, 5) : $v);
  }

  function _f($k, $value=null, $d=null) {
    $v =  _post($k);
    $v = ($v !== null ? $v : ($value ? (isset($value[$k]) ? $value[$k] : $d) : $d));
    return is_string($v) ? htmlentities($v, ENT_COMPAT, 'UTF-8') : $v;
  }

  function _fd($k, $value=null, $d=null) {
    $v =  _post($k);
    $v = ($v !== null ? $v : ($value ? (isset($value[$k]) ? _dd($value[$k]) : $d) : $d));
    return is_string($v) ? htmlentities($v, ENT_COMPAT, 'UTF-8') : $v;
  }

  function _fi($k, $value=null) {
    $v =  _post($k);
    $v = ($v !== null ? $v : ($value ? (isset($value[$k]) ? iconv('ISO-8859-15', 'UTF-8', $value[$k]) : null) : null));
    return is_string($v) ? htmlentities($v, ENT_COMPAT, 'UTF-8') : $v;
  }

  function _fr($k, $value=null) {
    $v =  _request($k);
    $v = ($v !== null ? $v : ($value ? (isset($value[$k]) ? $value[$k] : null) : null));
    return is_string($v) ? htmlentities($v, ENT_COMPAT, 'UTF-8') : $v;
  }

  function _fn($k, $value=null, $n=2) {
    $v =  _post($k);
    $v = ($v !== null ? $v : ($value ? (isset($value[$k]) ? _n($value[$k], $n) : null) : null));
    return is_string($v) ? htmlentities($v, ENT_COMPAT, 'UTF-8') : $v;
  }

  function _fupload($k, $value=null) {
    $v = _f($k, $value);
    if (is_array($v)) {
      $data = array();
      foreach ($v as $i) {
        if (is_string($i)) {
          $i = json_decode($i, true);
        }
        array_push($data, $i);
      }
      return $data;
    }
    return null;
  }

  function _t($val) {
    return preg_replace("(http:[^[:space:]]+)", "<a href=\"\\1\" rel=\"external\">link</a>",
           str_replace("\n", "<br />", htmlentities($val, ENT_COMPAT, 'UTF-8')));
  }

  function _tm($val) {
    return preg_replace("/height:\s*[0-9]+px/", "", $val);
  }

  function _tr($val, $n=15, $l=100) {
    $parts = explode(" ", $val);
    if (count($parts) > $n || strlen($val) > $l) {
      $output = array();
      while ($parts && strlen(implode(" ", $output)) < $l && count($output) < $n) {
        array_push($output, array_shift($parts));
      }
      return implode(" ", $output) . "...";
    }
    return $val;
  }

  function _v($val, $options) {
    foreach ($options as $k => $v) {
      if ($k == $val) {
        return $v;
      }
    }
    return "";
  }

  function _n($val, $pos=2) {
    return ($val === null ? "" : number_format($val*1.0, $pos, ",", "."));
  }

  function _nsmall($val, $pos=2) {
    $val = _n($val, $pos);
    return substr($val, 0, strlen($val)-3) . "<small>," . substr($val, strlen($val)-2) . "</small>";
  }

  function _nn($val, $pos=2) {
    return ($val === null ? "&mdash;" : number_format($val*1.0, $pos, ",", "."));
  }

  $smarty->registerPlugin('function', '_d', '_d');
  $smarty->registerPlugin('function', '_dd', '_dd');
  $smarty->registerPlugin('function', '_ddf', '_ddf');
  $smarty->registerPlugin('function', '_f', '_f');
  $smarty->registerPlugin('function', '_fi', '_fi');
  $smarty->registerPlugin('function', '_fn', '_fn');
  $smarty->registerPlugin('function', '_fupload', '_fupload');
  $smarty->registerPlugin('function', '_t', '_t');
  $smarty->registerPlugin('function', '_tr', '_tr');
  $smarty->registerPlugin('function', '_tm', '_tm');
  $smarty->registerPlugin('function', '_v', '_v');
  $smarty->registerPlugin('function', '_n', '_n');
  $smarty->registerPlugin('function', '_nn', '_nn');

  function _datetime_combos($name, $value, $y1=17, $y2=100) {
    $html = _date_combos($name, $value, $y1, $y2);
    $hours = array('<option value=""></option>');
    for ($i = 0; $i <= 24; $i++) {
      $selected = ($value && $i == substr($value, 11, 2)*1 ? ' selected="selected"' : '');
      array_push($hours, '<option value="' . $i . '"' . $selected . '>' . sprintf("%02d", $i) . '</option>');
    }
    $minutes = array('<option value=""></option>');
    for ($i = 0; $i <= 60; $i++) {
      $selected = ($value && $i == substr($value, 14, 2)*1 ? ' selected="selected"' : '');
      array_push($minutes, '<option value="' . $i . '"' . $selected . '>' . sprintf("%02d", $i) . '</option>');
    }
    return $html . " alle " .
           '<select name="' . $name . '_hour" id="' . $name . '_hour" class="form-control date_hour" style="display: inline-block; width: auto;">' . implode("\n", $hours) . '</select> : ' . 
           '<select name="' . $name . '_minute" id="' . $name . '_minute" class="form-control date_minute" style="display: inline-block; width: auto;">' . implode("\n", $minutes) . '</select>';
  }

  $smarty->registerPlugin('function', '_datetime_combos', '_datetime_combos');

  function _date_combos($name, $value, $y1=17, $y2=100) {
    $days = array('<option value=""></option>');
    for ($i = 1; $i <= 31; $i++) {
      $selected = ($value && $i == substr($value, 0, 2)*1 ? ' selected="selected"' : '');
      array_push($days, '<option value="' . $i . '"' . $selected . '>' . $i . '</option>');
    }
    $months = array('<option value=""></option>');
    $month_names = array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
    for ($i = 1; $i <= 12; $i++) {
      $selected = ($value && $i == substr($value, 3, 2)*1 ? ' selected="selected"' : '');
      array_push($months, '<option value="' . $i . '"' . $selected . '>' . $month_names[$i-1] . '</option>');
    }
    $years = array('<option value=""></option>');
    for ($i = date('Y')*1 - $y1; $i >= date('Y')*1 - $y2; $i--) {
      $selected = ($value && $i == substr($value, 6, 4)*1 ? ' selected="selected"' : '');
      array_push($years, '<option value="' . $i . '"' . $selected . '>' . $i . '</option>');
    }
    return '<select name="' . $name . '_day" id="' . $name . '_day" class="form-control date-day" style="display: inline-block; width: auto;">' . implode("\n", $days) . '</select> / ' . 
           '<select name="' . $name . '_month" id="' . $name . '_month" class="form-control date-month" style="display: inline-block; width: auto;">' . implode("\n", $months) . '</select> / ' . 
           '<select name="' . $name . '_year" id="' . $name . '_year" class="form-control date-year" style="display: inline-block; width: auto;">' . implode("\n", $years) . '</select>';
  }

  $smarty->registerPlugin('function', '_date_combos', '_date_combos');

  function zfill($num, $lim) {
     return (strlen($num) >= $lim) ? $num : zfill("0" . $num, $lim);
  }

  $smarty->registerPlugin('function', 'zfill', 'zfill');

  function _in_array($k, $values) {
    if (!$values) {
      return false;
    }
    return in_array($k, $values);
  }

  $smarty->registerPlugin('function', '_in_array', '_in_array');

  function _half($v, $i) {
    if ($i == 0) {
      return array_slice($v, 0, ceil(count($v)/2));
    } else {
      return array_slice($v, ceil(count($v)/2));
    }
  }

  $smarty->registerPlugin('function', '_half', '_half');

  function _menu($k) {
    return models\Menu::get_by_slug($k);
  }

  $smarty->registerPlugin('function', '_menu', '_menu');

  function _file_is_visible($file) {
    global $principal;
    return !trim(_a($file, 'description')) || strstr(mb_strtolower($file['description']), mb_strtolower($principal['email'])) !== FALSE;
  }

  $smarty->registerPlugin('function', '_file_is_visible', '_file_is_visible');

