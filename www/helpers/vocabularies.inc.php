<?php

  $PROVINCES = array(
    'AG' => 'Agrigento (Sicilia)',
    'AL' => 'Alessandria (Piemonte)',
    'AN' => 'Ancona (Marche)',
    'AO' => 'Aosta (Valle d\'Aosta)',
    'AR' => 'Arezzo (Toscana)',
    'AP' => 'Ascoli Piceno (Marche)',
    'AT' => 'Asti (Piemonte)',
    'AV' => 'Avellino (Campania)',
    'BA' => 'Bari (Puglia)',
    'BT' => 'Barletta-Andria-Trani (Puglia)',
    'BL' => 'Belluno (Veneto)',
    'BN' => 'Benevento (Campania)',
    'BG' => 'Bergamo (Lombardia)',
    'BI' => 'Biella (Piemonte)',
    'BO' => 'Bologna (Emilia-Romagna)',
    'BZ' => 'Bolzano (Trentino-Alto Adige)',
    'BS' => 'Brescia (Lombardia)',
    'BR' => 'Brindisi (Puglia)',
    'CA' => 'Cagliari (Sardegna)',
    'CL' => 'Caltanissetta (Sicilia)',
    'CB' => 'Campobasso (Molise)',
    'CI' => 'Carbonia-Iglesias (Sardegna)',
    'CE' => 'Caserta (Campania)',
    'CT' => 'Catania (Sicilia)',
    'CZ' => 'Catanzaro (Calabria)',
    'CH' => 'Chieti (Abruzzo)',
    'CO' => 'Como (Lombardia)',
    'CS' => 'Cosenza (Calabria)',
    'CR' => 'Cremona (Lombardia)',
    'KR' => 'Crotone (Calabria)',
    'CN' => 'Cuneo (Piemonte)',
    'EN' => 'Enna (Sicilia)',
    'FM' => 'Fermo (Marche)',
    'FE' => 'Ferrara (Emilia-Romagna)',
    'FI' => 'Firenze (Toscana)',
    'FG' => 'Foggia (Puglia)',
    'FC' => 'Forlì-Cesena (Emilia-Romagna)',
    'FR' => 'Frosinone (Lazio)',
    'GE' => 'Genova (Liguria)',
    'GO' => 'Gorizia (Friuli-Venezia Giulia)',
    'GR' => 'Grosseto (Toscana)',
    'IM' => 'Imperia (Liguria)',
    'IS' => 'Isernia (Molise)',
    'AQ' => 'L\'Aquila (Abruzzo)',
    'SP' => 'La Spezia (Liguria)',
    'LT' => 'Latina (Lazio)',
    'LE' => 'Lecce (Puglia)',
    'LC' => 'Lecco (Lombardia)',
    'LI' => 'Livorno (Toscana)',
    'LO' => 'Lodi (Lombardia)',
    'LU' => 'Lucca (Toscana)',
    'MC' => 'Macerata (Marche)',
    'MN' => 'Mantova (Lombardia)',
    'MS' => 'Massa-Carrara (Toscana)',
    'MT' => 'Matera (Basilicata)',
    'VS' => 'Medio Campidano (Sardegna)',
    'ME' => 'Messina (Sicilia)',
    'MI' => 'Milano (Lombardia)',
    'MO' => 'Modena (Emilia-Romagna)',
    'MB' => 'Monza e della Brianza (Lombardia)',
    'NA' => 'Napoli (Campania)',
    'NO' => 'Novara (Piemonte)',
    'NU' => 'Nuoro (Sardegna)',
    'OG' => 'Ogliastra (Sardegna)',
    'OT' => 'Olbia-Tempio (Sardegna)',
    'OR' => 'Oristano (Sardegna)',
    'PD' => 'Padova (Veneto)',
    'PA' => 'Palermo (Sicilia)',
    'PR' => 'Parma (Emilia-Romagna)',
    'PV' => 'Pavia (Lombardia)',
    'PG' => 'Perugia (Umbria)',
    'PU' => 'Pesaro e Urbino (Marche)',
    'PE' => 'Pescara (Abruzzo)',
    'PC' => 'Piacenza (Emilia-Romagna)',
    'PI' => 'Pisa (Toscana)',
    'PT' => 'Pistoia (Toscana)',
    'PN' => 'Pordenone (Friuli-Venezia Giulia)',
    'PZ' => 'Potenza (Basilicata)',
    'PO' => 'Prato (Toscana)',
    'RG' => 'Ragusa (Sicilia)',
    'RA' => 'Ravenna (Emilia-Romagna)',
    'RC' => 'Reggio Calabria (Calabria)',
    'RE' => 'Reggio Emilia (Emilia-Romagna)',
    'RI' => 'Rieti (Lazio)',
    'RN' => 'Rimini (Emilia-Romagna)',
    'RM' => 'Roma (Lazio)',
    'RO' => 'Rovigo (Veneto)',
    'SA' => 'Salerno (Campania)',
    'SS' => 'Sassari (Sardegna)',
    'SV' => 'Savona (Liguria)',
    'SI' => 'Siena (Toscana)',
    'SR' => 'Siracusa (Sicilia)',
    'SO' => 'Sondrio (Lombardia)',
    'TA' => 'Taranto (Puglia)',
    'TE' => 'Teramo (Abruzzo)',
    'TR' => 'Terni (Umbria)',
    'TO' => 'Torino (Piemonte)',
    'TP' => 'Trapani (Sicilia)',
    'TN' => 'Trento (Trentino-Alto Adige)',
    'TV' => 'Treviso (Veneto)',
    'TS' => 'Trieste (Friuli-Venezia Giulia)',
    'UD' => 'Udine (Friuli-Venezia Giulia)',
    'VA' => 'Varese (Lombardia)',
    'VE' => 'Venezia (Veneto)',
    'VB' => 'Verbano-Cusio-Ossola (Piemonte)',
    'VC' => 'Vercelli (Piemonte)',
    'VR' => 'Verona (Veneto)',
    'VV' => 'Vibo Valentia (Calabria)',
    'VI' => 'Vicenza (Veneto)',
    'VT' => 'Viterbo (Lazio)',
    'EE' => _('ESTERO'),
  );

  $smarty->assign("PROVINCES", $PROVINCES);

  $COUNTRIES = array(
    "IT" => "Italia",
    "" => "---",
    "AD" => "Andorra",
    "AE" => "United Arab Emirates",
    "AF" => "Afghanistan",
    "AG" => "Antigua and Barbuda",
    "AI" => "Anguilla",
    "AL" => "Albania",
    "AM" => "Armenia",
    "AO" => "Angola",
    "AQ" => "Antarctica",
    "AR" => "Argentina",
    "AS" => "American Samoa",
    "AT" => "Austria",
    "AU" => "Australia",
    "AW" => "Aruba",
    "AX" => "Åland Islands",
    "AZ" => "Azerbaijan",
    "BA" => "Bosnia and Herzegovina",
    "BB" => "Barbados",
    "BD" => "Bangladesh",
    "BE" => "Belgium",
    "BF" => "Burkina Faso",
    "BG" => "Bulgaria",
    "BH" => "Bahrain",
    "BI" => "Burundi",
    "BJ" => "Benin",
    "BL" => "Saint Barthélemy",
    "BM" => "Bermuda",
    "BN" => "Brunei Darussalam",
    "BO" => "Bolivia",
    "BQ" => "Caribbean Netherlands ",
    "BR" => "Brazil",
    "BS" => "Bahamas",
    "BT" => "Bhutan",
    "BV" => "Bouvet Island",
    "BW" => "Botswana",
    "BY" => "Belarus",
    "BZ" => "Belize",
    "CA" => "Canada",
    "CC" => "Cocos (Keeling) Islands",
    "CD" => "Congo",
    "CF" => "Central African Republic",
    "CG" => "Congo",
    "CH" => "Switzerland",
    "CI" => "Côte d'Ivoire",
    "CK" => "Cook Islands",
    "CL" => "Chile",
    "CM" => "Cameroon",
    "CN" => "China",
    "CO" => "Colombia",
    "CR" => "Costa Rica",
    "CU" => "Cuba",
    "CV" => "Cape Verde",
    "CW" => "Curaçao",
    "CX" => "Christmas Island",
    "CY" => "Cyprus",
    "CZ" => "Czech Republic",
    "DE" => "Germany",
    "DJ" => "Djibouti",
    "DK" => "Denmark",
    "DM" => "Dominica",
    "DO" => "Dominican Republic",
    "DZ" => "Algeria",
    "EC" => "Ecuador",
    "EE" => "Estonia",
    "EG" => "Egypt",
    "EH" => "Western Sahara",
    "ER" => "Eritrea",
    "ES" => "Spain",
    "ET" => "Ethiopia",
    "FI" => "Finland",
    "FJ" => "Fiji",
    "FK" => "Falkland Islands",
    "FM" => "Micronesia",
    "FO" => "Faroe Islands",
    "FR" => "France",
    "GA" => "Gabon",
    "GB" => "United Kingdom",
    "GD" => "Grenada",
    "GE" => "Georgia",
    "GF" => "French Guiana",
    "GG" => "Guernsey",
    "GH" => "Ghana",
    "GI" => "Gibraltar",
    "GL" => "Greenland",
    "GM" => "Gambia",
    "GN" => "Guinea",
    "GP" => "Guadeloupe",
    "GQ" => "Equatorial Guinea",
    "GR" => "Greece",
    "GS" => "South Georgia and the South Sandwich Islands",
    "GT" => "Guatemala",
    "GU" => "Guam",
    "GW" => "Guinea-Bissau",
    "GY" => "Guyana",
    "HK" => "Hong Kong",
    "HM" => "Heard and McDonald Islands",
    "HN" => "Honduras",
    "HR" => "Croatia",
    "HT" => "Haiti",
    "HU" => "Hungary",
    "ID" => "Indonesia",
    "IE" => "Ireland",
    "IL" => "Israel",
    "IM" => "Isle of Man",
    "IN" => "India",
    "IO" => "British Indian Ocean Territory",
    "IQ" => "Iraq",
    "IR" => "Iran",
    "IS" => "Iceland",
    "JE" => "Jersey",
    "JM" => "Jamaica",
    "JO" => "Jordan",
    "JP" => "Japan",
    "KE" => "Kenya",
    "KG" => "Kyrgyzstan",
    "KH" => "Cambodia",
    "KI" => "Kiribati",
    "KM" => "Comoros",
    "KN" => "Saint Kitts and Nevis",
    "KP" => "North Korea",
    "KR" => "South Korea",
    "KW" => "Kuwait",
    "KY" => "Cayman Islands",
    "KZ" => "Kazakhstan",
    "LA" => "Lao People's Democratic Republic",
    "LB" => "Lebanon",
    "LC" => "Saint Lucia",
    "LI" => "Liechtenstein",
    "LK" => "Sri Lanka",
    "LR" => "Liberia",
    "LS" => "Lesotho",
    "LT" => "Lithuania",
    "LU" => "Luxembourg",
    "LV" => "Latvia",
    "LY" => "Libya",
    "MA" => "Morocco",
    "MC" => "Monaco",
    "MD" => "Moldova",
    "ME" => "Montenegro",
    "MF" => "Saint-Martin (France)",
    "MG" => "Madagascar",
    "MH" => "Marshall Islands",
    "MK" => "Macedonia",
    "ML" => "Mali",
    "MM" => "Myanmar",
    "MN" => "Mongolia",
    "MO" => "Macau",
    "MP" => "Northern Mariana Islands",
    "MQ" => "Martinique",
    "MR" => "Mauritania",
    "MS" => "Montserrat",
    "MT" => "Malta",
    "MU" => "Mauritius",
    "MV" => "Maldives",
    "MW" => "Malawi",
    "MX" => "Mexico",
    "MY" => "Malaysia",
    "MZ" => "Mozambique",
    "NA" => "Namibia",
    "NC" => "New Caledonia",
    "NE" => "Niger",
    "NF" => "Norfolk Island",
    "NG" => "Nigeria",
    "NI" => "Nicaragua",
    "NL" => "The Netherlands",
    "NO" => "Norway",
    "NP" => "Nepal",
    "NR" => "Nauru",
    "NU" => "Niue",
    "NZ" => "New Zealand",
    "OM" => "Oman",
    "PA" => "Panama",
    "PE" => "Peru",
    "PF" => "French Polynesia",
    "PG" => "Papua New Guinea",
    "PH" => "Philippines",
    "PK" => "Pakistan",
    "PL" => "Poland",
    "PM" => "St. Pierre and Miquelon",
    "PN" => "Pitcairn",
    "PR" => "Puerto Rico",
    "PS" => "Palestine",
    "PT" => "Portugal",
    "PW" => "Palau",
    "PY" => "Paraguay",
    "QA" => "Qatar",
    "RE" => "Réunion",
    "RO" => "Romania",
    "RS" => "Serbia",
    "RU" => "Russian Federation",
    "RW" => "Rwanda",
    "SA" => "Saudi Arabia",
    "SB" => "Solomon Islands",
    "SC" => "Seychelles",
    "SD" => "Sudan",
    "SE" => "Sweden",
    "SG" => "Singapore",
    "SH" => "Saint Helena",
    "SI" => "Slovenia",
    "SJ" => "Svalbard and Jan Mayen Islands",
    "SK" => "Slovakia",
    "SL" => "Sierra Leone",
    "SM" => "San Marino",
    "SN" => "Senegal",
    "SO" => "Somalia",
    "SR" => "Suriname",
    "SS" => "South Sudan",
    "ST" => "Sao Tome and Principe",
    "SV" => "El Salvador",
    "SX" => "Sint Maarten (Dutch part)",
    "SY" => "Syria",
    "SZ" => "Swaziland",
    "TC" => "Turks and Caicos Islands",
    "TD" => "Chad",
    "TF" => "French Southern Territories",
    "TG" => "Togo",
    "TH" => "Thailand",
    "TJ" => "Tajikistan",
    "TK" => "Tokelau",
    "TL" => "Timor-Leste",
    "TM" => "Turkmenistan",
    "TN" => "Tunisia",
    "TO" => "Tonga",
    "TR" => "Turkey",
    "TT" => "Trinidad and Tobago",
    "TV" => "Tuvalu",
    "TW" => "Taiwan",
    "TZ" => "Tanzania",
    "UA" => "Ukraine",
    "UG" => "Uganda",
    "UM" => "United States Minor Outlying Islands",
    "US" => "United States",
    "UY" => "Uruguay",
    "UZ" => "Uzbekistan",
    "VA" => "Vatican",
    "VC" => "Saint Vincent and the Grenadines",
    "VE" => "Venezuela",
    "VG" => "Virgin Islands (British)",
    "VI" => "Virgin Islands (U.S.)",
    "VN" => "Vietnam",
    "VU" => "Vanuatu",
    "WF" => "Wallis and Futuna Islands",
    "WS" => "Samoa",
    "YE" => "Yemen",
    "YT" => "Mayotte",
    "ZA" => "South Africa",
    "ZM" => "Zambia",
    "ZW" => "Zimbabwe",
  );

  $smarty->assign("COUNTRIES", $COUNTRIES);

  $PAGE_CONTENT_TYPES = array(
    1 => 'Hero',
    2 => 'Breadcrumbs',
    11 => 'Testo',
    12 => 'Testo su 2 colonne',
    13 => 'Testo su 3 colonne',
    14 => 'Testo su 4 colonne',
    21 => 'Testo a sinistra, immagine a destra',
    22 => 'Immagine a sinistra, testo a destra',
    31 => 'Landing page',
  );

  $smarty->assign("PAGE_CONTENT_TYPES", $PAGE_CONTENT_TYPES);

  $PAGE_CONTENT_CONDITIONS = array(
    1 => 'Mostra a tutti',
    2 => 'Mostra solo agli utenti anonimi',
    3 => 'Mostra solo agli utenti autenticati',
  );

  $smarty->assign("PAGE_CONTENT_CONDITIONS", $PAGE_CONTENT_CONDITIONS);

  $ROLES = array(
    1 => 'Amministratore',
    2 => 'Editor contenuti'
  );

  $smarty->assign("ROLES", $ROLES);

  $PLACE_TYPES = array(
    1 => _('Affittacamere'),
    2 => _('Agriturismo'),
    3 => _('Bed &amp; Breakfast'),
    4 => _('Hotel'),
    5 => _('Altro'),
  );

  $smarty->assign("PLACE_TYPES", $PLACE_TYPES);

  $PLACE_TYPES_ALL = array(
    1 => _('Affittacamere'),
    2 => _('Agriturismo'),
    3 => _('Bed &amp; Breakfast'),
    4 => _('Hotel'),
    5 => _('Altro'),
    99 => _('Luogo'),
  );

  $smarty->assign("PLACE_TYPES_ALL", $PLACE_TYPES_ALL);

  $PLACE_SERVICES = array(
    1 => _("Pernottamento"),
    2 => _("Ristorazione"),
    3 => _("Altro"),
  );

  $smarty->assign("PLACE_SERVICES", $PLACE_SERVICES);

  $PLACE_SPACES = array(
    1 => _("Spazi chiusi"),
    2 => _("Spazi aperti"),
    3 => _("Cucina attrezzata"),
    4 => _("Piscina"),
    5 => _("Altro"),
  );

  $smarty->assign("PLACE_SPACES", $PLACE_SPACES);

  $COURSE_TYPES = array(
    1 => _('Categoria 1'),
    2 => _('Categoria 2'),
    3 => _('Categoria 3'),
    4 => _('Categoria 4'),
    5 => _('Categoria 5'),
    6 => _('Categoria 6'),
    7 => _('Categoria 7'),
    8 => _('Categoria 8'),
    9 => _('Categoria 9'),
  );

  $smarty->assign("COURSE_TYPES", $COURSE_TYPES);

  $USER_ROLES = array(
    1 => _('Studente'),
  );

  $smarty->assign("USER_ROLES", $USER_ROLES);

  $USER_ROLES_ADMIN = array(
    1 => _('Studente'),
    2 => _('Docente'),
  );

  $smarty->assign("USER_ROLES_ADMIN", $USER_ROLES_ADMIN);

  $LANGUAGES = array(
    1 => _('Italiano'),
    2 => _('Inglese'),
    3 => _('Francese'),
    4 => _('Spagnolo'),
    5 => _('Tedesco'),
    6 => _('Cinese'),
    7 => _('Croato'),
    8 => _('Danese'),
    9 => _('Greco'),
    10 => _('Giapponese'),
    11 => _('Olandese'),
    13 => _('Norvegese'),
    13 => _('Polacco'),
    14 => _('Portoghese'),
    15 => _('Romeno'),
    16 => _('Russo'),
    17 => _('Svedese'),
  );

  $smarty->assign("LANGUAGES", $LANGUAGES);

  $HOURS = array(
    0 => '00:00',
    1 => '01:00',
    2 => '02:00',
    3 => '03:00',
    4 => '04:00',
    5 => '05:00',
    6 => '06:00',
    7 => '07:00',
    8 => '08:00',
    9 => '09:00',
    10 => '10:00',
    11 => '11:00',
    12 => '12:00',
    13 => '13:00',
    14 => '14:00',
    15 => '15:00',
    16 => '16:00',
    17 => '17:00',
    18 => '18:00',
    19 => '19:00',
    20 => '20:00',
    21 => '21:00',
    22 => '22:00',
    23 => '23:00',
    24 => '24:00',
  );

  $smarty->assign("HOURS", $HOURS);

  $TIMES = array(
    '00:00' => '00:00',
    '00:30' => '00:30',
    '01:00' => '01:00',
    '01:30' => '01:30',
    '02:00' => '02:00',
    '02:30' => '02:30',
    '03:00' => '03:00',
    '03:30' => '03:30',
    '04:00' => '04:00',
    '04:30' => '04:30',
    '05:00' => '05:00',
    '05:30' => '05:30',
    '06:00' => '06:00',
    '06:30' => '06:30',
    '07:00' => '07:00',
    '07:30' => '07:30',
    '08:00' => '08:00',
    '08:30' => '08:30',
    '09:00' => '09:00',
    '09:30' => '09:30',
    '10:00' => '10:00',
    '10:30' => '10:30',
    '11:00' => '11:00',
    '11:30' => '11:30',
    '12:00' => '12:00',
    '12:30' => '12:30',
    '13:00' => '13:00',
    '13:30' => '13:30',
    '14:00' => '14:00',
    '14:30' => '14:30',
    '15:00' => '15:00',
    '15:30' => '15:30',
    '16:00' => '16:00',
    '16:30' => '16:30',
    '17:00' => '17:00',
    '17:30' => '17:30',
    '18:00' => '18:00',
    '18:30' => '18:30',
    '19:00' => '19:00',
    '19:30' => '19:30',
    '20:00' => '20:00',
    '20:30' => '20:30',
    '21:00' => '21:00',
    '21:30' => '21:30',
    '22:00' => '22:00',
    '22:30' => '22:30',
    '23:00' => '23:00',
    '23:30' => '23:30',
    '24:00' => '24:00',
  );

  $smarty->assign("TIMES", $TIMES);

  $DOWS = array(
    1 => _('Lunedì'),
    2 => _('Martedì'),
    3 => _('Mercoledì'),
    4 => _('Giovedì'),
    5 => _('Venerdì'),
    6 => _('Sabato'),
    7 => _('Domenica'),
  );

  $smarty->assign("DOWS", $DOWS);

  $DAY_TYPES = array(
    1 => _('Disponibile'),
    2 => _('Occupato'),
  );

  $smarty->assign("DAY_TYPES", $DAY_TYPES);

  $COURSE_DURATION_TYPES = array(
    6 => _('minuti'),
    1 => _('ore'),
    2 => _('giorni'),
    3 => _('week-end'),
    4 => _('settimane'),
    5 => _('mesi'),
  );

  $smarty->assign("COURSE_DURATION_TYPES", $COURSE_DURATION_TYPES);

  $COURSE_DURATION_REPEAT = array(
    1 => _('attività singola'),
    2 => _('attività multipla'),
  );

  $smarty->assign("COURSE_DURATION_REPEAT", $COURSE_DURATION_REPEAT);

  $COURSE_DURATION_FREQUENCY = array(
    1 => _("giornaliera"),
    2 => _("settimanale"),
    3 => _("quindicinale"),
    4 => _("mensile"),
    5 => _("libera"),
  );

  $smarty->assign("COURSE_DURATION_FREQUENCY", $COURSE_DURATION_FREQUENCY);

  $COURSE_LEVELS = array(
    1 => _("Principianti"),
    2 => _("Intermedi"),
    3 => _("Esperti"),
    4 => _("Tutti"),
  );

  $smarty->assign("COURSE_LEVELS", $COURSE_LEVELS);

  $COURSE_TARGETS = array(
    1 => _('bambini'),
    2 => _('vegetariani'),
    3 => _('celiaci'),
    4 => _('vegani'),
    5 => _('bio'),
    6 => _('altro'),
  );

  $smarty->assign("COURSE_TARGETS", $COURSE_TARGETS);

  $MONTHS = array(
    1 => _("Gennaio"),
    2 => _("Febbraio"),
    3 => _("Marzo"),
    4 => _("Aprile"),
    5 => _("Maggio"),
    6 => _("Giugno"),
    7 => _("Luglio"),
    8 => _("Agosto"),
    9 => _("Settembre"),
    10 => _("Ottobre"),
    11 => _("Novembre"),
    12 => _("Dicembre"),
  );

  $smarty->assign("MONTHS", $MONTHS);

  $DAYS = array(
    1 => "1",
    2 => "2",
    3 => "3",
    4 => "4",
    5 => "5",
    6 => "6",
    7 => "7",
    8 => "8",
    9 => "9",
    10 => "10",
    11 => "11",
    12 => "12",
    13 => "13",
    14 => "14",
    15 => "15",
    16 => "16",
    17 => "17",
    18 => "18",
    19 => "19",
    20 => "20",
    21 => "21",
    22 => "22",
    23 => "23",
    24 => "24",
    25 => "25",
    26 => "26",
    27 => "27",
    28 => "28",
    29 => "29",
    30 => "30",
    31 => "31",
  );

  $smarty->assign("DAYS", $DAYS);

  $PUBLISHED = array(
    0 => _("Bozza"),
    1 => _("Pubblicato"),
  );

  $smarty->assign("PUBLISHED", $PUBLISHED);

  $EVENT_STATUSES = array(
    0 => _("Bozza"),
    1 => _("Confermato"),
  );

  $smarty->assign("EVENT_STATUSES", $EVENT_STATUSES);

  $KMS = array(
    10 => "10 km",
    20 => "20 km",
    40 => "40 km",
    60 => "60 km",
    80 => "80 km",
    100 => "100 km",
    150 => "150 km",
    200 => "200 km",
  );

  $smarty->assign("KMS", $KMS);

  $PRICES = array(
    20 => "20 euro",
    40 => "40 euro",
    60 => "60 euro",
    80 => "80 euro",
    100 => "100 euro",
    150 => "150 euro",
    200 => "200 euro",
    300 => "300 euro",
    400 => "400 euro",
    500 => "500 euro",
    750 => "750 euro",
    1000 => "1000 euro",
  );

  $smarty->assign("PRICES", $PRICES);

  $HINT_KINDS = array(
    'I' => _("Inclusione"),
    'E' => _("Esclusione"),
  );

  $smarty->assign("HINT_KINDS", $HINT_KINDS);

  $BOOKING_STATUS = array(
    0 => _("In attesa"),
    1 => _("Confermata"),
    2 => _("Rifiutata"),
  );

  $smarty->assign("BOOKING_STATUS", $BOOKING_STATUS);

  $BLOG_CATEGORIES = array(
    1 => 'News ed eventi',
    2 => 'In evidenza',
    3 => 'Dicono di noi',
  );

  $smarty->assign("BLOG_CATEGORIES", $BLOG_CATEGORIES);

  $COUPON_STATUS = array(
    0 => 'bozza',
    1 => 'valido',
    2 => 'utilizzato',
  );

  $smarty->assign("COUPON_STATUS", $COUPON_STATUS);

  $CART_STATUS = array(
    0 => 'bozza',
    1 => 'confermato',
    2 => 'pagato',
    3 => 'rifiutato',
  );

  $smarty->assign("CART_STATUS", $CART_STATUS);

  $BANNER_CATEGORIES = array(
    1 => 'CMS: lanci',
    2 => 'CMS: blocchi',
    3 => 'Struttura: blocchi',
    4 => 'Pacchetto: blocchi',
  );

  $smarty->assign("BANNER_CATEGORIES", $BANNER_CATEGORIES);

  $LESSON_TYPES = array(
    1 => 'Lezione live',
    2 => 'Lezione video',
    99 => 'Video per playlist',
  );

  $smarty->assign("LESSON_TYPES", $LESSON_TYPES);

  $YES_NO = array(
    0 => 'No',
    1 => 'Sì',
  );

  $smarty->assign("YES_NO", $YES_NO);

