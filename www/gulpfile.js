var gulp = require('gulp');
var concat = require('gulp-concat');
var expect = require('gulp-expect-file');
var if_ = require('gulp-if');
var jshint = require('gulp-jshint');
var minifycss = require('gulp-minify-css');
var newer = require('gulp-newer');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');

var paths = {
    fonts: [
      'node_modules/bootstrap-sass/assets/fonts/bootstrap/*',
      'node_modules/font-awesome/fonts/*',
    ],
    sass: [
        './scss/*.scss'
    ],
    scripts_admin: [
        './js-src/admin-*.js'
    ],
    scripts_main: [
        './js-src/main-*.js'
    ],
    scripts_bundle: [
        'node_modules/jquery/dist/jquery.js',
        'node_modules/jquery-ui/ui/data.js',
        'node_modules/jquery-ui/ui/scroll-parent.js',
        'node_modules/jquery-ui/ui/widget.js',
        'node_modules/jquery-ui/ui/widgets/datepicker.js',
        'node_modules/jquery-ui/ui/widgets/mouse.js',
        'node_modules/jquery-ui/ui/widgets/droppable.js',
        'node_modules/jquery-ui/ui/widgets/sortable.js',
        'node_modules/jquery.cookie/jquery.cookie.js',
        'node_modules/jquery.easing/jquery.easing.js',
        'node_modules/jquery-mask-plugin/dist/jquery.mask.js',
        'node_modules/jquery.alphanum/jquery.alphanum.js',
        'node_modules/moment/moment.js',
        'node_modules/moment/locale/en-gb.js',
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        'node_modules/metismenu/dist/metisMenu.js',
        'node_modules/lodash/lodash.js',
        'node_modules/bootbox/bootbox.js',
        'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
        'node_modules/jquery.localscroll/jquery.localScroll.js',
        'node_modules/blueimp-file-upload/js/jquery.fileupload.js',
        'node_modules/blueimp-file-upload/js/jquery.iframe-transport.js',
        'js-src/tagmanager.js',
    ],
    dist_js: 'js',
    dist_css: 'css',
    dist_fonts: 'fonts',
    dist_scripts_admin: 'admin.js',
    dist_scripts_main: 'main.js',
    dist_scripts_bundle: 'bundle.js',
};

gulp.task('default', ['build']);

gulp.task('build', ['sass', 'scripts', 'scripts-bundle', 'copy']);

gulp.task('build-src', ['sass', 'scripts']);

gulp.task('copy', function (done) {
    gulp.src(paths.fonts)
        .pipe(gulp.dest(paths.dist_fonts))
        .on('end', done);
});

gulp.task('sass', function (done) {
    gulp.src(paths.sass)
        .pipe(newer('.tmp/styles'))
        .pipe(sourcemaps.init())
        .pipe(sass({
          precision: 10
        }).on('error', sass.logError))
        .pipe(gulp.dest('.tmp/styles'))
        .pipe(if_('*.css', minifycss({
          keepSpecialComments: 0
        })))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.dist_css))
        .pipe(gulp.dest('.tmp/styles'))
        .on('end', done);
});

gulp.task('scripts', ['scripts-admin', 'scripts-main']);

gulp.task('scripts-admin', [], function (done) {
    gulp.src(paths.scripts_admin)
        .pipe(newer('.tmp/scripts'))
        .pipe(jshint({
          laxcomma: true,
          asi : true
        }))
        .pipe(jshint.reporter('default'))
        .pipe(sourcemaps.init())
        .pipe(uglify()).on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(concat(paths.dist_scripts_admin))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.dist_js))
        .pipe(gulp.dest('.tmp/scripts'))
        .on('end', done);
});

gulp.task('scripts-main', [], function (done) {
    gulp.src(paths.scripts_main)
        .pipe(newer('.tmp/scripts'))
        .pipe(jshint({
          laxcomma: true,
          asi : true
        }))
        .pipe(jshint.reporter('default'))
        .pipe(sourcemaps.init())
        .pipe(uglify()).on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(concat(paths.dist_scripts_main))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.dist_js))
        .pipe(gulp.dest('.tmp/scripts'))
        .on('end', done);
});

gulp.task('scripts-bundle', [], function (done) {
    gulp.src(paths.scripts_bundle)
        .pipe(expect(paths.scripts_bundle))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat(paths.dist_scripts_bundle))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(paths.dist_js))
        .on('end', done);
});

gulp.task('watch', ['build'], function () {
    gulp.watch(paths.sass.concat(paths.scripts_admin.concat(paths.scripts_main)), ['build-src']);
});
