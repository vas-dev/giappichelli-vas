<?php
  $params = array("q" => _request('q'));

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $pages = models\Page::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Page::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("pages", $pages);
  $smarty->assign("batch_url", BASE_URL . "/admin/pages?q=" . $params['q'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pages");

  $smarty->display("admin-pages.tmpl");
?>
