<?php
  $article = models\BlogArticle::get_by_id($id);
  if (!$article) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $article->delete();
    header("Location: " . BASE_URL . "/admin/blog-articles");
    exit;
  }

  $smarty->assign("article", $article);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-articles");

  $smarty->display("admin-blog-articles-delete.tmpl");
?>
