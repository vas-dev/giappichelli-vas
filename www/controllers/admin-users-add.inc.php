<?php
  $user = array();
  $errors = array();
  $status = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      "company" => _post('company'),
      "lastname" => _post('lastname'),
      "firstname" => _post('firstname'),
      "email" => _post('email'),
      "password" => _post('password'),
      "mobile" => _post('mobile'),
      "phone" => _post('phone'),
      'address' => _post('address'),
      'city' => _post('city'),
      'province' => _post('province'),
      'zipcode' => _post('zipcode'),
      'country' => _post('country'),
      'courses' => _post('courses'),
      'groups' => _post('groups'),
      'roles' => _post('roles'),
      'active' => _post('active'),
      'premium' => _post('premium'),
      'privacy' => _post('privacy'),
      'privacy2' => _post('privacy2'),
      'newsletter' => _post('newsletter'),
    );
    if (!$params['firstname']) {
      $errors['firstname'] = 1;
    }
    if (!$params['lastname']) {
      $errors['lastname'] = 1;
    }
    if (!$params['roles']) {
      $errors['roles'] = 1;
    }
    if (!$params['privacy']) {
      $errors['privacy'] = 1;
    }
    if (!$params['email']) {
      $errors['email'] = 1;
    } else if (!_email($params['email'])) {
      $errors['email'] = 2;
    } else {
      $parms['email'] = strtolower($params['email']);
      if ($u = models\User::get_by_email($params['email'])) {
        $errors['email'] = 3;
      }
    }
    if (!$params['password']) {
      $errors['password'] = 1;
    }
    if (!$errors) {
      $params['dates'] = $params['dates'] ? array_values($params['dates']) : array();
      $user_id = models\User::add($params);
      header("Location: " . BASE_URL . "/admin/users/" . $user_id . "?s=add");
      exit;
    }
  }

  $smarty->assign("user", $user);
  $smarty->assign("status", $status);
  $smarty->assign("errors", $errors);
  $smarty->assign("courses", models\Course::get_vocabulary());
  $smarty->assign("groups", models\Group::get_vocabulary());

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "users");

  $smarty->display("admin-users-add.tmpl");
?>
