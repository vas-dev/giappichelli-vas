<?php
  $user = models\User::get_by_id($user_id);
  if (!$user) {
    not_found();
  }

  $status = null;
  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    $smarty->assign("dates", _post('dates'));
    $smarty->display("calendar.tmpl");
    exit;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      "company" => _post('company'),
      "lastname" => _post('lastname'),
      "firstname" => _post('firstname'),
      "email" => _post('email'),
      "password" => _post('password'),
      "mobile" => _post('mobile'),
      "phone" => _post('phone'),
      'address' => _post('address'),
      'city' => _post('city'),
      'province' => _post('province'),
      'zipcode' => _post('zipcode'),
      'country' => _post('country'),
      'courses' => _post('courses'),
      'groups' => _post('groups'),
      'roles' => _post('roles'),
      'active' => _post('active'),
      'premium' => _post('premium'),
      'privacy' => _post('privacy'),
      'privacy2' => _post('privacy2'),
      'newsletter' => _post('newsletter'),
      'days_1' => _post('days_1'),
      'dates' => _post('dates'),
      'max_hours' => _post('max_hours'),
    );
    $params['dates'] = $params['dates'] ? array_values($params['dates']) : array();
    if (!$params['firstname']) {
      $errors['firstname'] = 1;
    }
    if (!$params['lastname']) {
      $errors['lastname'] = 1;
    }
    if (!$params['email']) {
      $errors['email'] = 1;
    }
    if (!$errors) {
      $user->update_admin($params);
      $user->set_max_hours($params['max_hours']);
      $status = 1;
    }
  }

  $max_hours = $user->get_max_hours();

  $smarty->assign("user", $user);
  $smarty->assign("status", $status);
  $smarty->assign("errors", $errors);
  $smarty->assign("courses", models\Course::get_vocabulary());
  $smarty->assign("groups", models\Group::get_vocabulary());
  $smarty->assign("max_hours", $max_hours);

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "users");

  $smarty->display("admin-users-edit.tmpl");
?>
