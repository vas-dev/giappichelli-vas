<?php
  $category = array();

  $error = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'category_id' => _post('category_id'),
      'title' => _post('title'),
      'color' => _post('color'),
    );
    if (!$params['title']) {
      $error = 1;
    } else if (!$params['color']) {
      $error = 1;
    } else {
      models\BlogCategory::add($params);
      header("Location: " . BASE_URL . "/admin/blog-categories");
      exit;
    }
  }

  $smarty->assign("error", $error);
  $smarty->assign("category", $category);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-categories");

  $smarty->display("admin-blog-categories-add.tmpl");
?>
