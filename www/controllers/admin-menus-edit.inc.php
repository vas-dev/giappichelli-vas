<?php
  $menu = models\Menu::get_by_id($menu_id);
  if (!$menu) {
    not_found();
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'lang' => _post('lang'),
      'slug' => _post('slug'),
      'content' => _post('content'),
      'published' => _post('published'),
      'published_datetime' => _post_datetime('published_datetime'),
      'published_datetime2' => _post_datetime('published_datetime2'),
    );
    if (!$params['slug']) {
      $errors['slug'] = 1;
    }
    if (!$errors) {
      $menu->update($params);
      header("Location: " . BASE_URL . "/admin/menus");
      exit;
    }
  }

  $smarty->assign("obj", $menu);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "menus");

  $smarty->display("admin-menus-edit.tmpl");
?>
