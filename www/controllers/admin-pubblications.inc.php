<?php
  $params = array(
    "q" => _request('q'),
    "published" => _request('published'),
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $pubblications = models\Pubblication::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Pubblication::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("pubblications", $pubblications);
  $smarty->assign("batch_url", BASE_URL . "/admin/pubblications?" . http_build_query(array_filter(array(
    'q' => $params['q'],
  ))). "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pubblications");

  $smarty->display("admin-pubblications.tmpl");
?>
