<?php
  $user = models\User::get_by_id($id);
  if (!$user || $user['uniqid'] != $hash) {
    not_found();
  }

  $user->update_active(1);
  $user->remember();

  $smarty->assign("principal", $user);

  if (_request('ebook')) {
    header("Location: " . BASE_URL . "/profilo/ebook");
    exit;
  }

  $smarty->display("registrazione-conferma.tmpl");
?>

