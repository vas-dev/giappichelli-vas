<?php
  if (!$principal || !in_array(2, $principal['roles'])) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'course_id' => $course['id'],
      'name' => _post('name'),
      'description' => _post('description'),
      'published' => 1,
      'published_datetime' => _post_date('published_datetime'),
      'published_datetime2' => _post_date('published_datetime2'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$params['published_datetime']) {
      $errors['published_datetime'] = 1;
    }
    if (!$params['published_datetime2']) {
      $errors['published_datetime2'] = 1;
    }
    if (!$errors) {
      $homework->update($params);
      foreach ($homework->get_files() as $file) {
        $remove = _post('files_' . $file['id'] . "_remove");
        $upload = _file('files_' . $file['id'], 'users');
        if ($upload && _a($upload, 'asset')) {
          $file->update_reply(array(
            "reply_name" => $upload['name'],
            "reply_size" => $upload['size'],
            "reply_type" => $upload['ext'],
            "reply_asset" => $upload['asset'],
          ));
        } else if ($remove) {
          $file->update_reply(array(
            "reply_name" => null,
            "reply_size" => null,
            "reply_type" => null,
            "reply_asset" => null,
          ));
        }
      }
      header("Location: " . BASE_URL . "/profilo/corsi/" . $course['slug'] . "/compiti");
      exit;
    }
  }

  $files = $homework->get_files();
  $smarty->assign("files", $files);

  $smarty->assign("course", $course);
  $smarty->assign("homework", $homework);
  $smarty->assign("errors", $errors);

  $smarty->assign("submenu", "profilo/compiti");

  $smarty->display("profilo-corsi-compiti-modifica.tmpl");
