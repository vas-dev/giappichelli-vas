<?php
  if ($principal) {
    $principal->forget();
  }

  header("Location: " . BASE_URL . "/admin");
  exit;
?>
