<?php
  if (!$principal || !in_array(2, $principal['roles'])) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }


  if (_request('id')) {
    $lesson->set_question_status(_request('id'), _request('set_status'));
  }

  $smarty->assign("course", $course);
  $smarty->assign("lesson", $lesson);
  $smarty->assign("questions", $lesson->get_questions(array("status" => _request('status'))));

  $smarty->assign("submenu", "profilo/corsi");

  $smarty->display("profilo-corsi-domande.tmpl");
