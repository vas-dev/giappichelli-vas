<?php
  if (!$principal) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $lesson = models\Lesson::get_by_id($id);
  if (!$lesson || !array_intersect($lesson['courses'], $principal->courses())) {
    header("Location: " . BASE_URL . "/profilo");
    exit;
  }

  $validminutes = 360;
  $key = "q3KrF4yQV";
  $today = gmdate("n/j/Y g:i:s A");
  $str2hash = $_SERVER['REMOTE_ADDR'] . $key . $today . $validminutes;
  $base64hash = base64_encode(md5($str2hash, true));
  $urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
  $base64urlsignature = base64_encode($urlsignature);

  $smarty->assign("signature", $base64urlsignature);
  $smarty->assign("lesson", $lesson);

  $smarty->display("profilo-audio.tmpl");

