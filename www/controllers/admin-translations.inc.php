<?php
  $langs = array(
    "en" => "English",
    "it" => "Italian",
    "de" => "Deutsch",
  );
    
  $filters = array(
    0 => _('All the strings'),
    1 => _('Only the strings not translated'),
  );
    
  $lang = _request('lang', $lang);
  $lang = isset($langs[$lang]) ? $lang : null;
        
  $params = array(
    "q" => _request('q'),
    "lang" => $lang,
    "filter" => _request('filter'),
    "reference" => _request('reference'),
    "count" => 0,
    "batch" => 25,
  );      
        
  $files = array(
    "en" => "locales/en_GB.UTF8/LC_MESSAGES/messages.po",
    "it" => "locales/it_IT.UTF8/LC_MESSAGES/messages.po",
    "de" => "locales/de_DE.UTF8/LC_MESSAGES/messages.po",
  );
  
  if ($lang && file_exists($files[$lang])) {
    $translations = Gettext\Translations::fromPoFile($files[$lang]);
    if (_request('save') && isset($_REQUEST['t'])) {
      foreach ($_REQUEST['t'] as $k => $v) {
        $t = $translations->find(null, $v['original']);
        if ($t) {
          $t->setTranslation($v['translation']);
        } else {
          $translation = new Gettext\Translation(null, $v['original']);
          $translation->setTranslation($v['translation']);
          $translations[] = $translation;
        }
      }
      $rev = file_exists(preg_replace('/.po$/', '.rev', $files[$lang])) ? file_get_contents(preg_replace('/.po$/', '.rev', $files[$lang])) : 0;
      $rev++;
      Gettext\Generators\Po::toFile($translations, $files[$lang]);
      Gettext\Generators\Mo::toFile($translations, preg_replace('/.po$/', '_r' . $rev . '.mo', $files[$lang]));
      file_put_contents(preg_replace('/.po$/', '.rev', $files[$lang]), $rev);
    }
  
    $references = array();
    $translations = array_values(array_filter($translations->getArrayCopy(), function ($j) use ($params, &$references) {
      if ($params['q'] && strstr(mb_strtolower($j->getOriginal()), mb_strtolower($params['q'])) === FALSE &&
          $params['q'] && strstr(mb_strtolower($j->getTranslation()), mb_strtolower($params['q'])) === FALSE) {
        return false;
      }
      if ($params['filter'] && $params['filter'] == 1 && $j->getTranslation()) {
        return false;
      }
      $found = false;
      $not_admin = false;
      foreach ($j->getReferences() as $ref) {
        $ref = $ref[0];
        $ref = preg_replace('/^.*\//', '', $ref);
        $ref = preg_replace('/\..*/', '', $ref);
        if ($params['reference'] && $params['reference'] == $ref) {
          $found = true;
        }
        if (!preg_match('/^admin/', $ref)) {
          $not_admin = true;
          if (!in_array($ref, $references)) {
            $references[$ref] = $ref;
          }
        }
      }
      if ($params['reference'] && !$found || !$not_admin) {
        return false;
      }
      return true;
    }));

    $params['offset'] = _request('offset')*1;
    $params['count'] = count($translations);
    $translations = array_slice($translations, $params['offset'], $params['offset'] + $params['batch']);
  } else {
    $translations = array();
    $references = array();
  }

  $smarty->assign("lang", $lang);
  $smarty->assign("langs", $langs);
  $smarty->assign("params", $params);
  $smarty->assign("filters", $filters);
  $smarty->assign("references", $references);
  $smarty->assign("translations", $translations);

  $smarty->assign("batch_url", BASE_URL . "/admin/translations?" . http_build_query($params) . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "translations");

  $smarty->display("admin-translations.tmpl");
