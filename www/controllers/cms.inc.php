<?php
  if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && _request('cookie')) {
    setcookie("pcookie", 1, time()+86400*365, "/", COOKIE_DOMAIN);
    exit;
  }

  $banners = models\Banner::get(array("published" => 1, "dates" => 1, "category" => 1, "slug" => $page['slug']), 0, 10);
  $smarty->assign("banners", $banners);

  $courses = array();
  $articles = array();
  $articles_2 = array();
  $articles_3 = array();
  $teachers = array();
  foreach ($page->content() as $content) {
    foreach ($content->config() as $cfg) {
      if ($cfg['type'] == 'news') {
        $articles = models\BlogArticle::get(array("published" => 1, "homepage" => 1, "c_category_id" => 1), 0, 4);
      } else if ($cfg['type'] == 'in-evidenza') {
        $articles_2 = models\BlogArticle::get(array("published" => 1, "homepage" => 1, "c_category_id" => 2), 0, 50);
      } else if ($cfg['type'] == 'dicono-di-noi') {
        $articles_3 = models\BlogArticle::get(array("published" => 1, "homepage" => 1, "c_category_id" => 3), 0, 4);
      } else if ($cfg['type'] == 'corsi') {
        $courses = models\Course::get(array("published" => 1), 0, 4);
      } else if ($cfg['type'] == 'docenti') {
        $teachers = models\Teacher::get(array("published" => 1), 0, 4);
      }
    }
  }

  function _day($i, $prms=array()) {
    global $params;
    if (!$prms) {
      $prms = $params;
    }
    return date('N', strtotime(sprintf('%04d-%02d-%02d', $prms['year'], $prms['month'], $i)));
  }

  $smarty->registerPlugin('function', '_day', '_day');

  $calendar = array(
    "year" => _request('year', date('Y')),
    "month" => _request('month', date('m')),
  );
  $calendar['lessons'] = models\Lesson::get(array(
    "year" => $calendar['year'],
    "month" => $calendar['month']
  ), 0, 120);
  $calendar['month_next'] = $calendar['month'] == 12 ? 1 : $calendar['month'] + 1;
  $calendar['year_next'] = $calendar['month'] == 12 ? $calendar['year'] + 1 : $calendar['year'];
  $calendar['month_prev'] = $calendar['month'] == 1 ? 12 : $calendar['month'] - 1;
  $calendar['year_prev'] = $calendar['month'] == 1 ? $calendar['year'] - 1 : $calendar['year'];

  $smarty->assign("page", $page);
  $smarty->assign("courses", $courses);
  $smarty->assign("teachers", $teachers);
  $smarty->assign("articles", $articles);
  $smarty->assign("articles_2", $articles_2);
  $smarty->assign("articles_3", $articles_3);
  $smarty->assign("calendar", $calendar);

  $landing = false;
  foreach ($page->content() as $content) {
    if ($content['type'] == 31) {
      $landing = true;
    }
  }

  if ($landing) {
    require('cms-landing.inc.php');
  }

  if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && _request('calendar')) {
    $smarty->display("cms-calendario.tmpl");
  } else {
    $smarty->display("cms.tmpl");
  }
