<?php
  $teacher = models\Teacher::get_by_id($teacher_id);
  if (!$teacher) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $teacher->delete();
    header("Location: " . BASE_URL . "/admin/teachers");
    exit;
  }

  $smarty->assign("teacher", $teacher);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "teachers");

  $smarty->display("admin-teachers-delete.tmpl");
?>
