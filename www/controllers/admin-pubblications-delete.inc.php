<?php
  $pubblication = models\Pubblication::get_by_id($pubblication_id);
  if (!$pubblication) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $pubblication->delete();
    header("Location: " . BASE_URL . "/admin/pubblications");
    exit;
  }

  $smarty->assign("pubblication", $pubblication);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pubblications");

  $smarty->display("admin-pubblications-delete.tmpl");
?>
