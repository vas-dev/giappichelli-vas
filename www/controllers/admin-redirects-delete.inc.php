<?php
  $redirect = models\Redirect::get_by_id($id);
  if (!$redirect) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $redirect->delete();
    header("Location: " . BASE_URL . "/admin/redirects");
    exit;
  }

  $smarty->assign("redirect", $redirect);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "redirects");

  $smarty->display("admin-redirects-delete.tmpl");
?>
