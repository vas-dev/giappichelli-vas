<?php
  $course = models\Course::get_by_id($course_id);
  if (!$course) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $course->delete();
    header("Location: " . BASE_URL . "/admin/courses");
    exit;
  }

  $smarty->assign("course", $course);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "courses");

  $smarty->display("admin-courses-delete.tmpl");
?>
