<?php
  $upload = _file('upload', 'editor');
  if ($upload) {
    $uploaded = 1;
    $url = BASE_URL . '/assets/' . $upload['asset'];
  } else {
    $uploaded = 0;
    $url = null;
  }

  echo json_encode([
    "uploaded" => $uploaded,
    "fileName" => basename($url),
    "url" => $url,
  ]);
?>
