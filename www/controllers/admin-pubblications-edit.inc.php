<?php
  $pubblication = models\Pubblication::get_by_id_and_lang($pubblication_id, $lang);
  if (!$pubblication) {
    not_found();
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    $smarty->assign("dates", _post('dates'));
    $smarty->display("calendar.tmpl");
    exit;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'name' => _post('name'),
      'description' => _post('description'),
      'image' => _file('image', 'pubblications'),
      'url' => _post('url'),
      'teachers' => _post('teachers'),
      'courses' => _post('courses'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$errors) {
      $pubblication->update($params);
      header("Location: " . BASE_URL . "/admin/pubblications");
      exit;
    }
  }

  $teachers = models\Teacher::get_vocabulary();
  $smarty->assign("teachers", $teachers);

  $courses = models\Course::get_vocabulary();
  $smarty->assign("courses", $courses);

  $smarty->assign("pubblication", $pubblication);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pubblications");

  $smarty->display("admin-pubblications-edit.tmpl");
?>