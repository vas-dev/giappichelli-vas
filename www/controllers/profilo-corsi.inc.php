<?php

if (!$principal) {
  header('Location: '.BASE_URL.'/login');
  exit;
} elseif (!_in_array($course['id'], $principal->courses()) || !$course->is_visible_for_user($principal)) {
  header('Location: '.BASE_URL.'/profilo');
  exit;
}

$status = _request('status', 0);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $params = array(
    "homework_id" => _post('homework_id'),
    "file" => _file('file', 'users'),
  );
  if ($params['file']) {
    $principal->add_file(array(
      "homework_id" => $params['homework_id'],
      "name" => $params['file']['name'],
      "size" => $params['file']['size'],
      "type" => $params['file']['ext'],
      "asset" => $params['file']['asset'],
    ));
    header("Location: " . BASE_URL . "/profilo?status=1");
    exit;
  } else {
    $status = 2;
  }
}

$next_lesson_live = $course->is_next_lesson_live();
if ($next_lesson_live) {
  header("Location: " . BASE_URL . "/profilo/corsi/" . $course['slug'] . "/" . $next_lesson_live['id']);
  exit;
}

$lessons = models\Lesson::get([
  'course_id' => $course['id'],
  "published" => 1,
  'type:not' => 99,
  'order_by' => 'COALESCE(l.lesson_date, \'2099-12-31\') ASC',
], 0, 100);

$homeworks = models\Homework::get(array("published" => 1));

$materials = [];
$materials_course = [
  "course" => $course,
  "lessons" => [],
];

foreach ($lessons as $lesson) {
  if ($lesson['materials'] && ($lesson['files'] || $lesson['widget_audio'])) {
    $materials_course['lessons'][] = $lesson;
  }
}

if ($materials_course['lessons']) {
  $materials[] = $materials_course;
}

$smarty->assign('course', $course);
$smarty->assign('lessons', $lessons);
$smarty->assign("homeworks", $homeworks);
$smarty->assign("materials", $materials);
$smarty->assign("status", $status);

$smarty->assign('submenu', 'profilo/corsi');

$smarty->display('profilo-corsi.tmpl');
