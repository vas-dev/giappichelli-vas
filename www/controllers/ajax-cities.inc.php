<?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = array("status" => 1);
    $rs = mysqli_query($db, "SELECT id, lat, lng ".
                            "FROM cities ".
                            "WHERE province = " . _text(_request('province')) . " AND name = " . _text(_request('city')) . ";") or die(mysqli_error($db));
    $row = mysqli_fetch_assoc($rs);
    if ($row && !$row['lat'] && !$row['lng']) {
      mysqli_query($db, "UPDATE cities SET lat = " . _text(_request('lat')) . ", lng = " . _text(_request('lng')) . " ".
                        "WHERE id = " . _integer($row['id']) . ";") or die(mysqli_error($db));
    }
  }
  else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (_request('q')) {
      $result = array();
      $rs = mysqli_query($db, "SELECT code, name, province FROM cities WHERE lower(name) LIKE " . _text('%' . mb_strtolower(_request('q')) . '%') . " ORDER BY name;") or die(mysqli_error($db));
      while ($row = mysqli_fetch_assoc($rs)) {
        $result[] = $row['name'] . " (" . $row['province'] . ")";
      }
    } else {
      $result = array("cities" => array());
      $rs = mysqli_query($db, "SELECT code, name, main FROM cities WHERE province = " . _text(_request('province')) . " ORDER BY name;") or die(mysqli_error($db));
      while ($row = mysqli_fetch_assoc($rs)) {
        $result['cities'][] = array_filter(array("id" => $row['code'], "name" => $row['name'], "main" => (int)$row['main']));
      }
    }
  }

  header("Content-Type: text/json; charset=UTF-8");
  echo json_encode($result);
