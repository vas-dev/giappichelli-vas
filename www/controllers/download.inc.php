<?php
  if (!file_exists(DIR_ASSETS . "/" . $path)) {
    not_found();
  }

  $content_type = mime_content_type(DIR_ASSETS . "/" . $path);

  $filename = _get('fn');
  header("Content-Type: " . $content_type);
  if ($filename) {
    header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
  }

  echo file_get_contents(DIR_ASSETS . "/" . $path);
