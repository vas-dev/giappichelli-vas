<?php
  $error = null;

  if ($principal) {
    header("Location: " . BASE_URL . "/admin/courses");
    exit;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = _post('email');
    $password = _post('password');
    $user = models\UserBE::get_by_email($email);
    if ($user && $user['password'] == md5($password)) {
      $user->remember();
      header("Location: " . BASE_URL . "/admin/courses");
      exit;
    } else {
      $error = true;
    }
  }

  $smarty->assign("error", $error);
  $smarty->display("admin-login.tmpl");
?>
