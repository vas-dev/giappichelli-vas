<?php
  $translations = array();

  header("Content-Type: text/javascript; charset=\"UTF8\"");
  echo "window.Giappichelli = window.Giappichelli || {};\n";
  echo "window.Giappichelli.translations = " . json_encode($translations) . ";";
