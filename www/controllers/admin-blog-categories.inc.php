<?php
  $params = array("q" => _request('q'));

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $categories = models\BlogCategory::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\BlogCategory::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("categories", $categories);
  $smarty->assign("batch_url", BASE_URL . "/admin/blog-categories?q=" . $params['q'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-categories");

  $smarty->display("admin-blog-categories.tmpl");
?>
