<?php
  $status = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      "compan" => _post('compan'),
      "lastname" => _post('lastname'),
      "firstname" => _post('firstname'),
      "email" => _post('email'),
      "password" => _post('password'),
      "role" => _post('role'),
      "roles" => _post('roles'),
      "courses" => _post('courses'),
    );
    models\UserBE::add($params);
    header("Location: " . BASE_URL . "/admin/users-be?add=1");
    exit;
  }

  $smarty->assign("user", array());
  $smarty->assign("status", $status);
  $smarty->assign("courses", models\Course::get_vocabulary());

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "users-be");

  $smarty->display("admin-users-be-add.tmpl");
?>
