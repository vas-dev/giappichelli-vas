<?php
  if (!$principal) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $status = _request('status', 0);
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      "homework_id" => _post('homework_id'),
      "file" => _file('file', 'users'),
    );
    if ($params['file']) {
      $principal->add_file(array(
        "homework_id" => $params['homework_id'],
        "name" => $params['file']['name'],
        "size" => $params['file']['size'],
        "type" => $params['file']['ext'],
        "asset" => $params['file']['asset'],
      ));
      header("Location: " . BASE_URL . "/profilo?status=1");
      exit;
    } else {
      $status = 2;
    }
  }

  $courses = models\Course::get(array("published" => 1, "ids" => $principal->courses()), 0, 10);
  $homeworks = models\Homework::get(array("published" => 1));

  $materials = [];
  foreach ($courses as $course) {
    $lessons = $course->get_lessons([
      "published" => 1,
      "lesson_date:lte" => date('Y-m-d'),
    ]);
    if ($lessons) {
      $materials_course = [
        "course" => $course,
        "lessons" => [],
      ];
      foreach ($lessons as $lesson) {
        if ($lesson['materials'] && ($lesson['files'] || $lesson['widget_audio'])) {
          $materials_course['lessons'][] = $lesson;
        }
      }
      if ($materials_course['lessons']) {
        $materials[] = $materials_course;
      }
    }
  }

  $smarty->assign("courses", $courses);
  $smarty->assign("homeworks", $homeworks);
  $smarty->assign("materials", $materials);

  $smarty->assign("status", $status);
  $smarty->assign("submenu", "profilo");

  $smarty->display("profilo.tmpl");
