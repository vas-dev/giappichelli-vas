<?php
  $remove = _get('remove');
  if ($remove) {
    $user = models\User::get_by_id($remove);
    if ($user) {
      $user->remove();
    } else {
      $delete = null;
    }
  }

  $delete = _get('delete');
  if ($delete) {
    $user = models\User::get_by_id($delete);
    if ($user) {
      $user->delete();
    } else {
      $delete = null;
    }
  }

  $undelete = _get('undelete');
  if ($undelete) {
    $user = models\User::get_by_id($undelete);
    if ($user) {
      $user->undelete();
    } else {
      $delete = null;
    }
  }

  $params = array(
    'q' => _request('q'),
    'courses' => _request('course') ? [_request('course')] : [],
    'groups' => _request('group') ? [_request('group')] : [],
    'roles' => _request('roles'),
    'sort' => _request('sort'),
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $users = models\User::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\User::get_count($params);

  $smarty->assign("delete", $delete);
  $smarty->assign("users", $users);
  $smarty->assign("params", $params);
  $smarty->assign("batch_url", BASE_URL . "/admin/users?" . http_build_query(array_filter(array(
    'q' => $params['q'],
    'course' => $params['courses'],
    'group' => $params['groups'],
    'roles' => $params['roles'],
    'sort' => $params['sort'],
  ))). "&");
  $smarty->assign("batch_url_no_sort", BASE_URL . "/admin/users?" . http_build_query(array_filter(array(
    'q' => $params['q'],
    'course' => $params['courses'],
    'group' => $params['groups'],
    'roles' => $params['roles'],
  ))). "&");

  $smarty->assign("courses", models\Course::get_vocabulary());
  $smarty->assign("groups", models\Group::get_vocabulary());

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "users");

  $smarty->display("admin-users.tmpl");
?>
