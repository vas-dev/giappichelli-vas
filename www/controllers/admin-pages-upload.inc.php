<?php
  $files = _files('files', 'pages');
  if ($files) {
    header("Content-Type: text/json; charset=UTF-8");
    $data = array();
    foreach ($files as $file) {
      array_push($data, array(
        "asset" => $file['asset'],
        "size" => filesize($file['path']),
        "name" => $file['name'],
        "md5" => $file['md5'],
      ));
    }
    echo json_encode($data);
  } else {
    header('HTTP/1.0 400 Bad Request', true, 400);
  }
