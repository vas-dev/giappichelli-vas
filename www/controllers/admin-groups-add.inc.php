<?php
  $group = array();
  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'name' => _post('name'),
      'courses' => _post('courses'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$errors) {
      models\Group::add($params);
      header("Location: " . BASE_URL . "/admin/groups");
      exit;
    }
  }

  $smarty->assign("courses", models\Course::get_vocabulary());

  $smarty->assign("group", $group);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "groups");

  $smarty->display("admin-groups-add.tmpl");
?>
