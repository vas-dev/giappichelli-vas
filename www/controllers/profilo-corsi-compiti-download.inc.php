<?php
  if (!$principal || !in_array(2, $principal['roles'])) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $files = $homework->get_files();
  if (!$files) {
    header("Location: " . BASE_URL . "/profilo/corsi/" . $course['slug'] . "/compiti");
    exit;
  }

  $filename = "compiti-" . uniqid() . ".zip";

  $zip = new ZipArchive();
  if ($zip->open(DIR_ASSETS . DIRECTORY_SEPARATOR . $filename, ZipArchive::CREATE)!==TRUE) {
    exit("cannot open <$filename>\n");
  }

  foreach ($files as $file) {
    $zip->addFile(DIR_ASSETS . DIRECTORY_SEPARATOR . $file['asset'], "/" . $file['name']);
  }

  $zip->close();

  header("Location: " . BASE_URL . "/assets/" . $filename);
  exit;
