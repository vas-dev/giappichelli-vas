<?php
  $confirm = false;
  $errors = array();
  $user = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'firstname' => _post('firstname'),
      'lastname' => _post('lastname'),
      'city' => _post('city'),
      'email' => _post('email'),
      'phone' => _post('phone'),
      'privacy' => _post('privacy'),
      'privacy2' => _post('privacy2'),
      'newsletter' => _post('newsletter'),
    );
    if (!$params['firstname']) {
      $errors['firstname'] = 1;
    }
    if (!$params['lastname']) {
      $errors['lastname'] = 1;
    }
    if (!$params['email']) {
      $errors['email'] = 1;
    }
    if (!$params['city']) {
      $errors['city'] = 1;
    }
    if (!$params['phone']) {
      $errors['phone'] = 1;
    }
    if (!$params['privacy']) {
      $errors['privacy'] = 1;
    }
    if (!$params['email']) {
      $errors['email'] = 1;
    } else if (!_email($params['email'])) {
      $errors['email'] = 2;
    }
    if (!$errors) {
      $confirm = 1;

      $mail = new \PHPMailer();
      $mail->CharSet = 'utf-8';
      $mail->Encoding = '8bit';
      $mail->addReplyTo(EMAIL_ADDRESS);
      $mail->SetFrom(EMAIL_ADDRESS, EMAIL_NAME);
      foreach (explode(",", EMAIL_NOTIFICATIONS) as $email) {
        $mail->AddAddress($email);
      }
      $mail->Subject = "Nuova compilazione form LANDING";
      $mail->Body = "compilazione form Landing GIAPPICHELLI CORSI:\n\n";
      $keys = array(
        'firstname' => 'Nome',
        'lastname' => 'Cognome',
        'email' => 'Email',
        'city' => 'Città',
        'phone' => 'Numero di telefono',
        'privacy' => 'Privacy 1',
        'privacy2' => 'Privacy 2',
        'privacy2' => 'Privacy 2',
        'newsletter' => 'Newsletter',
      );

      foreach ($params as $k => $v) {
        $k = _v($k, $keys);
        $mail->Body .= "$k = $v\n";
      }
      $mail->Send();
    }
  }

  $validminutes = 360;
  $key = "q3KrF4yQV";
  $today = gmdate("n/j/Y g:i:s A");
  $str2hash = $_SERVER['REMOTE_ADDR'] . $key . $today . $validminutes;
  $base64hash = base64_encode(md5($str2hash, true));
  $urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
  $base64urlsignature = base64_encode($urlsignature);

  $smarty->assign("signature", $base64urlsignature);

  $smarty->assign("user", $user);
  $smarty->assign("errors", $errors);
  $smarty->assign("confirm", $confirm);
