<?php
  $group = models\Group::get_by_id($group_id);
  if (!$group) {
    not_found();
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'name' => _post('name'),
      'courses' => _post('courses'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$errors) {
      $group->update($params);
      header("Location: " . BASE_URL . "/admin/groups");
      exit;
    }
  }

  $smarty->assign("courses", models\Course::get_vocabulary());

  $smarty->assign("group", $group);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "groups");

  $smarty->display("admin-groups-edit.tmpl");
?>
