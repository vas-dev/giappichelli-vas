<?php
  $article = models\BlogArticle::get_by_id($id);
  if (!$article) {
    not_found();
  }

  $error = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'title' => _post('title'),
      'slug' => _post('slug'),
      'cover' => _image('cover', 'blog'),
      'author' => _post('author'),
      'description' => _post('description'),
      'body' => _post('body'),
      'date' => _post_date('date'),
      'category_id' => _post('category_id'),
      'category2_id' => _post('category2_id'),
      'published' => _post('published'),
      'published_datetime' => _post_datetime('published_datetime'),
      'requires_login' => _post('requires_login'),
      'seo_title' => _post('seo_title'),
      'seo_description' => _post('seo_description'),
      'seo_keyword' => _post('seo_keyword'),
      'seo_keywords' => _post('seo_keywords'),
      'seo_trackback' => _post('seo_trackback'),
      'homepage' => _post('homepage'),
      'focus' => _post('focus'),
      'benessere' => _post('benessere'),
      'tags' => _post_tags('tags'),
      'images' => _post_fileupload('images'),
      'files' => _post_fileupload('files'),
    );
    if (!$params['title'] || !$params['date'] || !$params['category_id']) {
      $error = 1;
    } else {
      $article->update($params);
      header("Location: " . BASE_URL . "/admin/blog-articles");
      exit;
    }
  }

  $categories = models\BlogCategory::get_vocabulary();
  $smarty->assign("categories", $categories);

  $smarty->assign("error", $error);
  $smarty->assign("article", $article);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-articles");

  $smarty->display("admin-blog-articles-edit.tmpl");
?>
