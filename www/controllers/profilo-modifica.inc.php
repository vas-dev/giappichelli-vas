<?php
  if (!$principal) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $confirm = false;
  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'firstname' => _post('firstname'),
      'lastname' => _post('lastname'),
      'address' => _post('address'),
      'city' => _post('city'),
      'zipcode' => _post('zipcode'),
      'province' => _post('province'),
      'country' => _post('country'),
      'mobile' => _post('mobile'),
      'phone' => _post('phone'),
      'email' => _post('email'),
      'password' => _post('password'),
      'password2' => _post('password2'),
      'password3' => _post('password3'),
      'roles' => $principal['roles'],
      'privacy' => _post('privacy'),
      'privacy2' => _post('privacy2'),
      'newsletter' => _post('newsletter'),
    );
    if (!$params['firstname']) {
      $errors['firstname'] = 1;
    }
    if (!$params['lastname']) {
      $errors['lastname'] = 1;
    }
    if (!$params['city']) {
      $errors['city'] = 1;
    }
    if (!$params['province']) {
      $errors['province'] = 1;
    }
    if (!$params['country']) {
      $errors['country'] = 1;
    }
    if (!$params['roles']) {
      $errors['roles'] = 1;
    }
    if (!$params['privacy']) {
      $errors['privacy'] = 1;
    }
    if (!$params['email']) {
      $errors['email'] = 1;
    } else if (!_email($params['email'])) {
      $errors['email'] = 2;
    } else {
      $parms['email'] = strtolower($params['email']);
      if (($u = models\User::get_by_email($params['email'])) && $u['id'] != $principal['id']) {
        $errors['email'] = 3;
      }
    }
    if (!$params['password']) {
      //
    } else if (!$params['password3']) {
      $errors['password3'] = 1;
    } else if (hash('sha256', $params['password3']) != $principal['password']) {
      $errors['password3'] = 2;
    } else if (!models\User::password_policy($params, $params['password'])) {
      $errors['password'] = 2;
    } else if ($params['password'] != $params['password2']) {
      $errors['password2'] = 1;
    }
    if (!$errors) {
      $principal->update($params);
      $confirm = 1;
    }
  }

  $smarty->assign("errors", $errors);
  $smarty->assign("confirm", $confirm);
  $smarty->assign("submenu", "profilo/modifica");

  $smarty->display("profilo-modifica.tmpl");
?>
