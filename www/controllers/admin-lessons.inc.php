<?php
  $params = array(
    "q" => _request('q'),
    "course_id" => _request('course'),
    "courses" => $principal['courses'],
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $lessons = models\Lesson::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Lesson::get_count($params);

  $smarty->assign("courses", models\Course::get_vocabulary());

  $smarty->assign("params", $params);
  $smarty->assign("lessons", $lessons);
  $smarty->assign("batch_url", BASE_URL . "/admin/lessons?q=" . $params['q'] . "&course=" . $params['course_id'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "lessons");

  $smarty->display("admin-lessons.tmpl");
?>
