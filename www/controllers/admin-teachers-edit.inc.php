<?php
  $teacher = models\Teacher::get_by_id_and_lang($teacher_id, $lang);
  if (!$teacher) {
    not_found();
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    $smarty->assign("dates", _post('dates'));
    $smarty->display("calendar.tmpl");
    exit;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'name' => _post('name'),
      'slug' => _post('slug'),
      'intro' => _post('intro'),
      'description' => _post('description'),
      'image' => _file('image', 'teachers'),
      'pdf' => _file('pdf', 'teachers'),
      'published' => _post('published'),
      'published_datetime' => _post_datetime('published_datetime'),
      'published_datetime2' => _post_datetime('published_datetime2'),
      'sorting' => _post('sorting'),
      'seo_title' => _post('seo_title'),
      'seo_image' => _image('seo_image', 'teachers'),
      'seo_description' => _post('seo_description'),
      'seo_keywords' => _post('seo_keywords'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$params['slug']) {
      $errors['slug'] = 1;
    } else if (($p = models\Teacher::get_by_slug_all($params['slug'])) && $p['id'] != $teacher['id']) {
      $errors['slug'] = 2;
    }
    if (!$errors) {
      $teacher->update($params);
      header("Location: " . BASE_URL . "/admin/teachers");
      exit;
    }
  }

  $smarty->assign("teacher", $teacher);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "teachers");

  $smarty->display("admin-teachers-edit.tmpl");
?>
