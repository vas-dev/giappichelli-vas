<?php
  if (!$principal || !in_array(2, $principal['roles'])) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $errors = array();
  $homework = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'course_id' => $course['id'],
      'name' => _post('name'),
      'description' => _post('description'),
      'published' => 1,
      'published_datetime' => _post_date('published_datetime'),
      'published_datetime2' => _post_date('published_datetime2'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$params['published_datetime']) {
      $errors['published_datetime'] = 1;
    }
    if (!$params['published_datetime2']) {
      $errors['published_datetime2'] = 1;
    }
    if (!$errors) {
      models\Homework::add($params);
      header("Location: " . BASE_URL . "/profilo/corsi/" . $course['slug'] . "/compiti");
      exit;
    }
  }

  $smarty->assign("course", $course);
  $smarty->assign("homework", $homework);
  $smarty->assign("errors", $errors);

  $smarty->assign("submenu", "profilo/compiti");

  $smarty->display("profilo-corsi-compiti-aggiungi.tmpl");
