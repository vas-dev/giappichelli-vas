<?php
  $path = implode(DIRECTORY_SEPARATOR, array_slice($request_parts, 1));
  $source = DIR_ASSETS . DIRECTORY_SEPARATOR . $path;
  if (!file_exists($source)) {
    not_found();
  }

  $w = _request('w');
  $h = _request('h');
  $t = _request('t');

  if ($t && $w && $h) {
    $key = "-w" . $w . "h" . $h . "t";
  } else if ($w && $h) {
    $key = "-w" . $w . "h" . $h;
  } else if ($w) {
    $key = "-w" . $w;
  } else{
    $key = "";
  }

  $target = $key ? preg_replace("/^(.*)\.([a-z0-9]{3})$/", "\\1" . $key . ".\\2", $source) : $source;
  if (!file_exists($target)) {
    if ($t && $w && $h) {
      $target = _thumbnail($source, $w, $h, null, "jpg", $target);
    } else if ($w && $h) {
      $target = _resize_h($source, $w, $h, null, $target);
    } else if ($w) {
      $target = _resize($source, $w, null, $target);
    } else {
      $target = $source;
    }
  }

  function getRequestHeaders() {
    if (function_exists("apache_request_headers")) {
      if($headers = apache_request_headers()) {
        return $headers;
      }
    }
    $headers = array();
    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
      $headers['If-Modified-Since'] = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
    }
    return $headers;
  }

  $file = DIR_ASSETS . str_replace(DIR_ASSETS, "", $target);
  $file_time = filemtime($file);
  $image_mime = image_type_to_mime_type(exif_imagetype($file));

  $headers = getRequestHeaders();
  if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == $file_time)) {
    header('Last-Modified: '.gmdate('D, d M Y H:i:s', $file_time).' GMT', true, 304);
  } else {
    header('Last-Modified: '.gmdate('D, d M Y H:i:s', $file_time).' GMT', true, 200);
    header('Pragma: public');
    header('Cache-Control: public, max-age=86400');
    header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
    header('Content-Type: ' . $image_mime);
    readfile($file);
  }
