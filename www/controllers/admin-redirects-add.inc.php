<?php
  $redirect = array();

  $error = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'request_uri' => _post('request_uri'),
      'redirect_uri' => _post('redirect_uri'),
    );
    if (!$params['request_uri']) {
      $error = 1;
    } else if (!$params['redirect_uri']) {
      $error = 1;
    } else {
      models\Redirect::add($params);
      header("Location: " . BASE_URL . "/admin/redirects");
      exit;
    }
  }

  $smarty->assign("error", $error);
  $smarty->assign("redirect", $redirect);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "redirects");

  $smarty->display("admin-redirects-add.tmpl");
?>
