<?php
  $params = array(
    "q" => _request('q'),
    "published" => _request('published'),
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $teachers = models\Teacher::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Teacher::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("teachers", $teachers);
  $smarty->assign("batch_url", BASE_URL . "/admin/teachers?" . http_build_query(array_filter(array(
    'q' => $params['q'],
  ))). "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "teachers");

  $smarty->display("admin-teachers.tmpl");
?>
