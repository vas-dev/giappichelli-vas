<?php
  if (!$principal) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }
  else if (!_in_array($course['id'], $principal->courses()) || !$course->is_visible_for_user($principal)) {
    header("Location: " . BASE_URL . "/profilo");
    exit;
  }

  if (_request('view')) {
    $lesson->add_view_for_user($principal);
    exit;
  }

  $max_views = false;
  if ($lesson['max_views']) {
    $count = $lesson->get_views_for_user($principal);
    if ($count >= $lesson['max_views']) {
      $max_views = true;
    }
  }

  $smarty->assign("course", $course);
  $smarty->assign("lesson", $lesson);
  $smarty->assign("max_views", $max_views);

  $lessons = models\Lesson::get([
    'course_id' => $course['id'],
    "published" => 1,
    'type:not' => 99,
    'order_by' => 'COALESCE(l.lesson_date, \'2099-12-31\') ASC',
  ], 0, 100);

  $smarty->assign("lessons", $lessons);

  $related = $lesson->related($course['id']);
  $smarty->assign("related", $related);

  $smarty->assign("submenu", "profilo/corsi");

  if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' || _request('ajax')) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $params = array(
        "user_id" => $principal['id'],
        "subject" => _request('subject'),
        "question" => _request('question'),
      );
      $lesson->add_question($params);
    }
    $smarty->assign("questions", $lesson->get_questions());

    $smarty->display("profilo-corsi-lezione-ajax.tmpl");
  } else {
    $validminutes = 360;
    $key = "q3KrF4yQV";
    $today = gmdate("n/j/Y g:i:s A");
    $str2hash = $_SERVER['REMOTE_ADDR'] . $key . $today . $validminutes;
    $base64hash = base64_encode(md5($str2hash, true));
    $urlsignature = "server_time=" . $today ."&hash_value=" . $base64hash. "&validminutes=$validminutes";
    $base64urlsignature = base64_encode($urlsignature);

    $smarty->assign("signature", $base64urlsignature);
    $smarty->assign("questions", $lesson->get_questions());

    $smarty->display("profilo-corsi-lezione.tmpl");
  }
