<?php
  $id = _request('id');
  if ($id) {
    $comment = models\BlogArticleComment::get_by_id($id);
    if ($comment) {
      $comment->update_status(1);
    }
  }

  $id = _request('delete');
  if ($id) {
    $comment = models\BlogArticleComment::get_by_id($id);
    if ($comment) {
      $comment->delete();
    }
  }

  $params = array("q" => _request('q'), "status" => 0);

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $comments = models\BlogArticleComment::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\BlogArticleComment::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("comments", $comments);
  $smarty->assign("batch_url", BASE_URL . "/admin/blog-comments?q=" . $params['q'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-comments");

  $smarty->display("admin-blog-comments.tmpl");
?>
