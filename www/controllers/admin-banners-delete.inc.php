<?php
  $banner = models\Banner::get_by_id($id);
  if (!$banner) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $banner->delete();
    header("Location: " . BASE_URL . "/admin/banners");
    exit;
  }

  $smarty->assign("banner", $banner);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "banners");

  $smarty->display("admin-banners-delete.tmpl");
?>
