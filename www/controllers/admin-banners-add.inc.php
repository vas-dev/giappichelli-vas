<?php
  $banner = array();

  $error = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'slug' => _post('slug'),
      'title' => _post('title'),
      'image' => _image('image', 'banners'),
      'navigator' => _image('navigator', 'banners'),
      'cta' => _post('cta'),
      'link' => _post('link'),
      'category' => _post('category'),
      'sorting' => _post('sorting'),
      'published' => _post('published'),
      'date_begin' => _post_date('date_begin'),
      'date_end' => _post_date('date_end'),
    );
    if (!$params['image'] || !$params['category'] || !$params['sorting']) {
      $error = 1;
    } else {
      models\Banner::add($params);
      header("Location: " . BASE_URL . "/admin/banners");
      exit;
    }
  }

  $smarty->assign("error", $error);
  $smarty->assign("banner", $banner);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "banners");

  $smarty->display("admin-banners-add.tmpl");
?>
