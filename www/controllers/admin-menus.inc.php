<?php
  $params = array("q" => _request('q'));

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $menus = models\Menu::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Menu::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("menus", $menus);
  $smarty->assign("batch_url", BASE_URL . "/admin/menus?q=" . $params['q'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "menus");

  $smarty->display("admin-menus.tmpl");
?>
