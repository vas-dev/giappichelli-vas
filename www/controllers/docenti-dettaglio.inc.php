<?php

$courses = models\Course::get(["teacher_id" => $teacher['id']]);
$pubblications = models\Pubblication::get(["teacher_id" => $teacher['id']]);

$smarty->assign("teacher", $teacher);
$smarty->assign("courses", $courses);
$smarty->assign("pubblications", $pubblications);

$smarty->display("docenti-dettaglio.tmpl");