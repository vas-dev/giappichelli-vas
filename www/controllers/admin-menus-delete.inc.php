<?php
  $menu = models\Menu::get_by_id($menu_id);
  if (!$menu) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $menu->delete();
    header("Location: " . BASE_URL . "/admin/menus");
    exit;
  }

  $smarty->assign("menu", $menu);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "menus");

  $smarty->display("admin-menus-delete.tmpl");
?>
