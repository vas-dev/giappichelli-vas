<?php
  $params = array(
    "q" => _request('q'),
    "c_category_id" => _request('category'),
    "date1" => _request('date1'),
    "date2" => _request('date2'),
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $articles = models\BlogArticle::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\BlogArticle::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("articles", $articles);
  $smarty->assign("batch_url", BASE_URL . "/admin/blog-articles?" . http_build_query(array_filter(array(
    "q" => $params['q'],
    "date1" => $params['date1'],
    "date2" => $params['date2'],
    "category" => $params['c_category_id'],
  ))) . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-articles");

  $smarty->display("admin-blog-articles.tmpl");
?>
