<?php

$teachers = $course['teachers'] ? models\Teacher::get_by_ids($course['teachers']) : null;
$pubblications = models\Pubblication::get(["course_id" => $course['id']]);

$confirm = 0;
$errors = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $params = array(
    'firstname' => _post('firstname'),
    'lastname' => _post('lastname'),
    'message' => _post('message'),
    'email' => _post('email'),
    'phone' => _post('phone'),
    'privacy' => _post('privacy'),
    'privacy2' => _post('privacy2'),
    'newsletter' => _post('newsletter'),
  );
  if (!$params['firstname']) {
    $errors['firstname'] = 1;
  }
  if (!$params['lastname']) {
    $errors['lastname'] = 1;
  }
  if (!$params['email']) {
    $errors['email'] = 1;
  }
  if (!$params['phone']) {
    $errors['phone'] = 1;
  }
  if (!$params['privacy']) {
    $errors['privacy'] = 1;
  }
  if (!$params['message']) {
    $errors['message'] = 1;
  }
  if (!$params['email']) {
    $errors['email'] = 1;
  } else if (!_email($params['email'])) {
    $errors['email'] = 2;
  }
  if (!$errors) {
    $confirm = 1;

    $mail = new \PHPMailer();
    $mail->CharSet = 'utf-8';
    $mail->Encoding = '8bit';
    $mail->addReplyTo(EMAIL_ADDRESS);
    $mail->SetFrom(EMAIL_ADDRESS, EMAIL_NAME);
    foreach (explode(",", EMAIL_NOTIFICATIONS) as $email) {
      $mail->AddAddress($email);
    }
    $mail->Subject = "Nuova compilazione form CORSO: " . $course['name'];
    $mail->Body = "Compilazione form corso GIAPPICHELLI CORSO:\n\n";
    $keys = array(
      'firstname' => 'Nome',
      'lastname' => 'Cognome',
      'email' => 'Email',
      'phone' => 'Numero di telefono',
      'message' => 'Messaggio',
      'privacy' => 'Privacy 1',
      'privacy2' => 'Privacy 2',
      'privacy2' => 'Privacy 2',
      'newsletter' => 'Newsletter',
    );

    foreach ($params as $k => $v) {
      $k = _v($k, $keys);
      $mail->Body .= "$k = $v\n";
    }
    $mail->Send();
  }
}

$courses = $course->get_related();
$courses = $smarty->assign("courses", $courses);

$smarty->assign("confirm", $confirm);
$smarty->assign("errors", $errors);

$smarty->assign("teachers", $teachers);
$smarty->assign("course", $course);
$smarty->assign("parent_course", $course->get_parent_course());
$smarty->assign("pubblications", $pubblications);

$smarty->display("corsi-dettaglio.tmpl");