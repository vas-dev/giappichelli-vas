<?php
  $params = array(
    "q" => _request('q'),
    "published" => _request('published'),
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $groups = models\Group::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Group::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("groups", $groups);
  $smarty->assign("batch_url", BASE_URL . "/admin/groups?" . http_build_query(array_filter(array(
    'q' => $params['q'],
  ))). "&");

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "groups");

  $smarty->display("admin-groups.tmpl");
?>
