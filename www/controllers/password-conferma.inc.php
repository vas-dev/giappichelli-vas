<?php
  $user = models\User::get_by_id($id);
  if (!$user || $user['uniqid'] != $hash) {
    header("Location: " . BASE_URL . "/password");
    exit;
  }

  $user->remember();

  $confirm = false;
  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'password' => _post('password'),
      'password2' => _post('password2'),
    );
    if (!$params['password']) {
      $errors['password'] = 1;
    } else if (!models\User::password_policy($user, $params['password'])) {
      $errors['password'] = 2;
    } else if ($params['password'] != $params['password2']) {
      $errors['password2'] = 1;
    }
    if (!$errors) {
      $user->update_password($params['password']);
      if (!$user['active']) {
        $user->update_active(1);
      }
      $confirm = true;
    }
  }

  $smarty->assign("confirm", $confirm);
  $smarty->assign("errors", $errors);

  $smarty->display("password-conferma.tmpl");
?>
