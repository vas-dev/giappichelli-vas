<?php
  if ($tag) {
    $tag = models\BlogTag::get_by_slug($tag);
  }
  if ($category) {
    $category = models\BlogCategory::get_by_slug($category, 1);
  }

  $params = array(
    "year" => $year,
    "month" => $month,
    "tag" => $tag,
    "category" => $category,
    "c_category_id" => 1,
    "published" => 1,
    "batch" => 9,
    "offset" => _request('offset'),
  );

  $articles = models\BlogArticle::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\BlogArticle::get_count($params);

  $query = http_build_query(array_filter(array()));
  $smarty->assign('batch_url', BASE_URL . $request_uri . "?" . ($query ? $query . "&" : ""));

  $focus = models\BlogArticle::get(array("c_category_id" => 1, "published" => 1, "focus" => 1), 0, 2);

  $smarty->assign("params", $params);
  $smarty->assign("articles", $articles);
  $smarty->assign("focus", $focus);

  $smarty->assign("tag", $tag);
  $smarty->assign("category", $category);
  $smarty->assign("year", $year);
  $smarty->assign("month", $month);

  $archive = models\BlogArticle::get_archive(1);
  $bcategories = models\BlogCategory::get(array("category_id" => 1));
  $tags = models\BlogTag::get();

  $smarty->assign("archive", $archive);
  $smarty->assign("bcategories", $bcategories);
  $smarty->assign("tags", $tags);

  $smarty->assign("menu", "news");

  $smarty->display("news.tmpl");
?>
