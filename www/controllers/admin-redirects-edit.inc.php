<?php
  $redirect = models\Redirect::get_by_id($id);
  if (!$redirect) {
    not_found();
  }

  $error = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'request_uri' => _post('request_uri'),
      'redirect_uri' => _post('redirect_uri'),
    );
    if (!$params['request_uri']) {
      $error = 1;
    } else if (!$params['redirect_uri']) {
      $error = 1;
    } else {
      $redirect->update($params);
      header("Location: " . BASE_URL . "/admin/redirects");
      exit;
    }
  }

  $smarty->assign("error", $error);
  $smarty->assign("redirect", $redirect);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "redirects");

  $smarty->display("admin-redirects-edit.tmpl");
?>
