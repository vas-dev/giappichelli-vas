<?php
  if (!$principal || !in_array(2, $principal['roles'])) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $homeworks = models\Homework::get(array("course_id" => $course['id']), 0, 100);

  $smarty->assign("course", $course);
  $smarty->assign("homeworks", $homeworks);

  $smarty->assign("submenu", "profilo/compiti");

  $smarty->display("profilo-corsi-compiti.tmpl");
