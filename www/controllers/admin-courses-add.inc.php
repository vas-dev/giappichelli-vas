<?php
  $course = _request('clone') ? models\Course::get_by_id(_request('clone')) : array(
    "pdf" => null,
    "prices" => array(
      array("price" => 0, "description" => "Prezzo per persona"),
    ),
  );

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    $smarty->assign("dates", _post('dates'));
    $smarty->display("calendar.tmpl");
    exit;
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'teachers' => _post('teachers'),
      'name' => _post('name'),
      'category' => _post('category'),
      'city' => _post('city'),
      'slug' => _post('slug'),
      'preview' => _post('preview'),
      'description' => _post('description'),
      'duration' => _post('duration'),
      'calendar' => _post('calendar'),
      'place' => _post('place'),
      'prices' => _post('prices'),
      'image' => _file('image', 'courses'),
      'pdf' => _file('pdf', 'courses'),
      'form' => _file('form', 'courses'),
      'button_label' => _post('button_label'),
      'button_link' => _post('button_link'),
      'max_hours' => _post('max_hours'),
      'features_form' => _post('features_form'),
      'published' => _post('published'),
      'published_datetime' => _post_datetime('published_datetime'),
      'published_datetime2' => _post_datetime('published_datetime2'),
      'sorting' => _post('sorting'),
      'seo_title' => _post('seo_title'),
      'seo_image' => _image('seo_image', 'courses'),
      'seo_description' => _post('seo_description'),
      'seo_keywords' => _post('seo_keywords'),
    );
    if (!$params['name']) {
      $errors['name'] = 1;
    }
    if (!$params['slug']) {
      $errors['slug'] = 1;
    }
    if (!$errors) {
      models\Course::add($params);
      header("Location: " . BASE_URL . "/admin/courses");
      exit;
    }
  }

  $teachers = models\Teacher::get_vocabulary();
  $smarty->assign("teachers", $teachers);

  $smarty->assign("course", $course);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "courses");

  $smarty->display("admin-courses-add.tmpl");
?>
