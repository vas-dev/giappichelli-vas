<?php
  $params = array(
    "q" => _request('q'),
    "published" => _request('published'),
    "ids" => $principal['courses'],
  );

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $courses = models\Course::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Course::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("courses", $courses);
  $smarty->assign("batch_url", BASE_URL . "/admin/courses?" . http_build_query(array_filter(array(
    'q' => $params['q'],
  ))). "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "courses");

  $smarty->display("admin-courses.tmpl");
?>
