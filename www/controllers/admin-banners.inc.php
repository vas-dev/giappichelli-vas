<?php
  $params = array("q" => _request('q'), "category" => _request('category'));

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $banners = models\Banner::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Banner::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("banners", $banners);
  $smarty->assign("batch_url", BASE_URL . "/admin/banners?q=" . $params['q'] . "&category=" . $params['category'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "banners");

  $smarty->display("admin-banners.tmpl");
?>
