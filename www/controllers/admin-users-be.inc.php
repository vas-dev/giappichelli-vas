<?php
  $delete = _get('delete');
  if ($delete) {
    $user = models\UserBE::get_by_id($delete);
    if ($user) {
      $user->delete();
    } else {
      $delete = null;
    }
  }

  $params = array('q' => _request('q'));
  $users = models\UserBE::get($params);

  $smarty->assign("delete", $delete);
  $smarty->assign("users", $users);

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "users-be");

  $smarty->display("admin-users-be.tmpl");
?>
