<?php
  $lesson = models\Lesson::get_by_id($id);
  if (!$lesson || $principal['role'] == 2 && !array_intersect($lesson['courses'], $principal['courses'])) {
    not_found();
  }

  $error = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'courses' => _post('courses'),
      'name' => _post('name'),
      'description' => _post('description'),
      'type' => _post('type'),
      'widget' => _post('widget'),
      'widget_audio' => _post('widget_audio'),
      'lesson_date' => _post_date('lesson_date'),
      'lesson_time' => _post('lesson_time'),
      'files' => _post_fileupload('files'),
      'homework' => _post('homework'),
      'materials' => _post('materials'),
      'questions' => _post('questions'),
      'related' => _post('related'),
      'playlist' => _post('playlist'),
      'support_button' => _post('support_button'),
      'support_target' => _post('support_target'),
      'max_views' => _post('max_views'),
      'published' => _post('published'),
      'published_datetime' => _post_datetime('published_datetime'),
      'published_datetime2' => _post_datetime('published_datetime2'),
    );
    if (!$params['courses'] || !$params['name']) {
      $error = 1;
    } else {
      $lesson->update($params);
      header("Location: " . BASE_URL . "/admin/lessons");
      exit;
    }
  }

  $smarty->assign("courses", models\Course::get_vocabulary());
  $smarty->assign("lessons", models\Lesson::get_vocabulary([
    "courses" => _f('courses', $lesson),
    "id:not" => $lesson['id'],
  ]));

  $smarty->assign("error", $error);
  $smarty->assign("lesson", $lesson);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "lessons");

  $smarty->display("admin-lessons-edit.tmpl");
?>
