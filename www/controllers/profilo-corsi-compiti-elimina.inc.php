<?php
  if (!$principal || !in_array(2, $principal['roles'])) {
    header("Location: " . BASE_URL . "/login");
    exit;
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $homework->delete();
    header("Location: " . BASE_URL . "/profilo/corsi/" . $course['slug'] . "/compiti");
    exit;
  }

  $smarty->assign("course", $course);
  $smarty->assign("homework", $homework);
  $smarty->assign("errors", $errors);

  $smarty->assign("submenu", "profilo/compiti");

  $smarty->display("profilo-corsi-compiti-elimina.tmpl");
