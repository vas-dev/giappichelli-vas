<?php
  $category = models\BlogCategory::get_by_id($id);
  if (!$category) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $category->delete();
    header("Location: " . BASE_URL . "/admin/blog-categories");
    exit;
  }

  $smarty->assign("category", $category);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-categories");

  $smarty->display("admin-blog-categories-delete.tmpl");
?>
