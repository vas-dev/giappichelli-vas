<?php
  $group = models\Group::get_by_id($group_id);
  if (!$group) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $group->delete();
    header("Location: " . BASE_URL . "/admin/groups");
    exit;
  }

  $smarty->assign("group", $group);

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "groups");

  $smarty->display("admin-groups-delete.tmpl");
?>
