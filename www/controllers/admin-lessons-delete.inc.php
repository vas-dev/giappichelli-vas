<?php
  $lesson = models\Lesson::get_by_id($id);
  if (!$lesson) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $lesson->delete();
    header("Location: " . BASE_URL . "/admin/lessons");
    exit;
  }

  $smarty->assign("lesson", $lesson);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "lessons");

  $smarty->display("admin-lessons-delete.tmpl");
?>
