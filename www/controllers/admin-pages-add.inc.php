<?php
  $page = _request('clone') ? models\Page::get_by_id(_request('clone')) : array();
  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'lang' => _post('lang'),
      'slug' => _post('slug'),
      'title' => _post('title'),
      'cover' => _image('cover', 'pages'),
      'published' => _post('published'),
      'published_datetime' => _post_datetime('published_datetime'),
      'published_datetime2' => _post_datetime('published_datetime2'),
      'seo_title' => _post('seo_title'),
      'seo_image' => _image('seo_image', 'pages'),
      'seo_description' => _post('seo_description'),
      'seo_keywords' => _post('seo_keywords'),
      'content' => _a($page, 'content'),
    );
    if (!$params['slug']) {
      $errors['slug'] = 1;
    }
    if (!$errors) {
      models\Page::add($params);
      header("Location: " . BASE_URL . "/admin/pages");
      exit;
    }
  }

  $smarty->assign("page", $page);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pages");

  $smarty->display("admin-pages-add.tmpl");
?>
