<?php
  $comment = models\BlogArticleComment::get_by_id($id);
  if (!$comment) {
    not_found();
  }

  $error = null;
  $article = $comment->get_article();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'user' => _post('user'),
      'type' => _post('type'),
      'body' => _post('body'),
    );
    if (!$params['user'] || !$params['body']) {
      $error = 1;
    } else {
      $user = models\User::get_by_id($params['user']);
      $article->add_comment($params['body'], $user, $params['type'] == 1 ? $comment['id'] : null);
      header("Location: " . BASE_URL . "/admin/blog-comments");
      exit;
    }
  }

  $smarty->assign("types", array(
    1 => "Rispondi al singolo commento",
    2 => "Crea un nuovo commento per l'articolo in testa",
  ));

  $users = models\User::get_moderators();

  $smarty->assign("error", $error);
  $smarty->assign("comment", $comment);
  $smarty->assign("article", $article);
  $smarty->assign("users", $users);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "blog-comments");

  $smarty->display("admin-blog-comments-edit.tmpl");
?>
