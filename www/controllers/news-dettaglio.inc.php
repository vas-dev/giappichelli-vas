<?php
  $article = models\BlogArticle::get_by_id($article_id);
  if (!$article) {
    not_found();
  }

  $confirm = false;
  $comment = _post('comment');
  if ($principal && $comment) {
    $article->add_comment($comment, $principal);

    $mail = new \PHPMailer();
    $mail->CharSet = 'utf-8';
    $mail->Encoding = '8bit';
    $mail->SetFrom(EMAIL_ADDRESS, EMAIL_NAME);
    foreach (explode(",", EMAIL_NOTIFICATIONS) as $email) {
      $mail->AddAddress($email);
    }
    $mail->Subject = "L'utente " . $principal['lastname'] . " " . $principal['firstname'] . " ha inviato un commento su una news";
    $mail->Body = "Visualizzare il commento da moderare alla pagina " . BASE_URL . "/admin/blog-comments";
    $mail->Send();

    $confirm = 1;
  }

  $params = array();
  $params['status'] = 1;
  $params['batch'] = 25;
  $params['offset'] = _request('n');
  $params['count'] = $article->get_comments_count($params);
  $params['url'] = BASE_URL . $request_uri;
  $comments = $article->get_comments($params, $params['offset'], $params['batch']);

  $smarty->assign("article", $article);
  $smarty->assign("comments", $comments);
  $smarty->assign("params", $params);

  $archive = models\BlogArticle::get_archive(1);
  $bcategories = models\BlogCategory::get(array("category_id" => 1));
  $tags = models\BlogTag::get();

  $related = $article->get_related();
  $smarty->assign("related", $related);

  $smarty->assign("archive", $archive);
  $smarty->assign("bcategories", $bcategories);
  $smarty->assign("tags", $tags);
  $smarty->assign("year", null);
  $smarty->assign("category", null);
  $smarty->assign("tag", null);

  $smarty->assign("confirm", $confirm);

  $smarty->assign("menu", "news");

  $smarty->display("news-dettaglio.tmpl");
?>
