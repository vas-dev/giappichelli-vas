<?php
  $user = models\UserBE::get_by_id($user_id);
  if (!$user) {
    not_found();
  }

  $status = null;

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      "company" => _post('company'),
      "lastname" => _post('lastname'),
      "firstname" => _post('firstname'),
      "email" => _post('email'),
      "password" => _post('password'),
      "role" => _post('role'),
      "roles" => _post('roles'),
      "courses" => _post('courses'),
    );
    $user->update($params);
    $status = 1;
  }

  $smarty->assign("user", $user);
  $smarty->assign("status", $status);
  $smarty->assign("courses", models\Course::get_vocabulary());

  $smarty->assign("menu", "users");
  $smarty->assign("submenu", "users-be");

  $smarty->display("admin-users-be-edit.tmpl");
?>
