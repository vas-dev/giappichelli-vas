<?php
  $params = array("q" => _request('q'));

  $params['batch'] = 25;
  $params['offset'] = _request('offset');
  $redirects = models\Redirect::get($params, $params['offset'], $params['batch']);
  $params['count'] = models\Redirect::get_count($params);

  $smarty->assign("params", $params);
  $smarty->assign("redirects", $redirects);
  $smarty->assign("batch_url", BASE_URL . "/admin/redirects?q=" . $params['q'] . "&");

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "redirects");

  $smarty->display("admin-redirects.tmpl");
?>
