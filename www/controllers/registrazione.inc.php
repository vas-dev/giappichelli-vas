<?php
  $confirm = false;
  $errors = array();
  $user = array(
    "roles" => [1],
    "courses" => _request('corso') ? [_request('corso')] : null,
  );

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'firstname' => _post('firstname'),
      'lastname' => _post('lastname'),
      'address' => _post('address'),
      'city' => _post('city'),
      'zipcode' => _post('zipcode'),
      'province' => _post('province'),
      'country' => _post('country'),
      'mobile' => _post('mobile'),
      'phone' => _post('phone'),
      'email' => _post('email'),
      'email2' => _post('email2'),
      'password' => _post('password'),
      'password2' => _post('password2'),
      'courses' => _post('courses'),
      'roles' => _post('roles'),
      'privacy' => _post('privacy'),
      'privacy2' => _post('privacy2'),
      'newsletter' => _post('newsletter'),
    );
    if (!$params['firstname']) {
      $errors['firstname'] = 1;
    }
    if (!$params['lastname']) {
      $errors['lastname'] = 1;
    }
    if (!$params['roles']) {
      $errors['roles'] = 1;
    }
    if (!$params['courses']) {
      $errors['courses'] = 1;
    }
    if (!$params['privacy']) {
      $errors['privacy'] = 1;
    }
    if (!$params['email']) {
      $errors['email'] = 1;
    } else if (!_email($params['email'])) {
      $errors['email'] = 2;
    } else if ($params['email'] && !$params['email2']) {
      $errors['email2'] = 1;
    } else if ($params['email'] != $params['email2']) {
      $errors['email2'] = 2;
    } else {
      $parms['email'] = strtolower($params['email']);
      if ($u = models\User::get_by_email($params['email'])) {
        $errors['email'] = 3;
      }
    }
    if (!$params['password']) {
      $errors['password'] = 1;
    } else if (!models\User::password_policy($params, $params['password'])) {
      $errors['password'] = 2;
    } else if ($params['password'] != $params['password2']) {
      $errors['password2'] = 1;
    }
    if (!$errors) {
      $user_id = models\User::add($params);
      $user = models\User::get_by_id($user_id);
      $user->email_registrazione();
      $confirm = 1;
    }
  }

  $smarty->assign("user", $user);
  $smarty->assign("errors", $errors);
  $smarty->assign("confirm", $confirm);
  $smarty->assign("courses", models\Course::get_vocabulary(["linked" => true]));

  $smarty->display("registrazione.tmpl");
?>
