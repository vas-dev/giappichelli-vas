<?php
  header("Content-type: text/xml");

  $links = array();

  $count = models\Place::get_count(array("published" => 2));
  for ($i = 0; $i < $count; $i += 100) {
    foreach (models\Place::get(array("published" => 2, $i, 100)) as $j) {
      $links[] = BASE_URL . "/strutture/" . $j['slug'];
    }
  }

  $count = models\Course::get_count(array("published" => 2));
  for ($i = 0; $i < $count; $i += 100) {
    foreach (models\Course::get(array("published" => 2, $i, 100)) as $j) {
      $links[] = BASE_URL . "/corsi/" . $j['slug'];
    }
  }

  $count = models\Event::get_count(array("published" => 2));
  for ($i = 0; $i < $count; $i += 100) {
    foreach (models\Event::get(array("published" => 2, $i, 100)) as $j) {
      $j['slug'] = $j['id'] . "-" . _a($j->course(), 'slug');
      $links[] = BASE_URL . "/eventi/" . $j['slug'];
    }
  }

  $smarty->assign("links", $links);

  $pages = models\Page::get(array("published" => 1), 0, 100);
  $smarty->assign("pages", $pages);


  $smarty->display("sitemap.tmpl");
