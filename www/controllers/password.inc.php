<?php
  $confirm = false;
  $errors = array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $params = array(
      'email' => _post('email'),
    );
    if (!$params['email'] || !($user = models\User::get_by_email($params['email']))) {
      $errors['email'] = 1;
    }
    if (!$errors) {
      $user->email_password();
      $confirm = true;
    }
  }

  $smarty->assign("confirm", $confirm);
  $smarty->assign("errors", $errors);

  $smarty->display("password.tmpl");
?>
