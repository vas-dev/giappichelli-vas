<?php
  $page = models\Page::get_by_id($page_id);
  if (!$page) {
    not_found();
  }

  $errors = array();
  $content = $content_id ? array_values(array_filter($page->content(), function ($i) use ($content_id) { return $i['id'] == $content_id; })) : array();
  $content = $content ? $content[0] : array();

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (_request('delete')) {
      $page->unset_content($content['id']);
      header("Location: " . BASE_URL . "/admin/pages/" . $page['id']);
      exit;
    }
    $params = array(
      'id' => $content ? $content['id'] : uniqid(),
      'name' => _post('name'),
      'slug' => _post('slug'),
      'sorting' => _post('sorting'),
      'published' => _post('published'),
      'published_datetime' => _datetime_json(_post_datetime('published_datetime')),
      'published_datetime2' => _datetime_json(_post_datetime('published_datetime2')),
      'condition' => _post('condition'),
      'type' => _post('type'),
      'title' => _post('title'),
      'subtitle' => _post('subtitle'),
      'image' => _a(_image('image', 'pages'), 'asset'),
      'image_link' => _post('image_link'),
      'cta' => _post('cta'),
      'cta_link' => _post('cta_link'),
      'text_1' => _post('text_1'),
      'text_2' => _post('text_2'),
      'text_3' => _post('text_3'),
      'text_4' => _post('text_4'),
      'images' => _post_fileupload('images'),
      'files' => _post_fileupload('files'),
      'config' => _post('config'),
      'class' => _post('class'),
      'style' => _post('style'),
    );
    if (!$params['sorting']) {
      $errors['sorting'] = 1;
    }
    if (!$params['condition']) {
      $errors['condition'] = 1;
    }
    if (!$params['type']) {
      $errors['type'] = 1;
    }
    if (!$errors) {
      if (!$content) {
        $page->add_content($params);
      } else {
        $page->set_content($params);
      }
      header("Location: " . BASE_URL . "/admin/pages/" . $page['id']);
      exit;
    }
  }

  $smarty->assign("page", $page);
  $smarty->assign("content", $content);
  $smarty->assign("errors", $errors);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pages");

  $smarty->display("admin-pages-edit-content.tmpl");
?>
