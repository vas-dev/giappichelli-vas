<?php
  $page = models\Page::get_by_id($page_id);
  if (!$page) {
    not_found();
  }

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $page->delete();
    header("Location: " . BASE_URL . "/admin/pages");
    exit;
  }

  $smarty->assign("page", $page);

  $smarty->assign("menu", "content");
  $smarty->assign("submenu", "pages");

  $smarty->display("admin-pages-delete.tmpl");
?>
