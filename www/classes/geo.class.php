<?php

  namespace classes;

  class Geo {

    static public function get_by_city($name) {
      global $db;
      $rs = mysqli_query($db, "SELECT lat, lng FROM cities WHERE name = " . _text($name) . " ORDER BY name;") or die(mysql_error());
      return mysqli_fetch_assoc($rs);
    }

    static public function get_by_province($name) {
      global $db;
      $rs = mysqli_query($db, "SELECT AVG(lat) AS lat, AVG(lng) AS lng FROM cities WHERE province = " . _text($name) . ";") or die(mysql_error());
      return mysqli_fetch_assoc($rs);
    }

  }

?>
